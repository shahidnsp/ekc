let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix .sass('resources/assets/sass/app.scss', 'public/css')
//     .copy('resources/assets/backend/js/!*.js', 'public/js')
//     .copy('resources/assets/backend/webfonts/', 'public/webfonts')
//     .copy('resources/assets/backend/fonts/', 'public/fonts')
//     .copy('resources/assets/backend/images/', 'public/images')
//     .copy('resources/assets/backend/img/', 'public/img')
//     .copy('resources/assets/backend/ckeditor/', 'public/ckeditor')
//     .copy('resources/assets/backend/sass/jasny-bootstrap.min.css', 'public/css');

/*mix.scripts([
    'resources/assets/backend/js/angular/angular.min.js',
    'resources/assets/backend/js/angular/angular-notify.js',
    'resources/assets/backend/js/angular/angular-resource.min.js',
    'resources/assets/backend/js/angular/angular-route.min.js',
    'resources/assets/backend/js/angular/ui-bootstrap-tpls.min.js',
    'resources/assets/backend/js/angular/angular-confirm.min.js',
    'resources/assets/backend/js/angular/angular-http-request-loader.min.js',
    'resources/assets/backend/js/angular/ng-google-chart.js',
    'resources/assets/backend/js/angular/angular-datepicker.min.js',
    'resources/assets/backend/js/angular/ng-tags-input.min.js',
    'resources/assets/backend/js/angular/moment.min.js',
    'resources/assets/backend/js/angular/advanced_select.js',
    'resources/assets/backend/js/angular/angular-ui-notification.js',
    'resources/assets/backend/js/angular/angular-ckeditor.js',
], 'public/js/angular.js');*/

mix.copy('resources/assets/frontend/', 'public/frontend');

mix.scripts([
    'resources/assets/backend/js/angular/angularscript.js',
    'resources/assets/backend/js/angular/controller/*.js',
    'resources/assets/backend/js/angular/service/*.js'
], 'public/js/angularscript.js');