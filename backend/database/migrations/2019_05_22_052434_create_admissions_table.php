<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appno');
            $table->date('date');
            $table->string('name');
            $table->string('course');
            $table->string('address');
            $table->string('fathername');
            $table->string('studentphone');
            $table->string('fatherphone');
            $table->string('caste');
            $table->string('plustworegno');
            $table->string('yearofpass');
            $table->string('gender');
            $table->date('dateofbirth');
            $table->integer('plustwomark');
            $table->decimal('plustwomarkper',8,2)->default(0);
            $table->string('keamregno')->nullable();
            $table->string('jeeregno')->nullable();
            $table->string('jeescore')->nullable();
            $table->string('natascore')->nullable();
            $table->decimal('ekcscore',8,2)->nullable(); //(plustwomarkper*2)+natascore   ->For B arch only
            $table->string('department');
            $table->string('from')->default('Main');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
