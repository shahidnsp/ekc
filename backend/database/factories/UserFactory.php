<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'pass'=>'secret',
        'isAdmin'=>$faker->boolean,
        'photo'=>'profile.png',
        'permission'=>'1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,
            1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111',
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'designation' => $faker->word,
        'photo'=>'profile.png',
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Testimonial::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'designation' => $faker->word,
        'photo'=>'profile.png',
        'description' => $faker->word,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Slider::class, function (Faker $faker) {
    return [
        'department' => 'Main',
        'photo'=>$faker->randomElement(['slider-1.jpg','slider-2.jpg','slider-3.jpg']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'photo'=>$faker->randomElement(['update-1.png','update-2.png','update-3.png']),
        'description' => $faker->sentence(10),
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Gallary::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'description' => $faker->word,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Brochure::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'filename' => $faker->word,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Download::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'type' => $faker->randomElement(['Photo','Link','PDF']),
        'link'=>$faker->url,
        'file' => $faker->word,
        'department' =>$faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\GallaryItem::class, function (Faker $faker) {
    return [
        'filename' => $faker->randomElement(['slider-1.jpg','slider-2.jpg','slider-3.jpg','update-1.png','update-2.png','update-3.png']),
        'type' => 'Photo',
        'gallaries_id'=>$faker->numberBetween(1,50),
    ];
});
$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'link' => $faker->url,
        'description' => $faker->sentence(20),
        'photo'=>'profile.png',
        'department' =>$faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Video::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'link' => 'https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0',
        'description' => $faker->word,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'description' => $faker->sentence,
        'phone' => $faker->phoneNumber,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
    ];
});

$factory->define(App\Admission::class, function (Faker $faker) {
    return [
        'appno' => $faker->randomDigit,
        'date' => $faker->date,
        'name' => $faker->name,
        'course' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
        'address' => $faker->address,
        'fathername' => $faker->name,
        'studentphone' => $faker->phoneNumber,
        'fatherphone' => $faker->phoneNumber,
        'caste' => $faker->randomElement(['General','OBC','OEC','SC/ST','Others']),
        'plustworegno' => $faker->randomDigit,
        'yearofpass' => $faker->year,
        'gender' => $faker->randomElement(['Male','Female']),
        'dateofbirth' => $faker->date,
        'plustwomark' => $faker->randomNumber,
        'plustwomarkper' => $faker->randomNumber,
        'keamregno' => $faker->randomNumber,
        'jeeregno' => $faker->randomNumber,
        'jeescore' => $faker->randomNumber,
        'natascore' => $faker->randomNumber,
        'ekcscore' => $faker->randomNumber,
        'department' => $faker->randomElement(['Main','Engineering','Architecture','Commerce','Public']),
        'from' => $faker->randomElement(['Main','Civil','Mechanical','Electronics','Computer','Safety']),
    ];
});

