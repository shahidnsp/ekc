<?php

use Illuminate\Database\Seeder;

class GallaryItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\GallaryItem::class, 50)->create();
    }
}
