<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       //  $this->call(UsersTableSeeder::class);
        \App\User::create(['name'=>'admin','email'=>'admin@admin.com',
            'password'=>bcrypt('admin'),'pass'=>'admin','isAdmin'=>1,
            'permission'=>'1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,
            1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111']);
        $this->call(BrochureSeeder::class);
        $this->call(DownloadSeeder::class);
        $this->call(GallarySeeder::class);
        $this->call(GallaryItemSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(NotificationSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(TestimonialSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(AdmissionSeeder::class);

        Illuminate\Support\Facades\Cache::flush();
    }
}
