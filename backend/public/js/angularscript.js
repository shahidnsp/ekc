var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'cp.ngConfirm',
        'angular.http.request.loader',
        'googlechart',
        '720kb.datepicker',
        'ngTagsInput',
        'vd.directive.advanced_select',
        'ui-notification',
        'ckeditor',
        'UserInfoService',
        'BrochureService',
        'TeamService',
        'TestimonialService',
        'SliderService',
        'DownloadService',
        'NewsService',
        'NotificationService',
        'GalleryService',
        'BrochureService',
        'UserService',
        'ContactService',
        'VideoService',
        'PopupService',
        'AdmissionService'
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/user', {
			templateUrl: 'template/user',
			controller: 'UserController'
		})
        .when('/teamsekc', {
            templateUrl: 'template/teamsekc',
            controller: 'TeamsEKCController'
        })
        .when('/testimonialsekc', {
            templateUrl: 'template/testimonialsekc',
            controller: 'TestimonialEKCController'
        })
        .when('/slidersekc', {
            templateUrl: 'template/slidersekc',
            controller: 'SliderEKCController'
        })
        .when('/downloadsekc', {
            templateUrl: 'template/downloadsekc',
            controller: 'DownloadEKCController'
        })
        .when('/newsekc', {
            templateUrl: 'template/newsekc',
            controller: 'NewsEKCController'
        })
        .when('/notificationsekc', {
            templateUrl: 'template/notificationsekc',
            controller: 'NotificationEKCController'
        })
        .when('/galleryekc', {
            templateUrl: 'template/galleryekc',
            controller: 'GalleryEKCController'
        } )
        .when('/brochuresekc', {
            templateUrl: 'template/brochuresekc',
            controller: 'BrochureEKCController'
        } )
        .when('/contactsekc', {
            templateUrl: 'template/contactsekc',
            controller: 'ContactEKCController'
        } )
        .when('/videosekc', {
            templateUrl: 'template/videosekc',
            controller: 'VideoEKCController'
        } )
        .when('/popupekc', {
            templateUrl: 'template/popupekc',
            controller: 'PopupEKCController'
        } )

 //Engineering

        .when('/testimonialseng', {
            templateUrl: 'template/testimonialseng',
            controller: 'TestimonialEngController'
        })
        .when('/sliderseng', {
            templateUrl: 'template/sliderseng',
            controller: 'SliderEngController'
        })
        .when('/downloadseng', {
            templateUrl: 'template/downloadseng',
            controller: 'DownloadEngController'
        })
        .when('/newseng', {
            templateUrl: 'template/newseng',
            controller: 'NewsEngController'
        })
        .when('/notificationseng', {
            templateUrl: 'template/notificationseng',
            controller: 'NotificationEngController'
        })
        .when('/galleryeng', {
            templateUrl: 'template/gallaryeng',
            controller: 'GalleryEngController'
        } )
        .when('/brochureseng', {
            templateUrl: 'template/brochureseng',
            controller: 'BrochureEngController'
        } )
        .when('/teamseng', {
            templateUrl: 'template/teamseng',
            controller: 'TeamsEngController'
        } )
        .when('/contactseng', {
            templateUrl: 'template/contactseng',
            controller: 'ContactEngController'
        } )
        .when('/videoseng', {
            templateUrl: 'template/videoseng',
            controller: 'VideoEngController'
        } )
        .when('/popupeng', {
            templateUrl: 'template/popueng',
            controller: 'PopupEngController'
        } )
        .when('/admissioneng', {
            templateUrl: 'template/admissioneng',
            controller: 'AdmissionEngController'
        })

//Architecture
        .when('/brochuresarc', {
            templateUrl: 'template/brochuresarc',
            controller: 'BrochureArcController'
        } )
        .when('/testimonialsarc', {
            templateUrl: 'template/testimonialsarc',
            controller: 'TestimonialArcController'
            })
        .when('/slidersarc', {
            templateUrl: 'template/slidersarc',
            controller: 'SliderArcController'
        })
        .when('/downloadsarc', {
            templateUrl: 'template/downloadsarc',
            controller: 'DownloadArcController'
        })
        .when('/newsarc', {
            templateUrl: 'template/newsarc',
            controller: 'NewsArcController'
        })
        .when('/notificationsarc', {
            templateUrl: 'template/notificationsarc',
            controller: 'NotificationArcController'
        })
        .when('/galleryarc', {
            templateUrl: 'template/gallaryarc',
            controller: 'GalleryArcController'
        } )
        .when('/brochuresarc', {
            templateUrl: 'template/brochuresarc',
            controller: 'BrochureArcController'
        } )
        .when('/teamsarc', {
            templateUrl: 'template/teamsarc',
            controller: 'TeamsArcController'
            } )
        .when('/contactsarc', {
            templateUrl: 'template/contactsarc',
            controller: 'ContactArcController'
        } )
        .when('/videosarc', {
            templateUrl: 'template/videosarc',
            controller: 'VideoArcController'
        } )
        .when('/popuparc', {
            templateUrl: 'template/popuparc',
            controller: 'PopupArcController'
        } )
        .when('/admissionarc', {
            templateUrl: 'template/admissionarc',
            controller: 'AdmissionArcController'
        })

//Public
        .when('/brochurespub', {
            templateUrl: 'template/brochurespub',
            controller: 'BrochurePubController'
        } )
        .when('/testimonialspub', {
            templateUrl: 'template/testimonialspub',
            controller: 'TestimonialPubController'
        })
        .when('/sliderspub', {
            templateUrl: 'template/sliderspub',
            controller: 'SliderPubController'
        })
        .when('/downloadspub', {
            templateUrl: 'template/downloadspub',
            controller: 'DownloadPubController'
        })
        .when('/newspub', {
            templateUrl: 'template/newspub',
            controller: 'NewsPubController'
        })
        .when('/notificationspub', {
            templateUrl: 'template/notificationspub',
            controller: 'NotificationPubController'
        })
        .when('/gallerypub', {
            templateUrl: 'template/gallarypub',
            controller: 'GalleryPubController'
        } )
        .when('/brochurespub', {
            templateUrl: 'template/brochurespub',
            controller: 'BrochurePubController'
        } )
       .when('/teamspub', {
            templateUrl: 'template/teamspub',
            controller: 'TeamsPubController'
       } )
        .when('/contactspub', {
            templateUrl: 'template/contactspub',
            controller: 'ContactPubController'
        } )
        .when('/videospub', {
            templateUrl: 'template/videospub',
            controller: 'VideoPubController'
        } )
        .when('/popuppub', {
            templateUrl: 'template/popuppub',
            controller: 'PopupPubController'
        } )
        .when('/admissionpub', {
            templateUrl: 'template/admissionpub',
            controller: 'AdmissionPubController'
        })

//commerce
       .when('/brochurescom', {
            templateUrl: 'template/brochurescom',
            controller: 'BrochureComController'
        } )
        .when('/testimonialscom', {
            templateUrl: 'template/testimonialscom',
            controller: 'TestimonialComController'
        })
        .when('/sliderscom', {
            templateUrl: 'template/sliderscom',
            controller: 'SliderComController'
        })
        .when('/downloadscom', {
            templateUrl: 'template/downloadscom',
            controller: 'DownloadComController'
        })
        .when('/newscom', {
            templateUrl: 'template/newscom',
            controller: 'NewsComController'
        })
        .when('/notificationscom', {
             templateUrl: 'template/notificationscom',
             controller: 'NotificationComController'
        })
        .when('/gallerycom', {
             templateUrl: 'template/gallarycom',
             controller: 'GalleryComController'
        } )
        .when('/brochurescom', {
             templateUrl: 'template/brochurescom',
             controller: 'BrochureComController'
        } )
       .when('/teamscom', {
             templateUrl: 'template/teamscom',
             controller: 'TeamsComController'
       } )
        .when('/contactscom', {
            templateUrl: 'template/contactscom',
            controller: 'ContactComController'
        } )
        .when('/videoscom', {
            templateUrl: 'template/vdeoscom',
            controller: 'VideoComController'
        } )
        .when('/popupcom', {
            templateUrl: 'template/popupcom',
            controller: 'PopupComController'
        } )
        .when('/admissioncom', {
            templateUrl: 'template/admissioncom',
            controller: 'AdmissionComController'
        } )
       .otherwise({
            redirectTo: 'template/dashboard'
        });
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();

                        reader.onload = (function (i) {
                            return function(e) {
                                var value = {
                                    lastModified: changeEvent.target.files[i].lastModified,
                                    lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                    name: changeEvent.target.files[i].name,
                                    size: changeEvent.target.files[i].size,
                                    type: changeEvent.target.files[i].type,
                                    data: e.target.result
                                };
                                values.push(value);
                            }

                        })(i);

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }


                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });

    app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });


    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        }
    });






app.controller('AdmissionArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Admission){

    $scope.admissions = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadAdmission() {
        $scope.admissions = [];
        Admission.query({department:'Architecture'},function (admissions) {
            $scope.admissions = admissions;
        });
    }

    loadAdmission();


    $scope.admissionedit = false;
    $scope.imageViewAdmission=false;

    $scope.newAdmission = function () {
        $scope.admissionedit = true;
        $scope.newadmission = new Admission();
        $scope.newadmission.department='Architecture';
        //$scope.newadmission.from='Main';
        $scope.curAdmission = {};
    };
    $scope.editAdmission = function (thisFlash) {
        $scope.admissionedit = true;
        $scope.imageViewAdmission=true;
        $scope.curAdmission = thisFlash;
        $scope.newadmission = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelAdmission = function () {
        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
    };

    $scope.addAdmission = function () {
        if ($scope.curAdmission.id) {
            $scope.newadmission.$update(function (flash) {
                angular.extend($scope.curAdmission, $scope.curAdmission, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Updated Successfully');
            });

        } else {
            $scope.newadmission.$save(function (admissions) {
                $scope.admissions.push(admissions);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Saved Successfully');
            });
        }

        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
        //loadAdmission();
    };

    $scope.deleteAdmission = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.admissions.indexOf(item);
                         $scope.admissions.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Admission Removed Successfully'
                         });

                         });*/

                        $http.get('apiweb/admissions/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.admissions.indexOf(item);
                                $scope.admissions.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Admission Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

});

app.controller('AdmissionComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Admission){

    $scope.admissions = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadAdmission() {
        $scope.admissions = [];
        Admission.query({department:'Commerce'},function (admissions) {
            $scope.admissions = admissions;
        });
    }

    loadAdmission();


    $scope.admissionedit = false;
    $scope.imageViewAdmission=false;

    $scope.newAdmission = function () {
        $scope.admissionedit = true;
        $scope.newadmission = new Admission();
        $scope.newadmission.department='Commerce';
        //$scope.newadmission.from='Main';
        $scope.curAdmission = {};
    };
    $scope.editAdmission = function (thisFlash) {
        $scope.admissionedit = true;
        $scope.imageViewAdmission=true;
        $scope.curAdmission = thisFlash;
        $scope.newadmission = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelAdmission = function () {
        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
    };

    $scope.addAdmission = function () {
        if ($scope.curAdmission.id) {
            $scope.newadmission.$update(function (flash) {
                angular.extend($scope.curAdmission, $scope.curAdmission, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Updated Successfully');
            });

        } else {
            $scope.newadmission.$save(function (admissions) {
                $scope.admissions.push(admissions);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Saved Successfully');
            });
        }

        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
        //loadAdmission();
    };

    $scope.deleteAdmission = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.admissions.indexOf(item);
                         $scope.admissions.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Admission Removed Successfully'
                         });

                         });*/

                        $http.get('apiweb/admissions/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.admissions.indexOf(item);
                                $scope.admissions.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Admission Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

});

app.controller('AdmissionEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Admission){

    $scope.admissions = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadAdmission() {
        $scope.admissions = [];
        Admission.query({department:'Engineering'},function (admissions) {
            $scope.admissions = admissions;
        });
    }

    loadAdmission();


    $scope.admissionedit = false;
    $scope.imageViewAdmission=false;

    $scope.newAdmission = function () {
        $scope.admissionedit = true;
        $scope.newadmission = new Admission();
        $scope.newadmission.department='Engineering';
        //$scope.newadmission.from='Main';
        $scope.curAdmission = {};
    };
    $scope.editAdmission = function (thisFlash) {
        $scope.admissionedit = true;
        $scope.imageViewAdmission=true;
        $scope.curAdmission = thisFlash;
        $scope.newadmission = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelAdmission = function () {
        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
    };

    $scope.addAdmission = function () {
        if ($scope.curAdmission.id) {
            $scope.newadmission.$update(function (flash) {
                angular.extend($scope.curAdmission, $scope.curAdmission, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Updated Successfully');
            });

        } else {
            $scope.newadmission.$save(function (admissions) {
                $scope.admissions.push(admissions);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Saved Successfully');
            });
        }

        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
        //loadAdmission();
    };

    $scope.deleteAdmission = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.admissions.indexOf(item);
                         $scope.admissions.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Admission Removed Successfully'
                         });

                         });*/

                        $http.get('apiweb/admissions/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.admissions.indexOf(item);
                                $scope.admissions.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Admission Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
    
});

app.controller('AdmissionPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Admission){

    $scope.admissions = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadAdmission() {
        $scope.admissions = [];
        Admission.query({department:'Public'},function (admissions) {
            $scope.admissions = admissions;
        });
    }

    loadAdmission();


    $scope.admissionedit = false;
    $scope.imageViewAdmission=false;

    $scope.newAdmission = function () {
        $scope.admissionedit = true;
        $scope.newadmission = new Admission();
        $scope.newadmission.department='Public';
        //$scope.newadmission.from='Main';
        $scope.curAdmission = {};
    };
    $scope.editAdmission = function (thisFlash) {
        $scope.admissionedit = true;
        $scope.imageViewAdmission=true;
        $scope.curAdmission = thisFlash;
        $scope.newadmission = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelAdmission = function () {
        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
    };

    $scope.addAdmission = function () {
        if ($scope.curAdmission.id) {
            $scope.newadmission.$update(function (flash) {
                angular.extend($scope.curAdmission, $scope.curAdmission, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Updated Successfully');
            });

        } else {
            $scope.newadmission.$save(function (admissions) {
                $scope.admissions.push(admissions);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Saved Successfully');
            });
        }

        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
        //loadAdmission();
    };

    $scope.deleteAdmission = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.admissions.indexOf(item);
                         $scope.admissions.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Admission Removed Successfully'
                         });

                         });*/

                        $http.get('apiweb/admissions/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.admissions.indexOf(item);
                                $scope.admissions.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Admission Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

});

app.controller('BrochureEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Main'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();


    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Main';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });

                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('BrochureArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Architecture'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();


    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Architecture';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });
                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('BrochureComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Commerce'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();

    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Commerce';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });
                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('BrochureEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Engineering'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();

    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Engineering';
        $scope.newbrochure.from='Main';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });
                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('BrochurePubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Public'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();


    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Public';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });
                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('ContactArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Engineering'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();


    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Engineering';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('ContactComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Commerce'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();

    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Commerce';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('ContactEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Main'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();


    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Main';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('ContactEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Engineering'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();

    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Engineering';
        $scope.newcontact.from='Main';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('ContactPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Public'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();


    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Public';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('DashboardController', function($scope,$http,$modal,$interval){

    $scope.info=[];
   /* $http.get('apiweb/get_dashboard_info').
        success(function(data, status)
        {
            $scope.info=data;
            console.log(data);
        });*/
});

app.controller('DownloadArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Architecture'},function (downloads) {
            $scope.downloads = downloads;
        });
    }
    loadDownload();

    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Architecture';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('DownloadComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Commerce'},function (downloads) {
            $scope.downloads = downloads;
        });
    }

    loadDownload();


    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Commerce';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('DownloadEKCController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Main'},function (downloads) {
            $scope.downloads = downloads;
        });
    }

    loadDownload();

    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Main';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });
                        });*/
                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('DownloadEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Engineering'},function (downloads) {
            $scope.downloads = downloads;
        });
    }

    loadDownload();


    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Engineering';
        $scope.newdownload.from='Main';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('DownloadPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Public'},function (downloads) {
            $scope.downloads = downloads;
        });
    }

    loadDownload();


    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Public';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });

                        });*/
                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('GalleryArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Architecture'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;
    $scope.imagesviewgallary = false;


    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Architecture';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        // console.log($scope.newgallery.gallary_items);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };


    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get(   'apiweb/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('GalleryComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Commerce'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Commerce';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };
    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get('apiweb/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('GalleryEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Engineering'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Engineering';
        $scope.newgallery.from='Main';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };

    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get('apiweb/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

});

app.controller('GalleryPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Public'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Public';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get('apiweb/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('GalleryEKCController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Main'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Main';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });
            loadGallery();

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get('/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('HomeController', function($scope,$http,$location,$interval,UserInfo){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "activemenu" : "";
    };

    $scope.user = {};

    $scope.showEKC=false;
    $scope.showENG=false;
    $scope.showARC=false;
    $scope.showCOM=false;
    $scope.showPUB=false;
    $scope.showDash=false;

    UserInfo.query().success(function(data){
        $scope.user = data;
        checkExist(data);
        console.log(data);
    });

    function checkExist(user){
        var menues=user.menu;
        var length=menues.length;
        for(var i=0;i<length;i++){
            switch (menues[i].type){
                case 'EKCWebsite':
                    $scope.showEKC=true;
                    break;
                case 'Engineering':
                    $scope.showENG=true;
                    break;
                case 'Architecture':
                    $scope.showARC=true;
                    break;
                case 'Commerce':
                    $scope.showCOM=true;
                    break;
                case 'Public':
                    $scope.showPUB=true;
                    break;
                default:
                    $scope.showDash=true;

            }
        }
    }






    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100,500];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 100;
    /* Nav menu */

});


app.directive('clickAnywhereButHere', function($document){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function() {
                // magic here.
                scope.$apply(attr.clickAnywhereButHere);
            })
        }
    }
});
app.controller('NewsArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Architecture'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();

    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Architecture';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
      //  console.log(curNews);
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Saved Successfully');
            });
        }

        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('NewsComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Commerce'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();


    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Commerce';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('News Saved Successfully');
            });
        }
        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('NewsEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Main'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();


    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Main';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Saved Successfully');
            });
        }

        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('NewsEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Engineering'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();


    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Engineering';
        $scope.newnews.from='Main';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Saved Successfully');
            });
        }

        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('NewsPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Public'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();


    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Public';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Saved Successfully');
            });
        }

        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });
                        });*/
                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});


app.controller('NotificationArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Architecture'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Architecture';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        //console.log($scope.curNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Updated Successfully');
            });
        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('NotificationComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Commerce'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Commerce';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Notification Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('NotificationEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Main'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Main';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Notification Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('NotificationPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Public'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Public';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Notification Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('NotificationEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Engineering'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Engineering';
        $scope.newnotification.from='Main';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Notification Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('PopupArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Architecture'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Architecture';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/popup/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('PopupComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Commerce'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Commerce';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/popup/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('PopupEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Main'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Main';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('PopupEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Engineering'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Engineering';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/popup/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});


app.controller('PopupPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Public'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Public';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/popup/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('SliderArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Architecture'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Architecture';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('SliderComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Commerce'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Commerce';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('SliderEKCController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Main'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Main';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('SliderEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Engineering'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Engineering';
        $scope.newslider.from='Main';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('SliderPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Public'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Public';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('TeamsArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Architecture'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Architecture';
        $scope.curTeam = {};
    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Updated Successfully');
            });

        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                $scope.newteam = {};
                ngNotify.set('Team Saved Successfully');

            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        angular.element("input[type='file']").val(null);
        //loadTeam();
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };


    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }



});

app.controller('TeamsComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Commerce'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Commerce';
        $scope.curTeam = {};

    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Team Updated Successfully');
            });

        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Saved Successfully');
            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        angular.element("input[type='file']").val(null);
        //loadTeam();
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });

                        });*/
                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }
});

app.controller('TeamsEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Engineering'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Engineering';
        $scope.newteam.from='Main';
        $scope.curTeam = {};
    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Updated Successfully');
            });

        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Saved Successfully');
            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        angular.element("input[type='file']").val(null);
        //loadTeam();
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }
});

app.controller('TeamsPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Public'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Public';
        $scope.curTeam = {};
    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        console.log(curTeam);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Team Updated Successfully');
            });
        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Saved Successfully');
            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        angular.element("input[type='file']").val(null);
        //loadTeam();
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };

    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }



});

app.controller('TeamsEKCController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Main'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Main';
        $scope.curTeam = {};
    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Updated Successfully');
            });

        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Saved Successfully');
            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        //loadTeam();
        angular.element("input[type='file']").val(null);
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }
});

app.controller('TestimonialArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Architecture'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Architecture';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('TestimonialComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Commerce'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Commerce';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                       /* item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('TestimonialEKCController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Main'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Main';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('TestimonialEngController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Engineering'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Engineering';
        $scope.newtestimonial.from='Main';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('TestimonialPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Public'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Public';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});

app.controller('UserController', function($scope,$http,$filter,$anchorScroll, $ngConfirm, ngNotify, User,UserInfo){
    $scope.users = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadUser() {
        $scope.users = [];
        User.query(function (users) {
            $scope.users = users;
        });
    }

    loadUser();

    $scope.useredit = false;

    $scope.newUser = function () {
        $scope.useredit = true;
        $scope.newuser = new User();
        $scope.newuser.isAdmin=0;
        $scope.newuser.enrollment_date=formatDate(new Date());
        $scope.curUser = {};
    };

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }

    $scope.editUser = function (thisFlash) {
        $scope.useredit = true;
        $scope.curUser = thisFlash;
        $scope.newuser = angular.copy(thisFlash);
        $scope.newuser.enrollment_date=formatDate(thisFlash.enrollment_date);
        if($scope.newuser.isleaved==0) {
            $scope.newuser.leave_date = formatDate(new Date());
            $scope.newuser.isleaved=false;
        }
        else {
            $scope.newuser.leave_date = formatDate($scope.newuser.leave_date);
            $scope.newuser.isleaved=true;
        }
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelUser = function () {
        $scope.useredit = false;
        $scope.newuser = new User();
    };

    $scope.addUser = function () {
        if ($scope.curUser.id) {
            $scope.newuser.$update(function (flash) {
                angular.extend($scope.curUser, $scope.curUser, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Updated Successfully');
            });

        } else {
            $scope.newuser.$save(function (users) {
                $scope.users.push(users);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Saved Successfully');
            });
        }

        $scope.useredit = false;
        $scope.newuser = new User();
        //loadUser();
    };

    $scope.deleteUser = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.users.indexOf(item);
                            $scope.users.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'User Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/user/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.users.indexOf(item);
                                $scope.users.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'User Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        $scope.showUser = false;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
        $scope.showUser = true;
        $scope.showHostel=false;
        $scope.showMess=false;
    };

    $scope.savePermission = function (ev){
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Would you like to Reset Permission ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function(scope){

                        UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                            .success(function(data){
                                $ngConfirm({theme: 'dark',buttons: {ok: {text: "Ok!",btnClass: 'btn-primary',keys: ['enter']}},content:'Permission Changed Successfully'});

                                $scope.resetPermission = false;
                                $scope.backToUsers();
                            });
                    }
                },
                // short hand button definition
                close: function(scope){
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };
});
app.controller('VideoArcController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Engineering'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();


    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Engineering';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('VideoComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Commerce'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();

    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Commerce';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('VideoEKCController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Main'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();


    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Main';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
app.controller('VideoEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Engineering'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();

    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Engineering';
        $scope.newvideo.from='Main';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});

app.controller('VideoPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Public'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();


    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Public';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('AdmissionService',[]).factory('Admission',['$resource',
    function($resource){
        return $resource('/apiweb/admission/:admissionId', {
            admissionId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('BrochureService',[]).factory('Brochure',['$resource',
    function($resource){
        return $resource('/apiweb/brochure/:brochureId', {
            brochureId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/apiweb/contacts/:contactsId', {
            contactsId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('DownloadService',[]).factory('Download',['$resource',
    function($resource){
        return $resource('/apiweb/download/:downloadId', {
            downloadId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('GalleryService',[]).factory('Gallery',['$resource',
    function($resource){
        return $resource('/apiweb/gallery/:galleryId', {
            galleryId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('NewsService',[]).factory('News',['$resource',
    function($resource){
        return $resource('/apiweb/news/:newsId', {
            newsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
angular.module('NotificationService',[]).factory('Notification',['$resource',
    function($resource){
        return $resource('/apiweb/notification/:notificationId', {
           notificationId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
angular.module('PopupService',[]).factory('Popup',['$resource',
    function($resource){
        return $resource('/apiweb/popup/:popupId', {
            popupId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('SliderService',[]).factory('Slider',['$resource',
    function($resource){
        return $resource('/apiweb/slider/:sliderId', {
            sliderId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('TeamService',[]).factory('Team',['$resource',
    function($resource){
        return $resource('/apiweb/teams/:teamsId', {
            teamsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('TestimonialService',[]).factory('Testimonial',['$resource',
    function($resource){
        return $resource('/apiweb/testimonials/:testimonialsId', {
            testimonialsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 11/05/2017.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
    var UserInfo = {};

    UserInfo.query=function(){
        return $http.get('/apiweb/userinfo')
    };

    UserInfo.get = function(id){
        return $http.get('/apiweb/user/'+id);
    };

    UserInfo.save = function(id,data){
        return $http.put('/apiweb/userinfo/'+id,data);
    };

    UserInfo.getAllPage=function(id){
        return $http.get('/apiweb/getAllPages/'+id);
    };

    UserInfo.getUserPermission=function(userId){
        return $http.get('/apiweb/getPermission/?id='+userId);
    };

    UserInfo.setUserPermission = function(data){
        return $http.post('/apiweb/changePermission',data);
    };

    return UserInfo;
}
]);

/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/apiweb/user/:userId', {
            userId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('VideoService',[]).factory('Video',['$resource',
    function($resource){
        return $resource('/apiweb/videos/:videosId', {
            videosId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);