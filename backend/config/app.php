<?php

return [

    'pages' => [
        [
            'title' => 'Dashboard',
            'name' => 'Dashboard',
            'route' => '/',
            'type' => 'Dashboard',
            'alias' => 'dashboard',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Teams',
            'name' => 'Teams',
            'route' => 'teamsekc',
            'type' => 'EKCWebsite',
            'alias' => 'teamsekc',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Testimonials',
            'name' => 'Testimonials',
            'route' => 'testimonialsekc',
            'type' => 'EKCWebsite',
            'alias' => 'testimonialsekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Sliders',
            'name' => 'Sliders',
            'route' => 'slidersekc',
            'type' => 'EKCWebsite',
            'alias' => 'slidersekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Downloads',
            'name' => 'Downloads',
            'route' => 'downloadsekc',
            'type' => 'EKCWebsite',
            'alias' => 'downloadsekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'News & Events',
            'name' => 'News & Events',
            'route' => 'newsekc',
            'type' => 'EKCWebsite',
            'alias' => 'newsekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Gallery',
            'name' => 'Gallery',
            'route' => 'galleryekc',
            'type' => 'EKCWebsite',
            'alias' => 'galleryekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Videos',
            'name' => 'Videos',
            'route' => 'videosekc',
            'type' => 'EKCWebsite',
            'alias' => 'videosekc',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Brochures',
            'name' => 'Brochures',
            'route' => 'brochuresekc',
            'type' => 'EKCWebsite',
            'alias' => 'brochuresekc',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Notifications',
            'name' => 'Notifications',
            'route' => 'notificationsekc',
            'type' => 'EKCWebsite',
            'alias' => 'notificationsekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Popup',
            'name' => 'Popup',
            'route' => 'popupekc',
            'type' => 'EKCWebsite',
            'alias' => 'popupekc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Contact Us',
            'name' => 'Contact Us',
            'route' => 'contactsekc',
            'type' => 'EKCWebsite',
            'alias' => 'contactsekc',
            'icon' => 'ti-dashboard'
        ],

        //College of Engineering
        [
            'title' => 'Teams',
            'name' => 'Teams',
            'route' => 'teamseng',
            'type' => 'Engineering',
            'alias' => 'teamseng',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Testimonials',
            'name' => 'Testimonials',
            'route' => 'testimonialseng',
            'type' => 'Engineering',
            'alias' => 'testimonialseng',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Sliders',
            'name' => 'Sliders',
            'route' => 'sliderseng',
            'type' => 'Engineering',
            'alias' => 'sliderseng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Downloads',
            'name' => 'Downloads',
            'route' => 'downloadseng',
            'type' => 'Engineering',
            'alias' => 'downloadseng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'News & Events',
            'name' => 'News & Events',
            'route' => 'newseng',
            'type' => 'Engineering',
            'alias' => 'newseng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Gallery',
            'name' => 'Gallery',
            'route' => 'galleryeng',
            'type' => 'Engineering',
            'alias' => 'galleryeng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Videos',
            'name' => 'Videos',
            'route' => 'videoseng',
            'type' => 'Engineering',
            'alias' => 'videoseng',
            'icon' => 'ti-dashboard'
        ],
       /* [
            'title' => 'Brochures',
            'name' => 'Brochures',
            'route' => 'brochureseng',
            'type' => 'Engineering',
            'alias' => 'brochureseng',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Notifications',
            'name' => 'Notifications',
            'route' => 'notificationseng',
            'type' => 'Engineering',
            'alias' => 'notificationseng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Popup',
            'name' => 'Popup',
            'route' => 'popupeng',
            'type' => 'Engineering',
            'alias' => 'popupeng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Contact Us',
            'name' => 'Contact Us',
            'route' => 'contactseng',
            'type' => 'Engineering',
            'alias' => 'contactseng',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Admission',
            'name' => 'Admission',
            'route' => 'admissioneng',
            'type' => 'Engineering',
            'alias' => 'admissioneng',
            'icon' => 'ti-dashboard'
        ],


        //College of Architecture
        [
            'title' => 'Teams',
            'name' => 'Teams',
            'route' => 'teamsarc',
            'type' => 'Architecture',
            'alias' => 'teamsarch',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Testimonials',
            'name' => 'Testimonials',
            'route' => 'testimonialsarc',
            'type' => 'Architecture',
            'alias' => 'testimonialsarc',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Sliders',
            'name' => 'Sliders',
            'route' => 'slidersarc',
            'type' => 'Architecture',
            'alias' => 'slidersarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Downloads',
            'name' => 'Downloads',
            'route' => 'downloadsarc',
            'type' => 'Architecture',
            'alias' => 'downloadsarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'News & Events',
            'name' => 'News & Events',
            'route' => 'newsarc',
            'type' => 'Architecture',
            'alias' => 'newsarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Gallery',
            'name' => 'Gallery',
            'route' => 'galleryarc',
            'type' => 'Architecture',
            'alias' => 'galleryarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Videos',
            'name' => 'Videos',
            'route' => 'videosarc',
            'type' => 'Architecture',
            'alias' => 'videosarc',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Brochures',
            'name' => 'Brochures',
            'route' => 'brochuresarc',
            'type' => 'Architecture',
            'alias' => 'brochuresarc',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Notifications',
            'name' => 'Notifications',
            'route' => 'notificationsarc',
            'type' => 'Architecture',
            'alias' => 'notificationsarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Popup',
            'name' => 'Popup',
            'route' => 'popuparc',
            'type' => 'Architecture',
            'alias' => 'popuparc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Contact Us',
            'name' => 'Contact Us',
            'route' => 'contactsarc',
            'type' => 'Architecture',
            'alias' => 'contactsarc',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Admission',
            'name' => 'Admission',
            'route' => 'admissionarc',
            'type' => 'Architecture',
            'alias' => 'admissionarc',
            'icon' => 'ti-dashboard'
        ],

        //College of Commerce
       [
            'title' => 'Teams',
            'name' => 'Teams',
            'route' => 'teamscom',
            'type' => 'Commerce',
            'alias' => 'teamscom',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Testimonials',
            'name' => 'Testimonials',
            'route' => 'testimonialscom',
            'type' => 'Commerce',
            'alias' => 'testimonialscom',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Sliders',
            'name' => 'Sliders',
            'route' => 'sliderscom',
            'type' => 'Commerce',
            'alias' => 'sliderscom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Downloads',
            'name' => 'Downloads',
            'route' => 'downloadscom',
            'type' => 'Commerce',
            'alias' => 'downloadscom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'News & Events',
            'name' => 'News & Events',
            'route' => 'newscom',
            'type' => 'Commerce',
            'alias' => 'newscom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Gallery',
            'name' => 'Gallery',
            'route' => 'gallerycom',
            'type' => 'Commerce',
            'alias' => 'gallerycom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Videos',
            'name' => 'Videos',
            'route' => 'videoscom',
            'type' => 'Commerce',
            'alias' => 'videoscom',
            'icon' => 'ti-dashboard'
        ],
       /* [
            'title' => 'Brochures',
            'name' => 'Brochures',
            'route' => 'brochurescom',
            'type' => 'Commerce',
            'alias' => 'brochurescom',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Notifications',
            'name' => 'Notifications',
            'route' => 'notificationscom',
            'type' => 'Commerce',
            'alias' => 'notificationscom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Popup',
            'name' => 'Popup',
            'route' => 'popupcom',
            'type' => 'Commerce',
            'alias' => 'popupcom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Contact Us',
            'name' => 'Contact Us',
            'route' => 'contactscom',
            'type' => 'Commerce',
            'alias' => 'contactscom',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Admission',
            'name' => 'Admission',
            'route' => 'admissioncom',
            'type' => 'Commerce',
            'alias' => 'admissioncom',
            'icon' => 'ti-dashboard'
        ],

        //Public School
        [
            'title' => 'Teams',
            'name' => 'Teams',
            'route' => 'teamspub',
            'type' => 'Public',
            'alias' => 'teamspub',
            'icon' => 'ti-dashboard'
        ],
       /* [
            'title' => 'Testimonials',
            'name' => 'Testimonials',
            'route' => 'testimonialspub',
            'type' => 'Public',
            'alias' => 'testimonialspub',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Sliders',
            'name' => 'Sliders',
            'route' => 'sliderspub',
            'type' => 'Public',
            'alias' => 'sliderspub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Downloads',
            'name' => 'Downloads',
            'route' => 'downloadspub',
            'type' => 'Public',
            'alias' => 'downloadspub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'News & Events',
            'name' => 'News & Events',
            'route' => 'newspub',
            'type' => 'Public',
            'alias' => 'newspub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Gallery',
            'name' => 'Gallery',
            'route' => 'gallerypub',
            'type' => 'Public',
            'alias' => 'gallerypub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Videos',
            'name' => 'Videos',
            'route' => 'videospub',
            'type' => 'Public',
            'alias' => 'videospub',
            'icon' => 'ti-dashboard'
        ],
        /*[
            'title' => 'Brochures',
            'name' => 'Brochures',
            'route' => 'brochurespub',
            'type' => 'Public',
            'alias' => 'brochurespub',
            'icon' => 'ti-dashboard'
        ],*/
        [
            'title' => 'Notifications',
            'name' => 'Notifications',
            'route' => 'notificationspub',
            'type' => 'Public',
            'alias' => 'notificationspub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Popup',
            'name' => 'Popup',
            'route' => 'popuppub',
            'type' => 'Public',
            'alias' => 'popuppub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Contact Us',
            'name' => 'Contact Us',
            'route' => 'contactspub',
            'type' => 'Public',
            'alias' => 'contactspub',
            'icon' => 'ti-dashboard'
        ],
        [
            'title' => 'Admission',
            'name' => 'Admission',
            'route' => 'admissionpub',
            'type' => 'Public',
            'alias' => 'admissionpub',
            'icon' => 'ti-dashboard'
        ],

    ],
    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        PragmaRX\Tracker\Vendor\Laravel\ServiceProvider::class,


        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Image' => Intervention\Image\Facades\Image::class,
        'Tracker' => PragmaRX\Tracker\Vendor\Laravel\Facade::class,

    ],

];
