<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        return view('app.index');
    }else{
        return view('frontend.index');
    }
});

Route::get('/test', function () {
    $mail_data['name']='test';
     Mail::send('email.contactform', $mail_data, function ($message) use ($mail_data) {
            $message->to('shammaspk3@gmail.com', 'EKC Web')->subject('Contact from web');
        });
});


Route::get('images/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'image/jpeg');
    }
    else{
        return response(['error' => 'File not found'], 200);
    }
    //return base64_encode($file);
});

Route::get('pdf/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'application/pdf');
    }
    else{
        return response(['error' => 'File not found'], 200);
    }
    //return base64_encode($file);
});

Route::get('doc/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'application/msword');
    }
    else{
        return response(['error' => 'File not found'], 200);
    }
    //return base64_encode($file);
});

Route::get('excel/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
    }
    else{
        return response(['error' => 'File not found'], 200);
    }
    //return base64_encode($file);
});




//EKC FRONTEND ROUTES...................

//EKC Website
Route::get('myhome','PageController@showHomePage');
Route::get('institutions','PageController@showInstitutionsPage');
Route::get('about','PageController@showAboutPage');
Route::get('concept','PageController@showConceptPage');
Route::get('governingbody','PageController@showGoverningbodyPage');
Route::get('directors','PageController@showDirectorsPage');
Route::get('principal','PageController@showPrincipalPage');
Route::get('admission','PageController@showAdmissionPage');
Route::get('placements','PageController@showPlacementsPage');
Route::get('facilities','PageController@showFacilitiesPage');
Route::get('updates','PageController@showUpdatesPage');
Route::get('photos','PageController@showPhotosPage');
Route::get('videos','PageController@showVideosPage');
Route::get('contact','PageController@showContactPage');
Route::get('online-grievance','PageController@showOnlinegrievancePage');
Route::post('postcontact',['as'=>'postcontact','uses'=>'PageController@postContact']);

Route::get('liberalarts','PageController@showLiberalartsPage');

Route::get('scholarships','PageController@showScholarshipsPage');
Route::get('career','PageController@showCareerPage');
Route::get('360','PageController@show360Page');
Route::get('brochure','PageController@showBrochurePage');
Route::post('online-application','PageController@onlineApplication');


Route::group(['prefix'=>'EKC-TC'],function(){
    Route::get('/','PageController@showTCIndexPage');
    Route::get('index','PageController@showTCIndexPage');
    Route::get('about','PageController@showTCAboutPage');
    Route::get('governingbody','PageController@showTCGoverningbodyPage');
    Route::get('directors','PageController@showTCDirectorsPage');
    Route::get('principal','PageController@showTCPrincipalPage');
    Route::get('administrative-team','PageController@showTCAdministrativeTeamPage');
    Route::get('committees','PageController@showTCCommitteesPage');
    Route::get('updates','PageController@showTCUpdatesPage');
    Route::get('photos','PageController@showTCPhotosPage');
    Route::get('videos','PageController@showTCVideosPage');
    Route::get('contact','PageController@showTCContactPage');
    Route::get('admission','PageController@showTCAdmissionPage');
    Route::get('facilities','PageController@showTCFacilitiesPage');
    Route::get('downloads','PageController@showTCDownloadsPage');
    Route::get('life','PageController@showTCLifePage');
    Route::get('career','PageController@showTCCareerPage');

    Route::get('KBAIC_105','PageController@showTCKBAIC_105Page');
    

    Route::group(['prefix'=>'civil'],function(){
        Route::get('/','PageController@showTCCivilIndexPage');
        Route::get('index','PageController@showTCCivilIndexPage');
        Route::get('about','PageController@showTCCivilAboutPage');
        Route::get('profile','PageController@showTCCivilProfilePage');
        Route::get('HOD-desk','PageController@showTCCivilHOD_deskPage');
        Route::get('vision-mission','PageController@showTCCivilVisionMissionPage');
        Route::get('peospos','PageController@showTCCivilPeosposPage');
        Route::get('staff','PageController@showTCCivilStaffPage');
        Route::get('career','PageController@showTCCivilCareerPage');
        Route::get('admission','PageController@showTCCivilAdmissionPage');
        Route::get('facilities','PageController@showTCCivilFacilitiesPage');
        Route::get('downloads','PageController@showTCCivilDownloadsPage');
        Route::get('updates','PageController@showTCCivilUpdatesPage');
        Route::get('photos','PageController@showTCCivilPhotosPage');
        Route::get('videos','PageController@showTCCivilVideosPage');
        Route::get('contact','PageController@showTCCivilContactPage');
    });

    Route::group(['prefix'=>'mechanical'],function(){
        Route::get('/','PageController@showTCMechanicalIndexPage');
        Route::get('index','PageController@showTCMechanicalIndexPage');
        Route::get('about','PageController@showTCMechanicalAboutPage');
        Route::get('profile','PageController@showTCMechanicalProfilePage');
        Route::get('HOD-desk','PageController@showTCMechanicalHOD_deskPage');
        Route::get('vision-mission','PageController@showTCMechanicalVisionMissionPage');
        Route::get('peospos','PageController@showTCMechanicalPeosposPage');
        Route::get('staff','PageController@showTCMechanicalStaffPage');
        Route::get('career','PageController@showTCMechanicalCareerPage');
        Route::get('admission','PageController@showTCMechanicalAdmissionPage');
        Route::get('facilities','PageController@showTCMechanicalFacilitiesPage');
        Route::get('downloads','PageController@showTCMechanicalDownloadsPage');
        Route::get('updates','PageController@showTCMechanicalUpdatesPage');
        Route::get('photos','PageController@showTCMechanicalPhotosPage');
        Route::get('videos','PageController@showTCMechanicalVideosPage');
        Route::get('contact','PageController@showTCMechanicalContactPage');
    });

    Route::group(['prefix'=>'electronic'],function(){
        Route::get('/','PageController@showTCElectronicIndexPage');
        Route::get('index','PageController@showTCElectronicIndexPage');
        Route::get('about','PageController@showTCElectronicAboutPage');
        Route::get('profile','PageController@showTCElectronicProfilePage');
        Route::get('HOD-desk','PageController@showTCElectronicHOD_deskPage');
        Route::get('vision-mission','PageController@showTCElectronicVisionMissionPage');
        Route::get('peospos','PageController@showTCElectronicPeosposPage');
        Route::get('staff','PageController@showTCElectronicStaffPage');
        Route::get('career','PageController@showTCElectronicCareerPage');
        Route::get('admission','PageController@showTCElectronicAdmissionPage');
        Route::get('facilities','PageController@showTCElectronicFacilitiesPage');
        Route::get('downloads','PageController@showTCElectronicDownloadsPage');
        Route::get('updates','PageController@showTCElectronicUpdatesPage');
        Route::get('photos','PageController@showTCElectronicPhotosPage');
        Route::get('videos','PageController@showTCElectronicVideosPage');
        Route::get('contact','PageController@showTCElectronicContactPage');
    });

    Route::group(['prefix'=>'computer'],function(){
        Route::get('/','PageController@showTCComputerIndexPage');
        Route::get('index','PageController@showTCComputerIndexPage');
        Route::get('about','PageController@showTCComputerAboutPage');
        Route::get('profile','PageController@showTCComputerProfilePage');
        Route::get('HOD-desk','PageController@showTCComputerHOD_deskPage');
        Route::get('vision-mission','PageController@showTCComputerVisionMissionPage');
        Route::get('peospos','PageController@showTCComputerPeosposPage');
        Route::get('staff','PageController@showTCComputerStaffPage');
        Route::get('career','PageController@showTCComputerCareerPage');
        Route::get('admission','PageController@showTCComputerAdmissionPage');
        Route::get('facilities','PageController@showTCComputerFacilitiesPage');
        Route::get('downloads','PageController@showTCComputerDownloadsPage');
        Route::get('updates','PageController@showTCComputerUpdatesPage');
        Route::get('photos','PageController@showTCComputerPhotosPage');
        Route::get('videos','PageController@showTCComputerVideosPage');
        Route::get('contact','PageController@showTCComputerContactPage');
    });

    Route::group(['prefix'=>'safety'],function(){
        Route::get('/','PageController@showTCSafetyIndexPage');
        Route::get('index','PageController@showTCSafetyIndexPage');
        Route::get('about','PageController@showTCSafetyAboutPage');
        Route::get('profile','PageController@showTCSafetyProfilePage');
        Route::get('HOD-desk','PageController@showTCSafetyHOD_deskPage');
        Route::get('vision-mission','PageController@showTCSafetyVisionMissionPage');
        Route::get('peospos','PageController@showTCSafetyPeosposPage');
        Route::get('staff','PageController@showTCSafetyStaffPage');
        Route::get('career','PageController@showTCSafetyCareerPage');
        Route::get('admission','PageController@showTCSafetyAdmissionPage');
        Route::get('facilities','PageController@showTCSafetyFacilitiesPage');
        Route::get('downloads','PageController@showTCSafetyDownloadsPage');
        Route::get('updates','PageController@showTCSafetyUpdatesPage');
        Route::get('photos','PageController@showTCSafetyPhotosPage');
        Route::get('videos','PageController@showTCSafetyVideosPage');
        Route::get('contact','PageController@showTCSafetyContactPage');
    });

    Route::group(['prefix'=>'humanities'],function(){
        Route::get('/','PageController@showTCHumanitiesIndexPage');
        Route::get('index','PageController@showTCHumanitiesIndexPage');
        Route::get('about','PageController@showTCHumanitiesAboutPage');
        Route::get('profile','PageController@showTCHumanitiesProfilePage');
        Route::get('HOD-desk','PageController@showTCHumanitiesHOD_deskPage');
        Route::get('vision-mission','PageController@showTCHumanitiesVisionMissionPage');
        Route::get('peospos','PageController@showTCHumanitiesPeosposPage');
        Route::get('staff','PageController@showTCHumanitiesStaffPage');
        Route::get('career','PageController@showTCHumanitiesCareerPage');
        Route::get('admission','PageController@showTCHumanitiesAdmissionPage');
        Route::get('facilities','PageController@showTCHumanitiesFacilitiesPage');
        Route::get('downloads','PageController@showTCHumanitiesDownloadsPage');
        Route::get('updates','PageController@showTCHumanitiesUpdatesPage');
        Route::get('photos','PageController@showTCHumanitiesPhotosPage');
        Route::get('videos','PageController@showTCHumanitiesVideosPage');
        Route::get('contact','PageController@showTCHumanitiesContactPage');
    });
});


Route::group(['prefix'=>'EKC-COA'],function(){
    Route::get('/','PageController@showCOAIndexPage');
    Route::get('index','PageController@showCOAIndexPage');
    Route::get('about','PageController@showCOAAboutPage');
    Route::get('governingbody','PageController@showCOAGoverningbodyPage');
    Route::get('directors','PageController@showCOADirectorsPage');
    Route::get('principal','PageController@showCOAPrincipalPage');
    Route::get('HOD-desk','PageController@showCOAHODdeskPage');
    Route::get('administrative-team','PageController@showCOAAdministrativeTeamPage');
    Route::get('staff','PageController@showCOAStaffPage');
    Route::get('team','PageController@showCOATeamPage');
    Route::get('career','PageController@showCOACareerPage');
    Route::get('academics','PageController@showCOAAcademicsPage');
    Route::get('curricular-activity','PageController@showCOACurricularActivityPage');
    Route::get('admission','PageController@showCOAAdmissionPage');
    Route::get('facilities','PageController@showCOAFacilitiesPage');
    Route::get('downloads','PageController@showCOADownloadsPage');
    Route::get('updates','PageController@showCOAUpdatesPage');
    Route::get('photos','PageController@showCOAPhotosPage');
    Route::get('videos','PageController@showCOAVideosPage');
    Route::get('contact','PageController@showCOAContactPage');
});

Route::group(['prefix'=>'EKC-CS'],function(){
    Route::get('/','PageController@showCSIndexPage');
    Route::get('index','PageController@showCSIndexPage');
    Route::get('about','PageController@showCSAboutPage');
    Route::get('governingbody','PageController@showCSGoverningbodyPage');
    Route::get('directors','PageController@showCSDirectorsPage');
    Route::get('principal','PageController@showCSPrincipalPage');
    Route::get('administrative-team','PageController@showCSAdministrativeTeamPage');
    Route::get('committees','PageController@showCSCommitteesPage');
    Route::get('career','PageController@showCSCareerPage');
    Route::get('admission','PageController@showCSAdmissionPage');
    Route::get('facilities','PageController@showCSFacilitiesPage');
    Route::get('downloads','PageController@showCSDownloadsPage');
    Route::get('life','PageController@showCSLifePage');
    Route::get('updates','PageController@showCSUpdatesPage');
    Route::get('photos','PageController@showCSPhotosPage');
    Route::get('videos','PageController@showCSVideosPage');
    Route::get('contact','PageController@showCSContactPage');
    
    Route::get('laungage','PageController@showCSlaungagePage');


     Route::group(['prefix'=>'bba'],function(){
        Route::get('/','PageController@showCSBBAIndexPage');
        Route::get('index','PageController@showCSBBAIndexPage');
        Route::get('about','PageController@showCSBBAAboutPage');
        Route::get('profile','PageController@showCSBBAProfilePage');
        Route::get('HOD-desk','PageController@showCSBBAHOD_deskPage');
        Route::get('staff','PageController@showCSBBAStaffPage');
        Route::get('career','PageController@showCSBBACareerPage');
        Route::get('admission','PageController@showCSBBAAdmissionPage');
        Route::get('facilities','PageController@showCSBBAFacilitiesPage');
        Route::get('downloads','PageController@showCSBBADownloadsPage');
        Route::get('updates','PageController@showCSBBAUpdatesPage');
        Route::get('photos','PageController@showCSBBAPhotosPage');
        Route::get('videos','PageController@showCSBBAVideosPage');
        Route::get('contact','PageController@showCSBBAContactPage');
    });

    Route::group(['prefix'=>'BCOMFinance'],function(){
        Route::get('/','PageController@showCSBCOMFinanceIndexPage');
        Route::get('index','PageController@showCSBCOMFinanceIndexPage');
        Route::get('about','PageController@showCSBCOMFinanceAboutPage');
        Route::get('profile','PageController@showCSBCOMFinanceProfilePage');
        Route::get('HOD-desk','PageController@showCSBCOMFinanceHOD_deskPage');
        Route::get('staff','PageController@showCSBCOMFinanceStaffPage');
        Route::get('career','PageController@showCSBCOMFinanceCareerPage');
        Route::get('admission','PageController@showCSBCOMFinanceAdmissionPage');
        Route::get('facilities','PageController@showCSBCOMFinanceFacilitiesPage');
        Route::get('downloads','PageController@showCSBCOMFinanceDownloadsPage');
        Route::get('updates','PageController@showCSBCOMFinanceUpdatesPage');
        Route::get('photos','PageController@showCSBCOMFinancePhotosPage');
        Route::get('videos','PageController@showCSBCOMFinanceVideosPage');
        Route::get('contact','PageController@showCSBCOMFinanceContactPage');
    });

    Route::group(['prefix'=>'BSCCS'],function(){
        Route::get('/','PageController@showCSBSCCSIndexPage');
        Route::get('index','PageController@showCSBSCCSIndexPage');
        Route::get('about','PageController@showCSBSCCSAboutPage');
        Route::get('profile','PageController@showCSBSCCSProfilePage');
        Route::get('HOD-desk','PageController@showCSBSCCSHOD_deskPage');
        Route::get('staff','PageController@showCSBSCCSStaffPage');
        Route::get('career','PageController@showCSBSCCSCareerPage');
        Route::get('admission','PageController@showCSBSCCSAdmissionPage');
        Route::get('facilities','PageController@showCSBSCCSFacilitiesPage');
        Route::get('downloads','PageController@showCSBSCCSDownloadsPage');
        Route::get('updates','PageController@showCSBSCCSUpdatesPage');
        Route::get('photos','PageController@showCSBSCCSPhotosPage');
        Route::get('videos','PageController@showCSBSCCSVideosPage');
        Route::get('contact','PageController@showCSBSCCSContactPage');
    });

    Route::group(['prefix'=>'BAMCAJ'],function(){
        Route::get('/','PageController@showCSBAMCAJIndexPage');
        Route::get('index','PageController@showCSBAMCAJIndexPage');
        Route::get('about','PageController@showCSBAMCAJAboutPage');
        Route::get('profile','PageController@showCSBAMCAJProfilePage');
        Route::get('HOD-desk','PageController@showCSBAMCAJHOD_deskPage');
        Route::get('staff','PageController@showCSBAMCAJStaffPage');
        Route::get('career','PageController@showCSBAMCAJCareerPage');
        Route::get('admission','PageController@showCSBAMCAJAdmissionPage');
        Route::get('facilities','PageController@showCSBAMCAJFacilitiesPage');
        Route::get('downloads','PageController@showCSBAMCAJDownloadsPage');
        Route::get('updates','PageController@showCSBAMCAJUpdatesPage');
        Route::get('photos','PageController@showCSBAMCAJPhotosPage');
        Route::get('videos','PageController@showCSBAMCAJVideosPage');
        Route::get('contact','PageController@showCSBAMCAJContactPage');
    });

    Route::group(['prefix'=>'BCOMCORP'],function(){
        Route::get('/','PageController@showCSBCOMCORPIndexPage');
        Route::get('index','PageController@showCSBCOMCORPIndexPage');
        Route::get('about','PageController@showCSBCOMCORPAboutPage');
        Route::get('profile','PageController@showCSBCOMCORPProfilePage');
        Route::get('HOD-desk','PageController@showCSBCOMCORPHOD_deskPage');
        Route::get('staff','PageController@showCSBCOMCORPStaffPage');
        Route::get('career','PageController@showCSBCOMCORPCareerPage');
        Route::get('admission','PageController@showCSBCOMCORPAdmissionPage');
        Route::get('facilities','PageController@showCSBCOMCORPFacilitiesPage');
        Route::get('downloads','PageController@showCSBCOMCORPDownloadsPage');
        Route::get('updates','PageController@showCSBCOMCORPUpdatesPage');
        Route::get('photos','PageController@showCSBCOMCORPPhotosPage');
        Route::get('videos','PageController@showCSBCOMCORPVideosPage');
        Route::get('contact','PageController@showCSBCOMCORPContactPage');
    });

    Route::group(['prefix'=>'BCOMCA'],function(){
        Route::get('/','PageController@showCSBCOMCAIndexPage');
        Route::get('index','PageController@showCSBCOMCAIndexPage');
        Route::get('about','PageController@showCSBCOMCAAboutPage');
        Route::get('profile','PageController@showCSBCOMCAProfilePage');
        Route::get('HOD-desk','PageController@showCSBCOMCAHOD_deskPage');
        Route::get('staff','PageController@showCSBCOMCAStaffPage');
        Route::get('career','PageController@showCSBCOMCACareerPage');
        Route::get('admission','PageController@showCSBCOMCAAdmissionPage');
        Route::get('facilities','PageController@showCSBCOMCAFacilitiesPage');
        Route::get('downloads','PageController@showCSBCOMCADownloadsPage');
        Route::get('updates','PageController@showCSBCOMCAUpdatesPage');
        Route::get('photos','PageController@showCSBCOMCAPhotosPage');
        Route::get('videos','PageController@showCSBCOMCAVideosPage');
        Route::get('contact','PageController@showCSBCOMCAContactPage');
    });

    Route::group(['prefix'=>'MCOMFinance'],function(){
        Route::get('/','PageController@showCSMCOMFinanceIndexPage');
        Route::get('index','PageController@showCSMCOMFinanceIndexPage');
        Route::get('about','PageController@showCSMCOMFinanceAboutPage');
        Route::get('profile','PageController@showCSMCOMFinanceProfilePage');
        Route::get('HOD-desk','PageController@showCSMCOMFinanceHOD_deskPage');
        Route::get('staff','PageController@showCSMCOMFinanceStaffPage');
        Route::get('career','PageController@showCSMCOMFinanceCareerPage');
        Route::get('admission','PageController@showCSMCOMFinanceAdmissionPage');
        Route::get('facilities','PageController@showCSMCOMFinanceFacilitiesPage');
        Route::get('downloads','PageController@showCSMCOMFinanceDownloadsPage');
        Route::get('updates','PageController@showCSMCOMFinanceUpdatesPage');
        Route::get('photos','PageController@showCSMCOMFinancePhotosPage');
        Route::get('videos','PageController@showCSMCOMFinanceVideosPage');
        Route::get('contact','PageController@showCSMCOMFinanceContactPage');
    });

    
});

Route::group(['prefix'=>'EKC-SCHOOL'],function(){
    Route::get('/','PageController@showSCHIndexPage');
    Route::get('index','PageController@showSCHIndexPage');
    Route::get('about','PageController@showSCHAboutPage');
    Route::get('governingbody','PageController@showSCHGoverningbodyPage');
    Route::get('directors','PageController@showSCHDirectorsPage');
    Route::get('principal','PageController@showSCHPrincipalPage');
    Route::get('administrative-team','PageController@showSCHAdministrativeTeamPage');
    Route::get('staff','PageController@showSCHStaffPage');
    Route::get('career','PageController@showSCHCareerPage');
    Route::get('admission','PageController@showSCHAdmissionPage');
    Route::get('facilities','PageController@showSCHFacilitiesPage');
    Route::get('life','PageController@showSCHLifePage');
    Route::get('updates','PageController@showSCHUpdatesPage');
    Route::get('photos','PageController@showSCHPhotosPage');
    Route::get('videos','PageController@showSCHVideosPage');
    Route::get('contact','PageController@showSCHContactPage');
});



///TODO add middleware to authenticate
//Route::group(['prefix'=>'apiweb'],function() {
Route::group(['middleware'=>['auth'],'prefix'=>'apiweb'],function(){
    //Rest resources
    Route::resource('brochure', 'Auth\BrochureController');
    Route::resource('download', 'Auth\DownloadController');
    Route::resource('gallery', 'Auth\GallaryController');
    Route::resource('news', 'Auth\NewsController');
    Route::resource('notification', 'Auth\NotificationController');
    Route::resource('teams', 'Auth\TeamController');
    Route::resource('testimonials', 'Auth\TestimonialController');
    Route::resource('user', 'Auth\UserController');
    Route::resource('contact', 'Auth\ContactController');
    Route::resource('video', 'Auth\VideoController');
    Route::resource('slider', 'Auth\SliderController');
    Route::resource('popup', 'Auth\PopupController');
    Route::resource('admission', 'Auth\AdmissionController');

    //DELETE
    Route::get('slider/{id}', 'Auth\SliderController@destroy');
    Route::get('testimonials/{id}', 'Auth\TestimonialController@destroy');
    Route::get('download/{id}', 'Auth\DownloadController@destroy');
    Route::get('news/{id}', 'Auth\NewsController@destroy');
    Route::get('gallery/{id}', 'Auth\GallaryController@destroy');
    Route::get('video/{id}', 'Auth\VideoController@destroy');
    Route::get('notification/{id}', 'Auth\NotificationController@destroy');
    Route::get('contact/{id}', 'Auth\ContactController@destroy');
    Route::get('teams/{id}', 'Auth\TeamController@destroy');
    Route::get('popup/{id}', 'Auth\PopupController@destroy');
    Route::get('admission/{id}', 'Auth\AdmissionController@destroy');
    Route::get('user/{id}', 'Auth\UserController@destroy');

    //UPDATE
    Route::post('testimonials/{id}', 'Auth\TestimonialController@update');
    Route::post('download/{id}', 'Auth\DownloadController@update');
    Route::post('news/{id}', 'Auth\NewsController@update');
    Route::post('gallery/{id}', 'Auth\GallaryController@update');
    Route::post('video/{id}', 'Auth\VideoController@update');
    Route::post('notification/{id}', 'Auth\NotificationController@update');
    Route::post('contact/{id}', 'Auth\ContactController@update');
    Route::post('teams/{id}', 'Auth\TeamController@update');
    Route::post('popup/{id}', 'Auth\PopupController@update');
    Route::post('admission/{id}', 'Auth\AdmissionController@update');

    Route::post('changeOrder/{id}/{order}', 'Auth\TeamController@changeOrder');

    Route::get('userinfo',['as'=>'userinfo','uses'=>'Auth\UserInfoController@UserInfoController']);
    Route::get('getAllPages/{id}',['as'=>'getAllPages','uses'=>'Auth\UserInfoController@getAllPages']);
    Route::get('getPermission',['as'=>'getPermission','uses'=>'Auth\UserInfoController@getUserPermission']);
    Route::post('changePermission',['as'=>'setPermission','uses'=>'Auth\UserInfoController@setUserPermission']);

    Route::delete('gallery/delete-image/{id}', 'Auth\GallaryController@deleteImage');
    Route::get('gallery/delete-image/{id}', 'Auth\GallaryController@deleteImage');
    Route::get('get_dashboard_info', 'HomeController@getDashboardInfo');

});



//Load angular templates
//Route::group(['middleware'=>'auth'],function() {
Route::get('template/{name}', ['as' => 'templates', function ($name) {
    return view('app.' . $name);
}]);

Auth::routes();

Route::get('/homepage', 'HomeController@index')->name('home');


