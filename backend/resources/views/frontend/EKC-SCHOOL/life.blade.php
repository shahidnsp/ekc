@extends('frontend.EKC-SCHOOL.layout.app')

@section('content')
	<section class="padding-top-50px">
        <div class="container">
            <div class="text-center margin-bottom-20px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">The website in maintenance mode.</h1>
                <span class="text-capitalize">Plaese contact this number :<b> +91 9744-500-040</b></span>
            </div>
            <div class="row wow fadeInUp">
                <div  class="offset-3 col-md-6"><img src="{{url('frontend/img/workerman.jpg')}}" alt=""></div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection