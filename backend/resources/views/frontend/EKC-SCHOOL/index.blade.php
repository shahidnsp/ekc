@extends('frontend.EKC-SCHOOL.layout.app')

@section('content')

     <?php
            $sliders = \Illuminate\Support\Facades\Cache::remember('SCHsliders', 2*60, function() {
                return \App\Slider::where('department','Public')->get();
            });
        ?>
     <!-- =========== Revelution Slider =========== -->
    <div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                 @foreach($sliders as $slider)
                <li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>

    <?php
        $notifications=\App\Notification::where('department','Public')->get();
     ?>
     @if(count($notifications)>0)
    <section>
        <div class="breaking-news">
            <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                @foreach($notifications as $notification)
                <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                @endforeach
               {{-- <a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</a>
                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
                <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a>--}}
            </marquee>
        </div>
    </section>
    <br/><br/><br/>
    <br/><br/><br/>
    @endif

    <section>
        <div class="container">
        <div class="row">
            <div class="col-lg-4 wow fadeInUp">
                <div class="background-blue padding-top-45px pull-top-100px sm-pull-top-0px">
                    <div class="padding-lr-30px text-white padding-bottom-60px ">
                        <h2 class="margin-bottom-20px font-3">Welcome to EKC Public School</h2>
                        <p class="affiliated">CBSE Syllabus</p>
                        <p class="margin-bottom-20px">
                            To create an exemplary society with responsible citizen, who are Enlightened with values and empowered with Soft skill and life skill, and there by provide a steady stream of leaders to the nation Capable of being on the driving seat of the world.
                        </p>
                        <a href="{{url('EKC-SCHOOL/admission')}}" class="btn btn-sm border-2 border-radius-30 padding-lr-15px">Admission Details</a>
                    </div>
                    <div class="d-none d-lg-block"><img src="{{url('frontend/img/EKC-SC/EKC-Public School.jpg')}}" alt=""></div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="row margin-tb-20px">
                    <div class="col-lg-4 col-md-6 margin-bottom-30px wow fadeInUp">
                        <div class="text-center">
                            <div class="margin-lr-auto">
                                <i class="fa fa-eye icon-large text-pink"></i>
                            </div>
                            <h3 class="margin-tb-10px font-weight-700">
                                <span class="text-medium text-uppercase text-main-color">Our Vision</span>
                            </h3>
                            <p>
                                World class technology institutes on a welcoming campus with a unique college spirit,
                                which challenges and inspires all of our students to achieve their ultimate potential.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 margin-bottom-30px wow fadeInUp">
                        <div class="text-center">
                            <div class="margin-lr-auto">
                                <i class="fa fa-paper-plane icon-large text-yellow"></i>
                            </div>
                            <h3 class="margin-tb-10px font-weight-700">
                                <span class="text-medium text-uppercase text-main-color">Our Mission</span>
                            </h3>
                            <p>
                                To establish schools that nature the inherent genius in every child ,cultivate independent thinking skills, foster academic excellence and develop strong moral and ethical values and thereby molding and grooming confidents individuals who will be an asset to the world.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 margin-bottom-30px wow fadeInUp">
                        <div class="text-center">
                            <div class="margin-lr-auto">
                                <i class="fa fa-lightbulb icon-large text-green"></i>
                            </div>
                            <h3 class="margin-tb-10px font-weight-700">
                                <span class="text-medium text-uppercase text-main-color">Our Philosophy</span>
                            </h3>
                            <p>
                                 “Read, in the name of your lord, who created? He created man from an embryo.Read, and you Lord. Most Exalted. Teachers by means of the pen. He teaches man what he never knew”. (Quran96:1-5)
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p class="padding-lr-20px">
                        To establish schools that nature the inherent genius in every child ,cultivate independent thinking skills, foster academic excellence and develop strong moral and ethical values and thereby molding and grooming confidents individuals who will be an asset to the world.
                    </p>
                </div>

            </div>
        </div>
    </div>
    </section>

    <section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Facilities</h1>
                <span class="opacity-7">The First Knowledge City Concept In Kerala</span>
            </div>

            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Library.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-main-color">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Well Furnished Library
                            </h2>
                            <p>The EKC have a spacious well furnished Library having sufficient volume of books, National and International available.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Radio.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-green">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Campus Radio
                            </h2>
                            <p>Our Campus Radio station is equipped with advanced technology devices in order to attain international standards.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-yellow">
                        <div class="text-left z-index-2 position-relative text-black">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                General Store
                            </h2>
                            <p>A general store is functioning in the campus to supply quality books, notebooks and other stationary items at reasonable price.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Stores.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-pink">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Conveyance
                            </h2>
                            <p>College buses ply on a regular basis to the campus from Thirur, Perinthalmanna, Vengara, Mukkam, Nilambur, Valancheri, etc...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Transport.jpg')}}" alt="">
                </div>
            </div>

        </div>
    </section>

    <?php
        $newses = \Illuminate\Support\Facades\Cache::remember('SCHnewses', 2*60, function() {
            return \App\News::where('department','Public')->orderBy('id', 'desc')->take(4)->get();
        });
    ?>
    @if(count($newses)>0)
    <section class="padding-tb-50px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Latest News</h1>
                <span class="opacity-7">The First Knowledge City Concept In Kerala</span>
            </div>
            <div class="row">
                @foreach($newses as $news)
                <div class="col-lg-3 col-md-6 sm-mb-45px">
                    <div class="blog-item thum-hover border-radius-15 hidden background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date border-radius-15 z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                               {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                        <hr>
                        <div class="padding-lr-30px">
                            <small class="margin-right-30px">By : <a href="#">Admin</a></small>
                        </div>
                        <hr class="margin-bottom-0px border-white">
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    @endif

    <section>
        <div class="row no-gutters">
            <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                <div class="padding-30px">

                </div>
            </div>
            <div class="col-lg-4 background-blue">
                <div class="padding-lr-45px padding-tb-30px text-white">
                    <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                    <p>The College of Architecture is one among the institutions in the first phase of the Knowledge City Project in advancing educational standards throughout Kerala.</p>
                    <ul class="margin-0px padding-0px list-unstyled">
                        <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                        <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                        <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                        <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                        <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 background-green">
                <form class="dark-form padding-lr-45px padding-tb-30px">
                    <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6 text-black">
                            <label >Full Name</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6 text-black">
                            <label >Email Address</label>
                            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-black">
                        <label >Contact Number</label>
                        <input type="Number" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group text-black">
                        <label>Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <a href="#" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</a>
                </form>

            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection