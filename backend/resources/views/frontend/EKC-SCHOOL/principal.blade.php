@extends('frontend.EKC-SCHOOL.layout.app')

@section('content')
	<section class="padding-tb-50px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Principal's Message</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-5" src="{{url('frontend/img/peace-Principal.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Pro. Aboobacker Sidheeque</h3>
                        <div class="padding-top-10px">
                            <p>
                                Education is the process of making human personality through training its physical, emotional, intellectual, imaginative as well as spiritual aspect. We Peaceans work for the motto nurturing world class leaders, believing in the vision, bring out an exemplary society with responsible citizens who are enlightened with values and empowered with modern skills so as to provide leaders from the nation so capable of being on the driving seat of the world.
                            </p>
                            <p>
                                Education is the life long process where the learning happens all the time, not only in classrooms. Peace schools focus on 21st century skills by giving students the skills they need to succeed in this new world, and helping them grow the confidence to practice those skills. So we offer a distinctive curriculum which give focus on 21st century skills, rather focusing on content standards, we focus on performance standard which ensure the cognitive involvement of each and every child in the learning process. Project Based Learning approach help students and teachers to improve their skills and work with a clear ‘why’.
                            </p>
                            <p>
                                Our students are nurtured to be purpose oriented, empathetical, creative, reflective, cognisant, critical thinkers, collaborators, givers, strivers, and dot connectors. Our teachers are trained first and foremost to be learners. They are coached to be empathetic, collaborative, creative, innovative and tech savvy. They will be empowered with honesty, integrity, trustworthy, confidence, trustworthiness, commitment, inspiration and positive energy.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection