@extends('frontend.EKC-SCHOOL.layout.app')

@section('content')
        <div class="padding-tb-80px background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 sticky-content">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">Admission at Eranad Knowledge City</h1>
                        </div>
                        <h3>Overview<a href="{{url('frontend/download/EKCBrochure2018.pdf')}}" download class="buttonDownload pull-right">Download Brochure</a></h3>
                        <p class="coming-soon">
                            we will update soon
                        </p>
                        <h3>Eligibility</h3>
                        <p class="coming-soon">
                            we will update soon
                        </p>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Public School</th>
                                    <th>No of Seats</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>JKG</td>
                                    <td>40 Seats</td>
                                </tr>
                                <tr>
                                    <td>SKG</td>
                                    <td>40 Seats</td>
                                </tr>
                                <tr>
                                    <td>Grade 1</td>
                                    <td>40 Seats</td>
                                </tr>
                                <tr>
                                    <td>Grade 2</td>
                                    <td>40 Seats</td>
                                </tr>
                                <tr>
                                    <td>Grade 3</td>
                                    <td>40 Seats</td>
                                </tr>
                                <tr>
                                    <td>Grade 4</td>
                                    <td>40 Seats</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4 col-md-4 sticky-sidebar">
                        <div class="widget">
                            <h4 class="widget-title clearfix"><span>Contact Us</span></h4>
                            <h6 class="text-red">Phone :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9544-500-040</span>
                            <h6 class="margin-top-20px text-red">Admission Enquiry :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9446-009-824</span>
                            
                            <h6 class="margin-top-20px text-red">EKC - Public School :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9526-500-040</span>
                            <span class="d-block"><i class="fa fa-envelope text-main-color margin-right-10px" aria-hidden="true"></i> manjeri@peaceschool.com</span>

                            <h6 class="margin-top-20px">Address :</h6>
                            <span class="d-block"><i class="fa fa-map text-main-color margin-right-10px" aria-hidden="true"></i> Eranad Knowledge City, Cherukulam</span>
                            <span class="margin-left-30px">Manjeri Malappuram Dist</span>
                            <span class="margin-left-30px"><br/>Kerala PIN 676122 </span>
                            <h6 class="margin-top-20px">Email :</h6>
                            <span class="d-block"><i class="fa fa-envelope-open text-main-color margin-right-10px" aria-hidden="true"></i> office@eranadknowledgecity.com </span>
                            <h6 class="margin-top-20px">Locate Us :</h6>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62624.7163207098!2d76.09392957741646!3d11.184321113569137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba64a8eac9c7a6f%3A0x11439d8329094129!2sEranad+Knowledge+City+Technical+Campus!5e0!3m2!1sen!2sin!4v1553062313905" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

@endsection