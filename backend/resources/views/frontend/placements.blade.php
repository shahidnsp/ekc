@extends('frontend.layout.app')

@section('content')
    <div class="padding-tb-80px background-light-grey">
        <div class="container">
            <div class="row margin-bottom-30px">
                <div class="col-lg-8 col-md-8 sticky-content">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3">Eranad Knowledge City Placement Cell</h1>
                    </div>
                    <p class="margin-bottom-10px">
                        Our placement Cell is furnishing alliance with the zealot of the industry and the looming advanced startups. We deliver exposure to students in pioneer technical knowledge’s like block chain, data analytics etc.
                    </p>                    
                    <p class="margin-bottom-10px">
                        The Placement Cell of Eranad Knowledge City technical campus creates a platform where students can showcase their talents which different companies seek to explore and utilize. We have successfully placed graduates who have appeared for placements. This year especially, the performance has been overwhelming. <br/>There is a great deal of enthusiasm among final year students when the placement season starts off. Students come forward with their queries regarding companies, placement procedures and the Placement Cell members readily help them.
                    </p>                    
                    <p class="margin-bottom-10px">
                        This year has been extremely good for placements and with the economic growth reviving; we expect to invite even more companies next year. The support from the College has been extremely encouraging.
                    </p>                    
                </div>
                <div class="col-lg-4 col-md-4 sticky-sidebar">
                    <ul class="row padding-top-80px margin-0px list-unstyled">
                        <li class="col-lg-12 margin-bottom-30px">
                            <div class="with-hover">
                                <img src="{{url('frontend/img/placements/placements-1.jpg')}}" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <ul class="row padding-0px margin-0px list-unstyled">
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-2.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-2.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-3.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-3.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-4.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-4.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-5.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-5.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-6.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-6.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-7.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-7.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-8.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-8.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-9.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-9.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-10.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-10.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-11.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-11.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-12.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-12.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-13.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-13.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-14.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-14.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-15.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-15.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-16.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-16.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-6 margin-bottom-30px">
                        <div class="with-hover">
                            <img src="{{url('frontend/img/placements/placements-17.jpg')}}" alt="">
                            <a href="{{url('frontend/img/placements/placements-17.jpg')}}" data-toggle="lightbox" data-gallery="example-gallery" class="d-block hover-option background-main-color img-fluid">
                                <h1 class="text-center text-white padding-top-n-25"><i class="fa fa-search"></i></h1>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection