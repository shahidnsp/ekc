<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="Eranad Knowledge City , EKC, Engineering college ,BTech, , BAarch ,BBA, Commerce and Science College ,ekc Public School" />
    <meta property="og:site_name" content="Eranad Knowledge City , EKC, Engineering college ,BTech, , BAarch ,BBA, Commerce and Science College ,ekc Public School" />
    <meta property="og:url" content="http://ekc.edu.in/" />
    <meta property="og:description" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram" />
    <meta property="og:type" content="website" />
	<title>Eranad Knowledge City  Cherukulam  Manjeri Malappuram Kerala | EKC  Engineering College near Manjeri Malappuram Kerala</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram">
    <meta name="keywords" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram" />
    <link href="{{url('frontend/img/favicon.png')}}" rel="icon">
	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,400,700,400i,500%7CDosis:300" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/hover-min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/animate.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/font-awesome.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/owl.carousel.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/owl.theme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('frontend/css/revslider-settings.css')}}">
	<script src="{{url('frontend/js/jquery-3.2.1.min.js')}}"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body class="background-white">
	<header class="gradient-white box-shadow">
		<div class="background-primary padding-tb-5px position-relative">
			<div class="container">
				<div class="row">
					<div class="col-xl-6  d-none d-xl-block">
						<div class="contact-info text-white ">
							<span class="margin-right-10px"><i class="fa fa-map-marker margin-right-5px"></i>Manjeri, Malappuram</span>
							<span class="margin-right-10px"><i class="fa fa-phone-square margin-right-5px"></i>+91 9744-500-040</span>
							<span><i class="fa fa-envelope margin-right-5px"></i>office@eranadknowledgecity.com</span>
						</div>
					</div>
					<div class="col-xl-2 d-none d-lg-block">
						<ul class="list-inline text-center margin-0px">
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" target="_blank" href="https://www.facebook.com/eranadknowledgecity"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" href="https://www.youtube.com/user/eranadknowledgecity" target="_blank"><i class="fab fa-youtube"></i></a>
							</li>
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" href="https://www.instagram.com/ekc_knowledgecity/" target="_blank"><i class="fab fa-instagram"></i></a>
							</li>
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" href="https://www.linkedin.com/school/eranad-knowledge-city-technical-campus-cherukulam-manjeri/" target="_blank"><i class="fab fa-linkedin"></i></a>
							</li>
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" href="https://twitter.com/EranadC" target="_blank"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="list-inline-item margin-lr-8px">
								<a class="facebook" href="{{url('login')}}" target="_blank"><i class="fa fa-lock"></i></a>
							</li>
						</ul>
					</div>
					<div class="col-xl-4 d-none d-lg-block">
						<ul class="user-area list-inline text-right margin-0px">
							<li class="list-inline-item"><a href="{{url('scholarships')}}" target="_blank">Scholarships</a></li>
							<li class="list-inline-item"><a href="{{url('career')}}" target="_blank">Career</a></li>
							<!-- <li class="list-inline-item"><a href="{{url('360')}}" target="_blank">360</a></li> -->
							<li class="list-inline-item"><a href="{{url('brochure')}}" target="_blank">E-Brochure</a></li>
							<li class="list-inline-item"><a href="https://ktu.edu.in/eu/res/viewResults.htm" target="_blank">Results</a></li>
							<li class="list-inline-item"><a href="https://ekctc.linways.com/" target="_blank">Login</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="header-output">
			<div class="container header-in">
				<div class="row">
					<div class="col-xl-2 col-lg-2">
						<a id="logo" href="/" class="d-inline-block margin-top-10px"><img src="{{url('frontend/img/logo.png')}}" alt=""></a>
						<a class="mobile-toggle padding-15px background-main-color" href="#"><i class="fas fa-bars"></i></a>
					</div>
					<div class="col-xl-10 col-lg-10 position-inherit">
						<div class="float-lg-right">
							<ul id="menu-main" class="float-lg-left nav-menu link-padding-tb-20px dropdown-white">
								<li class="{{ request()->is('myhome') ? 'active' : '' }}"><a href="{{url('myhome')}}">Home</a></li>
								<li class="has-dropdown {{ request()->is('about') ? 'active' : '' }} {{ request()->is('concept') ? 'active' : '' }} {{ request()->is('governingbody') ? 'active' : '' }} {{ request()->is('directors') ? 'active' : '' }} {{ request()->is('principal') ? 'active' : '' }} {{ request()->is('360') ? 'active' : '' }} {{ request()->is('brochure') ? 'active' : '' }}"><a href="#">About Us</a>
									<ul class="sub-menu">
										<li class="sub-item"><a href="{{url('about')}}">About EKC</a></li>
										<li class="sub-item"><a href="{{url('concept')}}">Knowledge City Concept</a></li>
										<li class="sub-item"><a href="{{url('governingbody')}}">Our Promoters</a></li>
										<!-- <li class="sub-item"><a href="{{url('directors')}}">other board of directors</a></li> -->
										<!-- <li class="sub-item"><a href="{{url('principal')}}">Principal's Message</a></li> -->
										<!-- <li class="sub-item"><a href="{{url('360')}}">Campus 360 View</a></li> -->
										<li class="sub-item"><a href="{{url('brochure')}}">E-Brochure</a></li>
									</ul>
								</li>
								<li class="has-dropdown"><a href="#">Our Institutions</a>
									<ul class="sub-menu">
										<li class="sub-item"><a href="{{url('EKC-TC/index')}}">College of Engineering</a></li>
										<li class="sub-item"><a href="{{url('EKC-COA/index')}}">College of Architecture</a></li>
										<li class="sub-item"><a href="{{url('EKC-CS/index')}}">College of Commerce & Sciences</a></li>
										<li class="sub-item"><a href="{{url('EKC-SCHOOL/index')}}">Public School</a></li>
										<li class="sub-item"><a href="{{url('liberalarts')}}">Department of liberal arts</a></li>
									</ul>
								</li>
								<li class="{{ request()->is('admission') ? 'active' : '' }}"><a href="{{url('admission')}}">Admission</a></li>
								<li class="{{ request()->is('placements') ? 'active' : '' }}"><a href="{{url('placements')}}">Placements</a></li>
								<li class="has-dropdown {{ request()->is('online-grievance') ? 'active' : '' }}"><a href="#">Moocs</a>
									<ul class="sub-menu">
										<li class="sub-item"><a href="https://nptel.ac.in/" target="_blank">NPTEL</a></li>
										<li class="sub-item"><a href="https://swayam.gov.in/" target="_blank">SWAYAM</a></li>
										<li class="sub-item"><a href="{{url('online-grievance')}}">online grievance</a></li>
									</ul>
								</li>
								<li class="{{ request()->is('facilities') ? 'active' : '' }}"><a href="{{url('facilities')}}">Facilities</a></li>
								<li class="{{ request()->is('updates') ? 'active' : '' }}"><a href="{{url('updates')}}">Updates</a></li>
								<li class="has-dropdown {{ request()->is('photos') ? 'active' : '' }} {{ request()->is('videos') ? 'active' : '' }}"><a href="#">Gallery</a>
									<ul class="sub-menu">
										<li class="sub-item"><a href="{{url('photos')}}">Photos</a></li>
										<li class="sub-item"><a href="{{url('videos')}}">Videos</a></li>
									</ul>
								</li>
								<li class="{{ request()->is('contact') ? 'active' : '' }}"><a href="{{url('contact')}}">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>


    @yield('content')


	<footer class="padding-tb-50px background-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 sm-mb-30px wow fadeInUp">
                    <div class="logo margin-bottom-10px"><img src="{{url('frontend/img/logo.png')}}" alt=""></div>
                    <div class="text-white font-weight-300" style="line-height: 17px;">
                    	Eranad Knowledge City, sprawling across 100 acres of lush and beautiful land is a first of its kind project in Kerala, set up by Al-Hind Educational & Charitable Trust to LEARN, LIVE, WORK & PLAY matching the International Standards.
                    </div>
                    <ul class="list-inline text-left margin-tb-20px margin-lr-0px text-white">
                        <li class="list-inline-item"><a class="facebook" href="https://www.facebook.com/eranadknowledgecity" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a class="youtube" href="https://www.youtube.com/user/eranadknowledgecity" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        <li class="list-inline-item"><a class="google" href="https://www.instagram.com/ekc_knowledgecity/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a class="linkedin" href="https://www.linkedin.com/school/eranad-knowledge-city-technical-campus-cherukulam-manjeri/about/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a class="twitter" href="https://twitter.com/EranadC" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <ul class="footer-menu-2 row margin-0px padding-0px list-unstyled">
                        <li class="col-6  padding-tb-5px"><a href="{{url('myhome')}}" class="text-grey-2">Home</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('about')}}" class="text-grey-2">About EKC</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('concept')}}" class="text-grey-2">EKC Concepts</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('governingbody')}}" class="text-grey-2">Our Promoters</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('principal')}}" class="text-grey-2">Principal Message</a></li>
                        <!-- <li class="col-6  padding-tb-5px"><a href="{{url('360')}}" class="text-grey-2">Campus 360 View</a></li> -->
                        <li class="col-6  padding-tb-5px"><a href="{{url('brochure')}}" class="text-grey-2">E-Brochure</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('contact')}}" class="text-grey-2">Contact Us</a></li>
                        <li class="col-6  padding-tb-5px"><a href="https://nptel.ac.in/" class="text-grey-2">NPTEL</a></li>
                        <li class="col-6  padding-tb-5px"><a href="https://swayam.gov.in/" target="_blank" class="text-grey-2">Swayam</a></li>
                        <li class="col-6  padding-tb-5px"><a href="#" class="text-grey-2">Online Grievance</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('career')}}" class="text-grey-2">Career</a></li>
                    </ul>
                </div>
                <div class="col-lg-5  col-md-5 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <ul class="footer-menu-2 row margin-0px padding-0px list-unstyled">
                    	<li class="col-6  padding-tb-5px"><a href="{{url('EKC-TC/index')}}" class="text-grey-2">College of Engineering</a></li>
                    	<li class="col-6  padding-tb-5px"><a href="{{url('EKC-COA/index')}}" class="text-grey-2">College of Architecture</a></li>
                    	<li class="col-6  padding-tb-5px"><a href="{{url('EKC-CS/index')}}" class="text-grey-2">College of Commerce</a></li>
                    	<li class="col-6  padding-tb-5px"><a href="{{url('EKC-CS/index')}}" class="text-grey-2">College of Sciences</a></li>
                    	<li class="col-6  padding-tb-5px"><a href="{{url('EKC-SCHOOL/index')}}" class="text-grey-2">Public School</a></li>
                    	<li class="col-6  padding-tb-5px"><a href="#" class="text-grey-2">Dipartment of Liberal Arts</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('admission')}}" class="text-grey-2">Admission</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('facilities')}}" class="text-grey-2">Our Facilities</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('scholarships')}}" class="text-grey-2">Scholarships</a></li>
                        <li class="col-6  padding-tb-5px"><a href="https://ktu.edu.in/eu/res/viewResults.htm" target="_blank" class="text-grey-2">Results</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('videos')}}" class="text-grey-2">College Videos</a></li>
                        <li class="col-6  padding-tb-5px"><a href="{{url('photos')}}" class="text-grey-2">College Photos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="background-second wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="text-white margin-tb-15px text-left">Eranad Knowledge City | &copy; 2019 All rights reserved</p>
                </div>
                <div class="col-md-6">
                    <p class="text-white margin-tb-15px text-right">Designed & Developed By
                        <a href="http://cloudbery.com" target="_blank"><img src="{{url('frontend/img/Cloudbery.png')}}" alt="Cloudbery Solutions" ></a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- <a class="application-theme" href="https://forms.gle/gJivf7JRcKqc62G4A" target="_blank">Online Application</a> -->

   



    <script src="{{url('frontend/js/sticky-sidebar.js')}}"></script>
    <script src="{{url('frontend/js/YouTubePopUp.jquery.js')}}"></script>
    <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/imagesloaded.min.js')}}"></script>
    <script src="{{url('frontend/js/wow.min.js')}}"></script>
    <script src="{{url('frontend/js/custom.js')}}"></script>
    <script src="{{url('frontend/js/popper.min.js')}}"></script>
    <script src="{{url('frontend/js/bootstrap.min.js')}}"></script>



     @yield('scripts')
</body>
</html>