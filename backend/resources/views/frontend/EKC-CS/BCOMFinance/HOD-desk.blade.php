@extends('frontend.EKC-CS.BCOMFinance.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-5" src="{{url('frontend/img/HOD-image.png')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <div class="margin-bottom-20px wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                        </div> 
                        <h3 class="text-dark font-weight-600 font-3">Dr. First Name</h3>
                        <p class="margin-bottom-10px">
                            Welcome to the web page of our department. Here you will find detailed information about us. As you see, we are one of the most active and vibrate education department. Excellent students and Experienced Faculties are our backbone.
                        </p>
                        <p class="margin-bottom-10px">
                            We a group of highly efficient, enthusiastic and helpful staff members both in technical and administrative fronts whole heartedly support our endeavor.
                        </p>
                        <p class="margin-bottom-10px">
                            In our department instructions and learning are synergistic to each other. We attract best students of our State for Under Graduate and Post Graduate courses. Our faculty members also engage extensively on outreach activities that benefit to the entire state. We work hard and also let ourselves hair down in cultural activities, Clubs Sports and picnic etc..
                        </p>
                        <p class="margin-bottom-10px">
                            We are proud of Alumni and would love to keep in touch. Do write to us and drop in whenever you can, we look forward to active participation in taking our department to the next level.
                        </p>
                        <p class="margin-bottom-10px">
                            To conclude this message let me extend a warm welcome to all, we are constantly on the lookout for bright young mind who would enrich our department by joining us . In the quality of students, faculty members we are delights to hear from you. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection