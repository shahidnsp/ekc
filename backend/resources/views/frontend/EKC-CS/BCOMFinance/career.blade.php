@extends('frontend.EKC-CS.BCOMFinance.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-6 background-white">
                    <div class="">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInLeft">
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Career Opportunity</h1>
                            <p>
                                Eranad Knowledge City offers the right exposure for talent to perform, grow and flourish. We consider our employees as our core strength and respect hard work, knowledge and commitment. The company's evolving culture and value system provides an ideal platform for employees to perform and grow. The company's human resource policies and benefit programs are designed to attract and retain the best talent. If you feel interested in working with Eranad Knowledge City, we have the following openings:
                            </p>
                            <p class="text-large font-weight-600 text-red font-3"><span class="text-black">Contact HR :</span> +91 7994-550-404</p>
                            <p class="text-large font-weight-600 text-red font-3"><span class="text-black">Send your Resume at :</span> hr@ekc.edu.in</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-1 wow fadeInRight">
                    <img src="{{url('frontend/img/career.jpg')}}">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection