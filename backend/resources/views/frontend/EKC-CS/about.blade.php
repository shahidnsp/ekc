@extends('frontend.EKC-CS.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-top-70px">
                <div class="col-lg-6 background-white">
                    <div class="">
                        <div class="font-weight-300 wow fadeInUp">
                            <small class="text-main-color">Since 2013</small>
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Welcome to<br/>Eranad Knowledge City</h1>
                            <p>
                                Eranad Knowledge City is a first of its kind project in Kerala set up by Al-Hind Educational & Charitable Trust, to pave the way for the radical revolution in educational standards to cater the needs of the society by creating an environment to LEARN, LIVE, WORK & PLAY matching the International Standards. Eranad Knowledge City is not just a group of institutions which imparts world class education, but an experience where everyone associated with is part of a mission to see that students have universal and equitable access to higher education in various disciplines and inculcate in knowledge and confidence embedded with professional traits to become highly competent in the dynamic global market.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 background-overlay wow fadeInUp" style="background-image: url('{{url('frontend/img/about-ekc-1.jpg')}}');"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="margin-bottom-20px">
                        Institutions at Eranad Knowledge City are characterized by the quality of education across a broad range of disciplines from Kinder Garten to Post-Graduation programs covering Engineering & Technology, Architecture, Science & Technology, Commerce, Business Management, Arts, and Mass Communication & Journalism.
                    </p>
                    <p>
                        The College of Commerce & Sciences offers programs that create graduates who are aware of and can interact with contemporary thought and research in their respective specializations and utilize such in their professional lives. The College creates a stimulating academic environment in which the students are given hands on expertise on the concepts they learn by assisting them with industry internships, creative assignments and participation in various discussion forums. The students of Eranad Knowledge City College of Commerce & Sciences are geared to develop their personality as innovative thinkers and successful entrepreneurs.
                    </p>
                </div>
            </div>
            <div class="row margin-tb-100px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row margin-tb-100px">
                <div class="col-lg-6 background-overlay wow fadeInUp" style="background-image:url('{{url('frontend/img/about-ekc-2.jpg')}}');"></div>
                <div class="col-lg-6 background-white">
                    <div class="">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Institutions at<br/>Eranad Knowledge City are...</h1>
                            <p>
                                Institutions at Eranad Knowledge City are characterized by the quality of education across a broad range of disciplines from Kinder Garten to Post-Graduation programs covering Engineering & Technology, Architecture, Science & Technology, Commerce, Business Management, Arts, and Mass Communication & Journalism. We have been successful in managing the process of transition among the students from the school to College by acknowledging that students in all their diversity come to us to learn and that we are responsible not just to keep them, but for creating environments where active learning can take place. We achieve this by enabling strategic infrastructure and extensive beyond the curriculum activities, to embed transition practices systematically.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection