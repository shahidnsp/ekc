@extends('frontend.EKC-CS.layout.app')

@section('content')


    <?php
        $sliders = \Illuminate\Support\Facades\Cache::remember('CSsliders', 2*60, function() {
            return \App\Slider::where('department','Commerce')->get();
        });
    ?>

<!-- =========== Revelution Slider =========== -->
	<div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
		<div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
			<ul>
				<!-- SLIDE  -->
                @foreach($sliders as $slider)
                <li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                </li>
                @endforeach
			</ul>
			<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
		</div>
	</div>

	<?php
        $notifications=\App\Notification::where('department','Commerce')->get();
     ?>
     @if(count($notifications)>0)
    <section>
        <div class="breaking-news">
            <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                @foreach($notifications as $notification)
                <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                @endforeach
               {{-- <a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</a>
                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
                <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a>--}}
            </marquee>
        </div>
    </section>
    @endif
	<section class="padding-top-100px">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-md-7">
                    <div class="fadeInUp margin-bottom-20px">
                        <h1 class="font-weight-300 text-title-large font-3">Welcome to College of<br/>Commerce & Sciences</h1>
                    </div>
                    <div class="margin-bottom-10px wow fadeInUp">
                        <p>
                            Eranad Knowledge City is a first of its kind project in Kerala set up by Al-Hind Educational & Charitable Trust, to pave the way for the radical revolution in educational standards to cater the needs of the society by creating an environment to LEARN, LIVE, WORK & PLAY matching the International Standards. Eranad Knowledge City is not just a group of institutions which imparts world class education, but an experience where everyone associated with is part of a mission to see that students have universal and equitable access to higher education in various disciplines and inculcate in knowledge and confidence embedded with professional traits to become highly competent in the dynamic global market.
                        </p>
                        <p>
                            Institutions at Eranad Knowledge City are characterized by the quality of education across a broad range of disciplines from Kinder Garten to Post-Graduation programs covering Engineering & Technology, Architecture, Science & Technology, Commerce, Business Management, Arts, and Mass Communication & Journalism.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <img class="border-radius-10" src="{{url('frontend/img/EKC-CS/Commerce.jpg')}}" alt="">
                </div>
                <div class="col-md-12">
                    <div class="margin-bottom-60px wow fadeInUp">
                        <p>
                            The College of Commerce & Sciences offers programs that create graduates who are aware of and can interact with contemporary thought and research in their respective specializations and utilize such in their professional lives. The College creates a stimulating academic environment in which the students are given hands on expertise on the concepts they learn by assisting them with industry internships, creative assignments and participation in various discussion forums. The students of Eranad Knowledge City College of Commerce & Sciences are geared to develop their personality as innovative thinkers and successful entrepreneurs.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row margin-top-20px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Facilities</h1>
                <span class="opacity-7">The first knowledge city concept in kerala</span>
            </div>

            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Library.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-main-color">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Well Furnished Library
                            </h2>
                            <p>The EKC have a spacious well furnished Library having sufficient volume of books, National and International available.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Radio.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-green">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Campus Radio
                            </h2>
                            <p>Our Campus Radio station is equipped with advanced technology devices in order to attain international standards.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-yellow">
                        <div class="text-left z-index-2 position-relative text-black">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                General Store
                            </h2>
                            <p>A general store is functioning in the campus to supply quality books, notebooks and other stationary items at reasonable price.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Stores.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-pink">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Conveyance
                            </h2>
                            <p>College buses ply on a regular basis to the campus from Thirur, Perinthalmanna, Vengara, Mukkam, Nilambur, Valancheri, etc...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Transport.jpg')}}" alt="">
                </div>
            </div>

        </div>
    </section>

        <?php
            $newses = \Illuminate\Support\Facades\Cache::remember('CSnewses', 2*60, function() {
                return \App\News::where('department','Commerce')->orderBy('id', 'desc')->take(3)->get();
            });
        ?>
        @if(count($newses)>0)
            <section class="padding-tb-100px">
                <div class="container">
                    <div class="text-center margin-bottom-35px wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3">Latest News</h1>
                        <span class="opacity-7">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elitdunt</span>
                    </div>
                    <div class="row">
                        @foreach($newses as $news)
                        <div class="col-lg-3 col-md-6 sm-mb-45px">
                            <div class="blog-item thum-hover border-radius-15 hidden background-white hvr-float hvr-sh2">
                                <div class="position-relative">
                                    <div class="date border-radius-15 z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                        {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                                    </div>
                                    <a href="#">
                                        <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                                    </a>
                                </div>
                                <a href="#" class="margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                                <hr>
                                <div class="padding-lr-30px">
                                    <small class="margin-right-30px">By : <a href="#">Admin</a></small>
                                </div>
                                <hr class="margin-bottom-0px border-white">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
         @endif


@endsection

@section('scripts')
    <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection