@extends('frontend.EKC-CS.MCOMFinance.layout.app')

@section('content')
    <section class="background-gray padding-tb-25px">
        <div class="container">
            <h6 class="font-weight-600 text-extra-large font-3 text-capitalize float-md-left padding-tb-10px">EKC - Colleges of Commerce and Science : Videos</h6>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="{{url('myhome')}}" class="text-grey-4">Home</a></li>
                <li><a href="{{url('EKC-CS/index')}}" class="text-grey-4">EKC-CS</a></li>
                <li><a href="#" class="text-grey-4">Gallery</a></li>
                <li class="active">Videos</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <section class="padding-tb-50px">
        <div class="container">
            <div class="row">
                @foreach($videos as $video)
                <div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="{{$video->link}}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                @endforeach

                {{--<div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <iframe class="margin-tb-28px" width="100%" height="200" src="https://www.youtube.com/embed/MPDNdteLK1w?modestbranding=1&autohide=1&showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>--}}
            </div>
            <div class="row">
                {!! $videos->links() !!}
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection