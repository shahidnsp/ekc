@extends('frontend.EKC-CS.BCOMCORP.layout.app')

@section('content')
    <div class="padding-tb-80px background-light-grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 sticky-content">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3">Admission at Eranad Knowledge City</h1>
                    </div>
                    <h3>B.Com Co-operation<a href="{{url('frontend/download/EKCBrochure2018.pdf')}}" download class="buttonDownload pull-right">Download Brochure</a></h3>
                    <p class="coming-soon">
                            We will update soon
                        </p>
                    <h3>Eligibility Criteria</h3>
                    <p>Any +2 or equivalent with not less than 50% marks in aggregate. SC/ST OBC and OEC will be given relations as per rules.</p>
                    
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>College of Commerce and Science</th>
                                <th>Course Duration</th>
                                <th>Gov: Seats</th>
                                <th>Mgt: Seats</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>BA Mass Communication & Journalism</td>
                                <td>3 Years (6 Sem)</td>
                                <td>20</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>B.B.A</td>
                                <td>3 Years (6 Sem)</td>
                                <td>25</td>
                                <td>25</td>
                            </tr>
                            <tr>
                                <td>B.Com Finance</td>
                                <td>3 Years (6 Sem)</td>
                                <td>30</td>
                                <td>30</td>
                            </tr>
                            <tr>
                                <td>B.Com Co-Operation</td>
                                <td>3 Years (6 Sem)</td>
                                <td>20</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>B.Com Computer Application</td>
                                <td>3 Years (6 Sem)</td>
                                <td>15</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <td>B.Sc Computer Science</td>
                                <td>3 Years (6 Sem)</td>
                                <td>12</td>
                                <td>12</td>
                            </tr>
                            <tr>
                                <td>M.Com Finance</td>
                                <td>2 Years (4 Sem)</td>
                                <td>10</td>
                                <td>5</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 sticky-sidebar">
                    <div class="widget">
                        <h4 class="widget-title clearfix"><span>Contact Us</span></h4>
                        <h6 class="text-red">Phone :</h6>
                        <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                        <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9544-500-040</span>
                        <h6 class="margin-top-20px text-red">Admission Enquiry :</h6>
                        <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                        <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9446-009-824</span>
                        <h6 class="margin-top-20px text-red">EKC - College of Commerce and Science :</h6>
                        <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                        <span class="d-block"><i class="fa fa-envelope text-main-color margin-right-10px" aria-hidden="true"></i> cs@eranadknowledgecity.com</span>

                        <h6 class="margin-top-20px">Address :</h6>
                        <span class="d-block"><i class="fa fa-map text-main-color margin-right-10px" aria-hidden="true"></i> Eranad Knowledge City, Cherukulam</span>
                        <span class="margin-left-30px">Manjeri Malappuram Dist</span>
                        <span class="margin-left-30px"><br/>Kerala PIN 676122 </span>
                        <h6 class="margin-top-20px">Email :</h6>
                        <span class="d-block"><i class="fa fa-envelope-open text-main-color margin-right-10px" aria-hidden="true"></i> office@eranadknowledgecity.com </span>
                        <h6 class="margin-top-20px">Locate Us :</h6>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62624.7163207098!2d76.09392957741646!3d11.184321113569137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba64a8eac9c7a6f%3A0x11439d8329094129!2sEranad+Knowledge+City+Technical+Campus!5e0!3m2!1sen!2sin!4v1553062313905" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection