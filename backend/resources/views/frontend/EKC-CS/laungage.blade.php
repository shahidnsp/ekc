@extends('frontend.EKC-CS.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-6 background-white">
                    <div class="">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInLeft">
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Department of Languages</h1>
                            <p>
                                The Department of Languages is a unique place at Eranad Knowledge city, college of commerce and sciences ever since it's establishment in 2013. The department offers common courses on Malayalam, English and Hindi literature.  It aims at providing instruction on the new trends of literature and languages and carrying out different programmes to make the language learning a pleasant experience.  The department consists of five faculty members.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-1 wow fadeInRight">
                    <img src="{{url('frontend/img/Department of Languages.jpg')}}">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection