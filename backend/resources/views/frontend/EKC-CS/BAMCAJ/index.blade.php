@extends('frontend.EKC-CS.BAMCAJ.layout.app')

@section('content')

    <?php
        $sliders = \Illuminate\Support\Facades\Cache::remember('TCCommerceBAMCAJsliders', 2*60, function() {
            return \App\Slider::where('department','Commerce')->where('from','BAMCAJ')->get();
        });
    ?>
    <!-- =========== Revelution Slider =========== -->
    <div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                @foreach($sliders as $slider)
               <li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                   <!-- MAIN IMAGE -->
                   <img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
               </li>
               @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>

    <?php
        $notifications=\App\Notification::where('department','Commerce')->where('from','BAMCAJ')->get();
     ?>
    <section>
        <div class="breaking-news">
            <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                @foreach($notifications as $notification)
                <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                @endforeach
               {{-- <a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</a>
                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
                <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a>--}}
            </marquee>
        </div>
    </section>

    <section class="padding-top-100px">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-md-7">
                    <div class="margin-bottom-35px fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3">Welcome to BA.Mass Communication and Journalism</h1>
                    </div>
                    <div class="margin-bottom-60px wow fadeInUp">
                        <p class="affiliated">Affiliated to Calicut University.</p>
                        <p class="approved">Approved by Government of Kerala.</p>
                        <p>
                            The department of Mass Communication and Journalism of Eranad Knowledge City College of Commerce and Sciences started it's journey from the year 2014.      The department provides avant-garde facilities for the students to produce quality media content. The Department offers three-year (Six semester) BA programme which includes print journalism, television production, advertising and public relations, new media, communication research etc.      The department has contributed a number of well reputed media personalities to the state in the fields of journalism, communication, education, arts and many others.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <img class="border-radius-10 margin-top-20px" src="{{url('frontend/img/EKC-CS/B.COM Finance3.jpg')}}" alt="">
                </div>
            </div>
            <div class="row margin-top-20px margin-bottom-50px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBAMCAJnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BAMCAJ')->orderBy('id', 'desc')->take(4)->get();
        });
    ?>
    @if(count($newses)>0)
    <section class="padding-tb-50px">
        <div class="container">
            <div class="text-center margin-bottom-35px fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Last News & Events</h1>
                <span class="opacity-7">The First Knowledge City Concept In Kerala</span>
            </div>
            <div class="row">
                @foreach($newses as $news)
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                 {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                        <hr>
                        <div class="padding-lr-30px">
                            <span class="margin-right-30px">By : <a href="#">Technical Campus</a></span>
                            <span class="margin-right-30px">In : <a href="#">News</a></span>
                        </div>
                        <hr class="margin-bottom-0px border-white">
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    @endif

    <section>
        <div class="row no-gutters">
            <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                <div class="padding-30px">

                </div>
            </div>
            <div class="col-lg-4 background-blue">
                <div class="padding-lr-45px padding-tb-30px text-white">
                    <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                    <p>Eranad Knowledge City is a first of its kind project in Kerala set up by Al-Hind Educational & Charitable Trust.</p>
                    <ul class="margin-0px padding-0px list-unstyled">
                        <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                        <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                        <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                        <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                        <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 background-green">
                <form class="dark-form padding-lr-45px padding-tb-30px" method="post" action="{{route('postcontact')}}">
                    {!! csrf_field() !!}

                    <input type="hidden" name="department" value="Commerce"/>
                    <input type="hidden" name="from" value="BAMCAJ"/>
                    <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6 text-black">
                            <label >Full Name</label>
                            <input type="text" name="name" class="form-control" id="inputName4" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6 text-black">
                            <label >Email Address</label>
                            <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-black">
                        <label >Contact Number</label>
                        <input type="Number" name="phone" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group text-black">
                        <label>Message</label>
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</button>
                </form>

            </div>

        </div>
    </section>

@endsection

@section('scripts')
  <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection