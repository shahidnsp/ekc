@extends('frontend.EKC-CS.BAMCAJ.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-6 background-white">
                    <div class="">
                        <div class="font-weight-300 wow fadeInUp">
                            <small class="text-main-color">Since 2004</small>
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Civil Engineering Campus<br/>Eranad Knowledge City</h1>
                            <p>
                                The department of Mass Communication and Journalism of Eranad Knowledge City College of Commerce and Sciences started it's journey from the year 2014.      The department provides avant-garde facilities for the students to produce quality media content. The Department offers three-year (Six semester) BA programme which includes print journalism, television production, advertising and public relations, new media, communication research etc.      The department has contributed a number of well reputed media personalities to the state in the fields of journalism, communication, education, arts and many others.
                            </p>
                            <h1 class="font-weight-300 text-title-large font-3 margin-bottom-20px">Vision</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp">
                    <img class="border-radius-10 margin-top-20px" src="{{url('frontend/img/EKC-CS/B.COM Finance3.jpg')}}" alt="">
                </div>
                <div class="col-lg-12">
                    <p>
                        The department of Mass Communication and Journalism of Eranad Knowledge City College of Commerce and Sciences will set excellent standards for Media education, Research, Training, using state-of-the-art technology for building a knowledge driven information society, contributing to human development, empowerment and participatory democracy, anchored in pluralism, universal values and ethics.
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-20px">Mission</h1>
                    <p>
                        To make a dynamic learning environment which nourishes new ideas, creativity, research and develops leaders and innovators in the domain of media and mass communication.
                    </p>
                </div>
            </div>
            <div class="row margin-tb-100px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row no-gutters">
            <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                <div class="padding-30px">

                </div>
            </div>
            <div class="col-lg-4 background-blue">
                <div class="padding-lr-45px padding-tb-30px text-white">
                    <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                    <p>Eranad Knowledge City is a first of its kind project in Kerala set up by Al-Hind Educational & Charitable Trust.</p>
                    <ul class="margin-0px padding-0px list-unstyled">
                        <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                        <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                        <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                        <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                        <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 background-green">
                <form class="dark-form padding-lr-45px padding-tb-30px">
                    <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6 text-black">
                            <label >Full Name</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6 text-black">
                            <label >Email Address</label>
                            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-black">
                        <label >Contact Number</label>
                        <input type="Number" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group text-black">
                        <label>Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <a href="#" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</a>
                </form>

            </div>

        </div>
    </section>
@endsection

@section('scripts')

@endsection