@extends('frontend.EKC-CS.BAMCAJ.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">HOD's Desk</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-3" src="{{url('frontend/img/HOD-image.png')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3 margin-bottom-20px">Dr. First Name</h3>
                        <p class="margin-bottom-20px">
                            Nothing defines the human experience better than communication. The ability that each of us have, in fact, the need that each one of us has to convey our thoughts to others and to receive theirs.
                        </p>
                        <p class="margin-bottom-20px">
                            The department of Mass Communication and Journalism of Earnad Knowledge City College of Commerce and Sciences   consisting of 8 experienced and competent members is an integral part of the institution.
                        </p>
                        <p class="margin-bottom-20px">
                            The department facilitates ample exposure and experience in disciplines such as TV Journalism, Advertising, Newspaper Making, Cinema Production, Photojournalism, Radio programmes, Media Research and Creative Writing.
                        </p>
                        <p class="margin-bottom-20px">
                            Department of Mass Communication and Journalism teaches "what" to communicate. It remains as a center of creativity, of learning, of intellectual energy and entertainment.
                        </p>
                        <p class="margin-bottom-20px">
                            Thank you for your interest in the Department of Mass Communication and Journalism. Please do not hesitate to contact me if i can assist you in any way.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection