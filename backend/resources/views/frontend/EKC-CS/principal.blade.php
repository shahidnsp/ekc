@extends('frontend.EKC-CS.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Principal's Message</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-5" src="{{url('frontend/img/EKC-CS/EKC-CS-Principal-suchithra varma.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Mrs.Suchithra Varma</h3>
                        <!-- <small class="font-weight-600">M.Tech Ph.D</small> -->
                        <div class="padding-top-0px">
                            <p class="margin-bottom-30px">
                                Our college has already been in the forefront of education in this area for a few years now.  I am really happy to be of help to deserving students, in their academic pursuits. Set in picturesque surroundings, not far from the city limits, our college provides excellent classes by efficient and enthusiastic faculty members and also top class avenues for extra-curricular activities also.
                            </p>
                            <!-- <p class="margin-bottom-30px">
                                Through occasional symposia, workshops, industry visits, seminars, the students are constantly armed with professional ability, analytical reasoning skills and creativity to be adapted to face the global confrontation in engineering and technology.
                            </p>
                            <p>
                                Co-curricular and extracurricular exertions are accustomed to them in a uniform tone to establish their charisma in a classic way. Our Entrepreneurship development unit handing out to instill analysis perception and industrious competence in the midst of students and faculties. Also our fundamental guts is possessing a group of executives as they are constantly showing their determination and perseverance for this institution to elevate to an apical level. 
                            </p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection

@section('scripts')

@endsection