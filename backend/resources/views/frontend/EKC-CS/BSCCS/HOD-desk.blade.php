@extends('frontend.EKC-CS.BSCCS.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">HOD's Desk</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-3" src="{{url('frontend/img/EKC-CS/BSCCS-HOD-Sathish Kumar.M.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Prof. Sathish Kumar.M</h3>
                        <div class="padding-top-20px">
                            <p class="margin-bottom-20px">
                                Welcome to the Department of Computer Science at Eranad Knowledge City,   College of Commerce and Sciences , Cherukulam. Department offers Bachelor’s Degree in Computer Science under University Of  Calicut .The  program   is designed to create globally competent manpower for the information and communication technology (ICT) industry. At the same time, it is also designed to prepare the student for post graduate education in the best universities across the globe.
                            </p>
                            <p class="margin-bottom-20px">
                                The college is located in a green environment with a state of art facilities and highly qualified faculty. Our experienced faculty are the strong pillars of the department. The department is well equipped with good infrastructure such as latest Computers, Projector, LAN connection & Internet connectivity.
                            </p>
                            <p class="margin-bottom-20px">
                                The department works with the objective of addressing critical challenges faced by the Industry, society and the academia. Perhaps even more important is our unceasing commitment to our students, helping them to learn, grow, develop, and achieve their goals in their pursuit to excel in their professional career. The department offers excellent academic environment with a team of highly qualified faculty members to cater the needs of the students and to inspire them to develop their technical skills. Students are prepared to adopt high technologies by getting all basic and advance knowledge, which is covered in the graduation course of B.Sc. Computer Science.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="margin-bottom-20px">
                            The staff members are encouraged to attend national and state level workshop and Conference to enhance their knowledge.
                        </p>
                        <p>The students are encouraged:</p>
                        <ul>
                            <li>To participate in Seminars, Technical Fest, Symposium and Conferences conducted by other Colleges.</li>
                            <li>To participate in the departmental activities such as debate, extempore, group discussion, Quiz and brain storming sessions conducted in the departmental  Forum  </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection