@extends('frontend.layout.app')

@section('content')
    <section>
            <div class="container">
                <div class="row margin-tb-50px">
                    <div class="col-lg-12">
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Department of Liberal Arts</h1>
                        <p>
                            The Following  courses are  offered under Department of Liberal Arts. One course must be taken in the first year and it is optional to take the second course. But only two courses can be taken in the first year. Course fee id Rs. 950/= per course.  Course will be conducted only if  there are minimum 10 Students for that course . Or the second preferred course should be selected. The course will be 10 hours in two days.
                        </p>
                        <h3>Courses Offered.</h3>
                        <h4 class="tex-primary">
                            Civil Engineering
                        </h4>
                        <ol>
                            <li>Interior Design</li>
                            <li>Building model making</li>
                            <li>Sustainable development</li>
                            
                        </ol>
                        <h4>Computer science and Engineering</h4>
                        <ol>
                            <li>Virtual Reality</li>
                            <li>Artificial Intelligence</li>
                            <li>IOT ( Internet of Things)</li>
                            <li>Ethical Hacking</li>
                            <li>Android App Development</li>
                        </ol>
                        <h4>Electronics and Communication Engineering</h4>
                        <ol>
                            <li>Robotics</li>
                            <li>PLC/SCADA</li>
                            <li>Image Processing</li>
                            <li>4G 5G technologies</li>
                        </ol>
                        <h4>Mechanical Engineering</h4>
                        <ol>
                            <li>Industrial Product Development </li>
                            <li>Electric Vehicle Design</li>
                        </ol>
                        <h4>General Courses</h4>
                        <ol>
                            <li>Public Speaking</li>
                            <li>Event Management</li>
                            <li>Entrepreneurship </li>
                            <li>Drawing and Sketching</li>
                            <li>Photography</li>
                        </ol>
                    </div>
                </div>
            </div>
    </section>

@endsection

@section('scripts')

@endsection