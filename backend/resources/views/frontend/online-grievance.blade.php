@extends('frontend.layout.app')

@section('content')

    <section class="padding-tb-50px">
        <div class="container">
            <div class="row">
                 <div class="col-lg-6 col-md-6">
                    @if (\Illuminate\Support\Facades\Session::has('message'))
                    <h2 class="text text-success"><?php $msg=\Illuminate\Support\Facades\Session::get('message'); echo $msg[0];?></h2>
                     @endif
                 </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 offset-3 padding-20px box-shadow">
                    <div class="margin-bottom-20px text-center wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3">Online Grievance Support</h1>
                        <!-- <p class="coming-soon">
                            We will update soon
                        </p> -->
                    </div>
                    <form method="post" action="{{route('postcontact')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="department" value="Main"/>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Full Name</label>
                                <input type="text" class="form-control" id="inputName4" name="name" placeholder="First Name" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Email</label>
                                <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="example@gmail.com" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Contact Number</label>
                                <input type="number" class="form-control" id="inputNumber4" name="phone" placeholder="(+91) ---- --- ---" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Your Place</label>
                                <input type="text" class="form-control" id="inputEmail4" name="address" placeholder="Residential place" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="5" required=""></textarea>
                        </div>
                        <button type="submit" class="btn-sm btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <!-- <section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-50px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Contact Us</h1>
                <span>A first kind of Knowledge City concept in Kerala</span>
            </div>
            <div class="services-out">
                <div class="row">
                    <div class="col-lg-3 col-md-6 sm-mb-30px">
                        <div class="text-center background-white border border-grey-1 padding-20px">
                            <i class="fa fa-map-marker icon-large text-blue" aria-hidden="true"></i>
                            <h3><span class="text-medium text-capitalize font-3 font-weight-600">visit our campus</span></h3>
                            <i class="d-block text-up-small text-grey-2 margin-bottom-15px"></i>
                            <p>
                                Eranad Knowledge City<br/>Cherukulam, Manjeri,<br/>Malappuram, Kerala, Pin-676122<br/>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 sm-mb-30px">
                        <div class="text-center background-white border border-grey-1 padding-20px">
                            <i class="fa fa-comments icon-large text-green" aria-hidden="true"></i>
                            <h3><span class="text-medium text-capitalize font-3 font-weight-600">let's talk</span></h3>
                            <p>+91 9744-500-040,<br/>+91 9544-500-040<br/><br/></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 sm-mb-30px">
                        <div class="text-center background-white border border-grey-1 padding-20px">
                            <i class="fa fa-envelope icon-large text-yellow" aria-hidden="true"></i>
                            <h3><span class="text-medium text-capitalize font-3 font-weight-600">e-mail us</span></h3>
                            <p>office@eranadknowledgecity.com<br/>btech@eranadknowledgecity.com<br/>cs@eranadknowledgecity.com</p>    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 sm-mb-30px">
                        <div class="text-center background-white border border-grey-1 padding-20px">
                            <i class="fa fa-bullhorn icon-large text-pink" aria-hidden="true"></i>
                            <h3><span class="text-medium text-capitalize font-3 font-weight-600">enquiry services</span></h3>
                            <p>+91 9744-500-040,<br/>+91 9544-500-040<br/><br/></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    
@endsection

@section('scripts')

@endsection