@extends('frontend.EKC-TC.layout.app')

@section('content')
    <section class="padding-tb-100px">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-6">
    					<div class="margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Academic Council</h1>
    					</div>
    					<table id="example" class="table table-bordered" cellspacing="0" width="100%">
    						<thead>
    							<tr>
    								<th>Name</th>
    								<th>Position</th>
    							</tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td>Adv. Shihab Mecheri</td>
    								<td>CEO</td>
    							</tr>
    							<tr>
    								<td>Er. Kamrudheen K P</td>
    								<td>Director</td>
    							</tr>
    							<tr>
    								<td>Shri Mujeeb Kirikkal</td>
    								<td>Director</td>
    							</tr>
    							<tr>
    								<td>Dr. Satheesh Kumar.K</td>
    								<td>Principal</td>
    							</tr>
    							<tr>
    								<td>Prof. Shihabudheen Kunnath</td>
    								<td>HoD/S&H & IQAC (i/c)</td>
    							</tr>
    							<tr>
    								<td>Prof. Shivakumar</td>
    								<td>HoD – Civil Engineering</td>
    							</tr>
    							<tr>
    								<td>Prof.Adarsh T K</td>
    								<td>HoD - CSE</td>
    							</tr>
    							<tr>
    								<td>Prof Fazludheen C</td>
    								<td>HoD – Mechanical Engineering</td>
    							</tr>
    							<tr>
    								<td>Prof Navaneeth</td>
    								<td>HoD – ECE</td>
    							</tr>
    							<tr>
    								<td>Prof Vivek kanjiyangat</td>
    								<td>HoD – Safety & Fire Engineering</td>
    							</tr>
    							<tr>
    								<td>Mr Umesh</td>
    								<td>Finance Manager</td>
    							</tr>
    							<!-- <tr>
    								<td>Ms Chinnu Mariam Ninan</td>
    								<td>Student Council In charge</td>
    							</tr>
    							<tr>
    								<td>Ms Athira B Kaimal</td>
    								<td>CoE / ERP</td>
    							</tr> -->
    							<tr>
    								<td>Mr Shabeer</td>
    								<td>Librarian</td>
    							</tr>
    							<tr>
    								<td>Mr Mohd Malick</td>
    								<td>Head – Physical Education Department</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    				<div class="col-md-6">
    					<div class="margin-bottom-35px margin-top-50px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Student Council</h1>
    						<p>
    							The Students' Council (SC) is the representative body of the entire student community of  Eranad Knowledge City Technical Campus. Its executive committee comprises of advisors from the Management and Faculty and students elected from the representatives off all the classes across different programmes in our campus. The SC is expected to promote  welfare measures and activities that encourage intellectual and personal development of the students in our campus.
    						</p>
    						<p>
    							The Council along with the various sub committees conducts 3 major programmes that created distinct mark for Eranad Knowledge City at national and state levels over these years since its inception.
    						</p>
    						<ul>
    							<li><strong>Vibranium</strong> - National Level Inter Collegiate Technical & Cultural Festival</li>
    							<li><strong>Aroah</strong> - Arts Festival of EKC-TC</li>
    							<li><strong>Athlon</strong> - Sports Festival of EKC-TC</li>
    						</ul>
    						<p>
    							Apart from these, the Council conducts regular programmes for the students of EKC-TC as  Ifthaar,  Onam Celebrations, Christmas Celebrations, Tech Fest, various intra-collegiate games etc.
    						</p>
    					</div>
    					<!-- <table id="example" class="table table-bordered" cellspacing="0" width="100%">
    						<thead>
    							<tr>
    								<th>Name</th>
    								<th>Position</th>
    								<th>Contact NO</th>
    							</tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td>Secretary - Arts Fest (Aaroh)</td>
    								<td>Danish C T (S7 MEch)</td>
    								<td>9567155258</td>
    							</tr>
    							<tr>
    								<td>Secretary  - Tech Fest (Vibranium)</td>
    								<td>Midhilaj Abdul naser (S7 CSE)</td>
    								<td>811904883</td>
    							</tr>
    							<tr>
    								<td>Secretary – Sports (Athlon)</td>
    								<td>Mohammed Sadik (S7 CSE)</td>
    								<td>9995182948</td>
    							</tr>
    							<tr>
    								<td>Staff Secretary</td>
    								<td>Chinnu Mariam Ninan</td>
    								<td>9895802652</td>
    							</tr>
    						</tbody>
    					</table> -->
    				</div>
    			</div>
    		</div>
    	</section>

    	<section class="padding-bottom-100px">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-6">
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">IQAC</h1>
    						<h4 class="text-grey-2">The primary aim of  IQAC is</h4>
    					</div>
    					<ul class="list-style-6 padding-0px margin-bottom-35px">
    						<li>To develop a system for conscious, consistent and catalytic action to improve the academic and administrative performance of the institution. </li>
    						<li>To promote measures for institutional functioning towards quality enhancement through internalization of quality culture and institutionalization of best practices.</li>
    					</ul>
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h4 class="text-grey-2">IQAC shall evolve mechanisms and procedures for</h4>
    					</div>
    					<ul class="list-style-6 padding-0px">
    						<li>Ensuring timely, efficient and progressive performance of academic, administrative and financial tasks.</li>
    						<li>The relevance and quality of academic and research programmes.</li>
    						<li>Equitable access to and affordability of academic programmes for various sections of society.</li>
    						<li>Optimization and integration of modern methods of teaching and learning.</li>
    						<li>The credibility of evaluation procedures.</li>
    						<li>Ensuring the adequacy, maintenance and proper allocation of support structure and services.</li>
    						<li>Sharing of research findings and networking with other institutions in India and abroad.</li>
    					</ul>
    				</div>
    				<div class="col-md-6">
    					<div class="margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Functions of IQAC</h1>
    						<h4 class="text-grey-2">Some of the functions expected of the IQAC are:</h4>
    					</div>
    					<ul class="list-style-1 padding-0px">
    						<li>Development and application of quality benchmarks/parameters for various academic and administrative activities of the institution.</li>
    						<li>Facilitating the creation of a learner-centric environment conducive to quality education and faculty maturation to adopt the required knowledge and technology for participatory teaching and learning process.</li>
    						<li>Arrangement for feedback response from students, parents and other stakeholders on quality-related institutional processes.</li>
    						<li>Dissemination of information on various quality parameters of higher education.</li>
    						<li>Organization of inter and intra institutional workshops, seminars on quality related themes and promotion of quality circles.</li>
    						<li>Documentation of the various programmes/activities leading to quality improvement.</li>
    						<li>Acting as a nodal agency of the Institution for coordinating quality-related activities, including adoption and dissemination of best practices.</li>
    						<li>Development and maintenance of institutional database through MIS for the purpose of maintaining /enhancing the institutional quality.</li>
    						<li>Development of Quality Culture in the institution.</li>
    						<li>Preparation of the Annual Quality Assurance Report (AQAR), Self Assessment Report (SAR) as per guidelines and parameters of NAAC, to be submitted to NAAC.</li>
    					</ul>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-md-12">
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Composition of IQAC</h1>
    					</div>
    					<p>
    						Our IQAC  team consists of members representing the teaching and non-teaching faculty, representatives of the management, alumni, local community and collaborating organizations. External experts are also invited by the IQAC to contribute to the effective functioning of the college.
    					</p>
    				</div>
    			</div>
    		</div>
    	</section>

    	<section class="padding-bottom-100px">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Class and Course Commitee</h1>
    					</div>
    					<p>
    						Course Committee is for common courses across branches of study, normally for the first two semesters.  Example: Mathematics Course Committee, Physics Course Committee etc...,
    						Class committees are for higher semesters, in each department.  Both (Course and Class committees) shall have a Senior Professor/Faculty member who does not handle that course (Course Committee) or any courses for that semester (Class Committee) as its chairman and all teachers taking the course/courses and 4 to 6 student representatives.
    					</p>
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-small font-3">The purpose of a Class Committee is</h1>
    					</div>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-md-6">
    					<ul class="list-style-3 padding-0px">
    						<li>to ensure that all curricular and co-/ extra-curricular activities of the class are carried out smoothly;</li>
    						<li>to assess and monitor the class as a whole;</li>
    						<li>to convey the feedback of teachers regarding the class, attendance and to set their expectations for students;</li>
    						<li>to enable students to provide feedback regarding the teaching-learning process, internal assessments, co-/ extra-curricular activities, student support services, mentoring, etc</li>
    						<li>Class committees shall have at least two meetings in a semester  the meetings shall be called by the HoD</li>
    						<li>The first meeting shall be in the beginning of the semester and will be used to assess the performance of the class in the previous semester.</li>
    						<li>The second meeting shall be called early in the second half of the semester, and shall be used to provide feedback and course correction for the curricular and co-/extra-curricular activities of the class.</li>
    					</ul>
    				</div>
    				<div class="col-md-6">
    					<ul class="list-style-3 padding-0px">
    						<li>Both meetings will have an assessment of the class by the Chairman, Class Tutors, followed by the comments of teachers.</li>
    						<li>Both meeting could have evaluation of attendance status of students.</li>
    						<li>Both meetings could present
    							<ul class="list-style-2">
    								<li>Common/ open feedback of the class about the major problems they faced in class</li>
    								<li>Specific feedback on the teaching-learning process, internal tests, covering of the syllabus, difficult areas of learning, infrastructure, attendance, special help needed, etc. </li>
    							</ul>
    						</li>
    						<li>The Chairman should write the minutes of the meeting or get one of the students to do it Minutes of the meeting will be reviewed by IQAC and necessary corrective actions can be recommended</li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</section>

    	<section class="padding-bottom-100px">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="margin-bottom-20px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Anti Ragging Committee</h1>
    					</div>
    					<p>
    						Ragging of any nature is a criminal and non-bail able offence. Involvement in ragging shall lead to stringent punishment, including imprisonment as per the law of the land. A student, whose involvement in ragging is established, shall be summarily dismissed from the college. Each student of the Institute, along with his/her parent, is required to give an undertaking in this regard and the same is to be submitted at the time of registration.
    					</p>
    				</div>
    			</div>
    		</div>
    	</section>
@endsection

@section('scripts')

@endsection