@extends('frontend.EKC-TC.electronic.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p>
                        Nurture students through effective teaching learning process and quality mentoring thereby molding them to be globally competent with high degree of technical, interpersonal, analytical, managerial skills and professional ethics.
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <p>
                        The mission of the ECE Department is to implement the art of effective teaching learning process with quality mentoring for providing well-balanced education for students to acquire professional and social competencies, to strengthen the soft skills, to create awareness on social commitment and life-long learning through interaction with outside world regarding contemporary issues and technological trends in the field of Electronics and Communication Engineering.
                    </p>
                    <!-- <ul class="list-style-1 padding-0px">
                        <li>To educate and prepare students for leadership roles in Mechanical Engineering and to conduct research for the benefit of the society.</li>
                        <li>To make our students life-long learners capable of building their careers upon a solid foundation of knowledge.</li>
                        <li>To make our students competent in communicating technical materials and concepts in individual and group situations.</li>
                        <li>To develop students as socially committed professionals with sound engineering knowledge, creative minds, leadership qualities and practical skills.</li>
                        <li>To inculcate moral and ethical values among the students.</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection