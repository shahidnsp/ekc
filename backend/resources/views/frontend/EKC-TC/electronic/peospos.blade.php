@extends('frontend.EKC-TC.electronic.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12 background-white">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Educational Objectives (PEOs) </h1>
                        <ul class="list-style-3 padding-0px margin-bottom-40px">
                            <li>To teach and mentor the students to obtain in-depth knowledge in the field of Electronics and Communication Engineering thereby making them capable to understand, design and analyze the electronic circuits, equipments and softwares related to the field of study.</li>
                            <li>To build an ability in students to function on multi-disciplinary streams.</li>
                            <li>To train the students on inter-personal skills so that they can effectively perform the duties assigned to them as team leaders or project managers in the industry/organization with high degree of professional, ethical and social responsibility.</li>
                            <li>To inculcate the thirst for life-long learning in students and thereby motivate them for higher studies/research.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Specific Outcomes (PSOs)</h1>
                        <ul class="list-style-4 padding-0px margin-bottom-40px">
                            <li>Graduates will be able to apply knowledge of basic Sciences in Electronics and Communication Engineering.</li>
                            <li>Graduates will have the skill to design and conduct experiments, as well as to analyze and interpret data.</li>
                            <li>Graduates will be able to design a project, system, component, or process to meet the needs of the society.</li>
                            <li>Graduates will be aware of contemporary issues and will have the skill to identify, formulate, and solve them with engineering tools.</li>
                            <li>Graduates will acquire the broad education necessary to understand the impact of engineering solutions in a global, economic, environmental, and societal context.</li>
                            <li>Graduates will understand the need for, and an ability to engage in life-long learning.</li>
                            <li>Graduates will be able to use the techniques, skills, and modern engineering tools necessary for engineering practice in electronics and related domains.</li>
                            <li>Graduates will be familiar with the ongoing research areas in electronics and related domains.</li>
                            <li>Graduates will be able to participate and succeed in competitive examinations.</li>
                            <li>Graduates will acquire a remarkable improvement in their confidence to compete with best in the world and serve the society in a noble way.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">PEOs - PSOs Mapping</h1>
                        <table id="example" class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>PEOs / PSOs</th>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th>E</th>
                                    <th>F</th>
                                    <th>G</th>
                                    <th>H</th>
                                    <th>I</th>
                                    <th>J</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <td>H</td>
                                    <td>H</td>
                                    <td>H</td>
                                    <td>M</td>
                                    <td>L</td>
                                    <td>L</td>
                                    <td>H</td>
                                    <td>M</td>
                                    <td>L</td>
                                    <td>L</td>
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <td>H</td>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>H</td>
                                    <td>L</td>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>L</td>
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <td>L</td>
                                    <td>L</td>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>H</td>
                                    <td>M</td>
                                    <td>L</td>
                                    <td>L</td>
                                    <td>H</td>
                                    <td>H</td>
                                </tr>
                                <tr>
                                    <th>4</th>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>M</td>
                                    <td>L</td>
                                    <td>M</td>
                                    <td>H</td>
                                    <td>L</td>
                                    <td>H</td>
                                    <td>M</td>
                                    <td>L</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection