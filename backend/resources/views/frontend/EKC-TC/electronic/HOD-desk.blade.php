@extends('frontend.EKC-TC.electronic.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/Prof.Navaneeth.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <div class="margin-bottom-50px wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                        </div> 
                        <h3 class="text-dark font-weight-600 font-3">Prof. Navaneeth</h3>
                        <p class="margin-bottom-20px">
                            We the Department of electronics and communication engineering of Eranad Knowledge City campus focuses on generating engineers with sound knowledge and practical skills. Graduates will be able thrive in competitive fields and produce remarkable achievements. Advancement in all technological areas are continuously monitored and tried to inculcate among students. Department also promotes activities that aims cultural, moral and ethical values. The entire team is always dedicated in serving students to achieve their dreams. We always try to partner with industries and look ahead for the future of our graduates. 
                        </p>
                        <!-- <p class="coming-soon">
                            we will update soon...
                        </p> -->
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-12">
                        <p class="margin-top-10px">
                            Our students have participated in National level Contest like Smart India hackathon, CSI in app and won certificate’s, Our Students and faculty regularly publish their research work in reputed international and national journals and attend and organize various international and national conferences, We also plan for value added national/international collaborations.
                        </p>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection