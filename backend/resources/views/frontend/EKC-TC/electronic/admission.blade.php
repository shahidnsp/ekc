@extends('frontend.EKC-TC.electronic.layout.app')

@section('content')
    <div class="padding-tb-80px background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 sticky-content">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">Admission at Eranad Knowledge City</h1>
                        </div>
                        <h3>Eligibility<a href="{{url('frontend/download/EKCBrochure2018.pdf')}}" download class="buttonDownload pull-right">Download Brochure</a></h3>
                        <p>
                            <b>Engineering Courses: B.Tech</b> : Higher Secondary / Vocational Higher Secondary Examination, Kerala, or equivalent  with 50% marks in Mathematics separately, and 50 % marks in Mathematics, Physics and Chemistry (or computer science if no chemistry) put together.
                        </p>
                        <p class="margin-bottom-10px">
                            <b>However, 45% marks in Mathematics, Physics and Chemistry put together are eligible for admission under Management Quota in Eranad Knowledge City MANJERI</b>
                        </p>
                        <p>
                            <b>Architecture Course : </b> 10+2 or equivalent examination  with Mathematics as a subject by securing 50% marks in aggregate  OR  10+3 Diploma (any stream) with 50% aggregate marks with Mathematics as one of the  subject  In addition candidates should have 70 Marks out of 200 (35%)  for (NATA) 2019 on or before 10.06.2019
                        </p>
                        <p class="margin-bottom-40px text-red">
                            <b># Abstract only. Contact Admission Team for more clarification or refer KEAM  Prospectus</b>
                        </p>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>College of Engineering & Technology</th>
                                    <th>Course Duration</th>
                                    <th>Gov: Seats</th>
                                    <th>Mgt: Seats</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Civil Engineening</td>
                                    <td>4 Years (8 Sem)</td>
                                    <td>30</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Computer Science & Engineering</td>
                                    <td>4 Years (8 Sem)</td>
                                    <td>30</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Electronics & Communication Engineering</td>
                                    <td>4 Years (8 Sem)</td>
                                    <td>30</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Mechanical Engineering</td>
                                    <td>4 Years (8 Sem)</td>
                                    <td>30</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Safety & Fire Engineering</td>
                                    <td>4 Years (8 Sem)</td>
                                    <td>30</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <th class="text-red">Online Admission</th>
                                    <td colspan="3">
                                        <a class="text-main-color font-weight-600" href="https://ekctc.linways.com/onlineadmission/newbtech/form_redirect.php?courseTypeId=1&formTypeId=2" target="_blank">
                                            https://ekctc.linways.com/onlineadmission/newbtech/apply_online 
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4 col-md-4 sticky-sidebar">
                        <div class="widget">
                            <p>
                                <a class="text-red font-weight-600" href="https://ekctc.linways.com/onlineadmission/newbtech/form_redirect.php?courseTypeId=1&formTypeId=2" target="_blank"><h4>B.Tech Online Admission.</h4></a>
                            </p>
                            <h4 class="widget-title clearfix margin-top-30px"><span>Contact Us</span></h4>
                            <h6 class="text-red">Phone :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9544-500-040</span>
                            <h6 class="margin-top-20px text-red">Admission Enquiry :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9446-009-824</span>
                            <h6 class="margin-top-20px text-red">EKC - Technical College :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-envelope text-main-color margin-right-10px" aria-hidden="true"></i> office@eranadknowledgecity.com</span>
                            

                            <h6 class="margin-top-20px">Address :</h6>
                            <span class="d-block"><i class="fa fa-map text-main-color margin-right-10px" aria-hidden="true"></i> Eranad Knowledge City, Cherukulam</span>
                            <span class="margin-left-30px">Manjeri Malappuram Dist</span>
                            <span class="margin-left-30px"><br/>Kerala PIN 676122 </span>
                            <h6 class="margin-top-20px">Email :</h6>
                            <span class="d-block"><i class="fa fa-envelope-open text-main-color margin-right-10px" aria-hidden="true"></i> office@eranadknowledgecity.com </span>
                            <h6 class="margin-top-20px">Locate Us :</h6>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62624.7163207098!2d76.09392957741646!3d11.184321113569137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba64a8eac9c7a6f%3A0x11439d8329094129!2sEranad+Knowledge+City+Technical+Campus!5e0!3m2!1sen!2sin!4v1553062313905" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

@endsection