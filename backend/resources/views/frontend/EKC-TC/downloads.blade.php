@extends('frontend.EKC-TC.layout.app')

@section('content')

    <?php
        $downloads = \Illuminate\Support\Facades\Cache::remember('TCMainDownloads', 2*60, function() {
            return \App\Download::where('department','Engineering')->where('from','Main')->get();
        });
    ?>

    <section class="padding-tb-50px background-light-grey text-center">
        <div class="container">
            <div class="row">
                @foreach($downloads as $download)
                    @if($download->type=='Photo')
                    <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                        <div class="downloads-item box-shadow-hover">
                            <i class="fa fa-file-image text-green" aria-hidden="true"></i>
                            <p>{{$download->name}}</p>
                            <a href="{{url('images/'.$download->file)}}" download="#">Download Now</a>
                        </div>
                    </div>
                    @elseif($download->type=='Link')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-link text-pink" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{$download->link}}" target="_blank">Visit Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='PDF')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-pdf text-pink" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('pdf/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='Doc')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-word text-yellow" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('doc/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='Excel')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-word text-yellow" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('excel/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection