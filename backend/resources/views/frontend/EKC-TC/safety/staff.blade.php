@extends('frontend.EKC-TC.safety.layout.app')

@section('content')
    <section class="padding-tb-100px">

        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Team</h1>
                <span class="opacity-7">The first knowledge city concept in Kerala</span>
            </div>
            <?php
                $teams=\App\Team::where('department','Engineering')->where('from','Safety')->orderBy('order')->get();
            ?>

            <div class="row">
                @foreach($teams as $index=> $team)
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.{{$index}}s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="{{url('images/'.$team->photo)}}" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a target="_blank" class="facebook" href="{{$team->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a target="_blank" class="youtube" href="{{$team->twitter}}"><i class="fab fa-twitter"></i></a></li>
                                        <li><a target="_blank" class="linkedin" href="{{$team->linkedin}}"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a target="_blank" class="google" href="{{$team->gplus}}"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">{{$team->name}}</a></h4>
                        <small>{{strtoupper($team->designation)}}</small>
                        <p class="text-grey-2 margin-top-10px">
                            {{$team->description}}
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </section>
@endsection

@section('scripts')

@endsection