@extends('frontend.EKC-TC.safety.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p>
                        To emerge as a hub of excellence in Safety and Fire Engineering education by providing quality education,  advanced research and training thereby producing globally recognized professionals who are creative and innovative to create an impact globally in industry, research, entrepreneurship and society
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <ul class="list-style-1 padding-0px">
                        <li>
                            To provide quality education and to prepare nationally and internationally competitive undergraduate students for a successful career in safety, occupational health, environmental management and fire protection engineering.
                        </li>
                        <li>
                            To promote research in the field of safety, occupational health, environmental management and fire protection engineering for improving safety and fire engineering practices.
                        </li>
                        <li>
                            To encourage students for advanced study and research and to improve the safety standards of the society and the nation at large.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection