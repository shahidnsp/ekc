@extends('frontend.EKC-TC.safety.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12 background-white">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Educational Objectives (PEOs) </h1>
                        <ul class="list-style-3 padding-0px margin-bottom-40px">
                            <li>Graduates will compete on a global platform to pursue their professional career in Safety and Fire Engineering and allied disciplines. </li>
                            <li>Graduates will pursue higher education and/or engage in continuous up gradation of their professional skills. </li>
                            <li>Graduates will communicate effectively and will demonstrate professional behaviour while working in diverse teams. </li>
                            <li>Graduates will demonstrate high regard for human rights, respect values in diverse cultures, and have concern for society and environment.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Outcomes</h1>
                        <ul class="list-style-4 padding-0px">
                            <li><b>Engineering Knowledge:</b> Apply the knowledge of mathematics, science, engineering fundamentals and an engineering specialisation to the solution of complex engineering problems. </li>
                            <li><b>Problem analysis:</b> Identify, formulate, research literature, and analyse complex engineering problems reaching substantiated conclusions using first principles of mathematics, natural sciences and engineering sciences. </li>
                            <li><b>Design/development of solutions:</b> Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal and environmental considerations. </li>
                            <li><b>Conduct investigations of complex problems:</b> Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to arrive at valid conclusions. </li>
                            <li><b>Modern tool usage:</b> Create, select, and apply appropriate techniques, resources and modern engineering and IT tools including prediction and modelling to complex engineering activities with an understanding of their limitations. </li>
                            <li><b>The engineer and the society:</b> Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues and the consequent responsibilities relevant to the professional engineering practice. </li>
                            <li><b>Environment and sustainability:</b> Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of the need for sustainable development. </li>
                            <li><b>Ethics:</b> Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice. </li>
                            <li><b>Individual and team work:</b> Function effectively as an individual, and as a member or leader in diverse teams, and in multidisciplinary settings. </li>
                            <li><b>Communication:</b> Communicate effectively on complex engineering activities with the engineering community and with society at large, such as, being able to comprehend and write effective reports and design documentation, make effective presentations and give and receive clear instructions. </li>
                            <li><b>Project management and finance:</b> Demonstrate knowledge and understanding of the engineering and management principles and apply these to one’s own work, as a member and leader in a team, to manage projects and in multidisciplinary environments.</li>
                            <li><b>Life-long learning:</b> Recognise the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Specific Outcomes (PSOs)</h1>
                        <ul class="list-style-4 padding-0px">
                            <li>Analyse and assess fire safety of buildings and industries to design and specify fire protection systems. </li>
                            <li>Assess hazards and risks in process and manufacturing industries and devise remedial measures and safety management systems. </li>
                            <li>Assess the occupational health and environmental issues associated with industrial and other activities to design control measures with traditional and modern computational tools based on codes and statutes.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection