@extends('frontend.EKC-TC.safety.layout.app')

@section('content')
        <section class="padding-tb-70px">
            <div class="background-light-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/HOD-Vivek kanjiyangat.jpg')}}" alt="">
                        </div>
                        <div class="col-md-9">
                            <div class="margin-bottom-35px wow fadeInUp">
                                <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                            </div> 
                            <h3 class="text-dark font-weight-600 font-3">Prof. Vivek kanjiyangat</h3>
                            <p class="margin-bottom-20px">
                                We are delighted that you are considering an Under Graduate degree programme in Safety and Fire Engineering. We are sure you will find the best career opportunities among all other traditional branches of Engineering with these four year prospectus in Engineering. The department has an excellent reputation in across the state and the course is unique in their nature. Our courses are relevant to industries requirements and placement oriented. 
                            </p>
                            <p>
                                The department has well qualified, experienced and committed faculties to provide quality education and also show the research pathways in this changing era to the students. The faculty aims at putting extra ordinary effort towards motivating the students in solving actual industrial problems by undertaking various projects, industrial visits and in-plant training. The Department is actively associated in grooming the engineering graduates in Safety and Fire Engineering by nurturing them with inputs from various basic and specialized subjects.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('scripts')

@endsection