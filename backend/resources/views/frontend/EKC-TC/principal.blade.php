@extends('frontend.EKC-TC.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Principal's Message</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-radius-5" src="{{url('frontend/img/EKC-Principal.jpeg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Dr. Satheesh Kumar.K</h3>
                        <div class="padding-top-30px">
                            <p class="margin-bottom-30px">
                                For a region of activity, we admit that our students are the best. Brilliant activities in academics with finest proceedings and principled pattern, we put up them to pick up the placements. This can only be feasible on the grounds that we had an admirable backing from the authorities, faculty members, the crew, the students and their parents.
                            </p>
                            <p class="margin-bottom-30px">
                                Through occasional symposia, workshops, industry visits, seminars, the students are constantly armed with professional ability, analytical reasoning skills and creativity to be adapted to face the global confrontation in engineering and technology.
                            </p>
                            <p>
                                Co-curricular and extracurricular exertions are accustomed to them in a uniform tone to establish their charisma in a classic way. Our Entrepreneurship development unit handing out to instill analysis perception and industrious competence in the midst of students and faculties. Also our fundamental guts is possessing a group of executives as they are constantly showing their determination and perseverance for this institution to elevate to an apical level. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="margin-tb-20px">
                            Specially designed upbringing curriculum are accustomed to faculties also to pursue higher studies and also to upgrade their skill set in their domain. 
                            I am pretty much certain about our students to continue in a state of up gradation with their stellar achievement throughout the course and also endure their excellent endeavor in pursuing post-graduation, research studies in a very professional way.
                            We have been accomplishing greater communication with the industries providing student projects, seminars, in plant training and placement. I stretch my finest wishes to all my students ,certainly that they will establish their own path to accomplishment.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection