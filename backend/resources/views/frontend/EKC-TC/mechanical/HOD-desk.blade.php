@extends('frontend.EKC-TC.mechanical.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/Mechanical-HOD-Fazludheen Chemmala.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <div class="margin-bottom-50px wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                        </div> 
                        <h3 class="text-dark font-weight-600 font-3">Prof. Fazludheen C</h3>
                        <p class="margin-bottom-20px">
                            The department of Mechanical Engineering offers B. Tech in Mechanical Engineering.
                            The department has well qualified, experienced and committed faculties to provide quality
                            education and also show the research pathways in this changing era to the students.
                        </p>
                        <p>
                            The faculty aims at putting extra ordinary effort towards motivating the students in
                            solving actual industrial problems by undertaking various projects, industrial visits and in-plant
                            training. The Department is actively associated in grooming the engineering graduates in
                            Mechanical Engineering by nurturing them with inputs from various basic and specialized
                            subjects including Production, Industrial, Thermal and Design Engineering. A wide selection of
                            projects, seminars and training facilities besides technical visits to industrial enterprises and
                            specialized lectures from industry specialists generates enormous potential and confidence
                            among the students enabling them to independently handle the life size problems in their
                            engineering careers. Students are equipped with multi-dimensional skill set covering all the
                            required attributes. Students are encouraged to take up projects for the well-being of society.
                            The Department has well equipped laboratories such as Basic Workshop Lab, Machines Shop
                            Lab, Computer Aided Machine Drawing Lab, 
                    </div>
                    <div class="col-md-12">
                        <p>
                            Mechanical Measurements Lab, Material Testing
                            Lab, Heat Transfer Lab, Fluid Mechanics & Machines Lab, Heat & Mass Transfer Lab to perform
                            the practical’s to understand the concepts.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection