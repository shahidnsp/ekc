@extends('frontend.EKC-TC.mechanical.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12 background-white">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Educational Objectives (PEOs) </h1>
                        <ul class="list-style-3 padding-0px margin-bottom-40px">
                            <li>To develop ability to apply mathematical tools, scientific basics and fundamental engineering knowledge in the field/applications.</li>
                            <li>Students will gain the ability to identify, analyse, formulate and solve different challenging Mechanical engineering problems.</li>
                            <li>To develop an understanding of the multidisciplinary approach and an ability to relate engineering issues to broader social and human context.</li>
                            <li>To develop good communication skills to transfer/share ideas among/other fraternities.</li>
                            <li>To develop professional skills useful for immediate placement/higher studies.</li>
                            <li>To produce graduates who are prepared for life-long learning and successful careers as Mechanical engineers.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Specific Outcomes (PSOs)</h1>
                        <ul class="list-style-4 padding-0px">
                            <li>An ability to apply fundamental knowledge of mathematics, science and engineering in designing computer-based systems to real world problems.</li>
                            <li>An ability to analyze a problem and to identify, formulate and use appropriate computing and Mechanical engineering tools/methods for obtaining its solution.</li>
                            <li>An ability to design, implement and evaluate a field program to meet desired needs, within realistic constraints such as economic, environmental, social, political, health and safety, manufacturability and sustainability as well as National Design standards.</li>
                            <li>An ability to practice Mechanical Engineering using up-to-date techniques, knowledge of contemporary issues, current techniques, skills and tools.</li>
                            <li>An ability to communicate effectively, both in verbal and written forms.</li>
                            <li>An ability to function as a member or a leader in team of Mechanical Engineers as well as on multidisciplinary teams.</li>
                            <li>An ability to understand and demonstrate professional, ethical, legal, security and social issues and responsibilities.</li>
                            <li>Broad education necessary to analyze the local and global impact of computing and engineering solutions on individuals, organizations and society.</li>
                            <li>An ability to understand and demonstrate the knowledge of and need for sustainable development.</li>
                            <li>An ability to recognize the need for and an ability to engage in continuing professional development and life-long learning.</li>
                            <li>Equipping the students for pursuing postgraduate studies or face competitive examinations that offer challenging and rewarding careers.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection