@extends('frontend.EKC-TC.mechanical.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p>
                        To emerge as a hub of excellence in Mechanical Engineering education by providing quality
                        education thereby producing globally recognized professionals who are creative and innovative
                        to cater the changing industrial demands and social needs.
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <ul class="list-style-1 padding-0px">
                        <li>To educate and prepare students for leadership roles in Mechanical Engineering and to conduct research for the benefit of the society.</li>
                        <li>To make our students life-long learners capable of building their careers upon a solid foundation of knowledge.</li>
                        <li>To make our students competent in communicating technical materials and concepts in individual and group situations.</li>
                        <li>To develop students as socially committed professionals with sound engineering knowledge, creative minds, leadership qualities and practical skills.</li>
                        <li>To inculcate moral and ethical values among the students.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection