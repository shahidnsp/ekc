@extends('frontend.EKC-TC.layout.app')

@section('content')

     <?php
        $sliders = \Illuminate\Support\Facades\Cache::remember('TCsliders', 2*60, function() {
            return \App\Slider::where('department','Engineering')->where('from','Main')->get();
        });
    ?>
    <!-- =========== Revelution Slider =========== -->
    <div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                @foreach($sliders as $slider)
                <li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
     <?php
        $notifications=\App\Notification::where('department','Engineering')->where('from','Main')->get();
     ?>
     @if(count($notifications)>0)
    <section>
        <div class="breaking-news">
            <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                @foreach($notifications as $notification)
                <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                @endforeach
               {{-- <a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</a>
                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
                <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a>--}}
            </marquee>
        </div>
    </section>
    @endif

    <section class="padding-top-50px">
        <div class="container">
            <div class="margin-bottom-20px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Welcome to Technical Campus</h1>
                <span class="text-capitalize">The first knowledge city concept in kerala</span>
            </div>
            <div class="row wow fadeInUp">
                <div class="col-md-7">
                    <div class="margin-bottom-60px wow fadeInUp">
                        <p>
                            EKC Technical Campus is the flagship project
                            of Eranad knowledge City. EKC TC is lead by a
                            team of educated, enlightened, experienced
                            technocrats with a vision firmly determined
                            to promote high quality education and striving to provide every facility for achieving excellence. The college is affiliated to the APJ
                            Abdul Kalam Technological University, Kerala.
                        </p>
                        <p>
                            The Engineering Programs offered at
                            Eranad Knowledge city are designed for career initiation and growth in the competitive
                            market place. There is equal emphasis on
                            theoretical concepts as well as practical insights, making our programs the right weapons to conquer the career goals.
                        </p>
                        <p>
                            EKC Technical Campus is the flagship project of Ernad Knowledge City, a game changing educational city at Manjeri, sprawling across 100 acres of lush and beautiful land. Eranad Knowledge City is envisaged as an Educational City consisting of a cluster of professional educational centres of excellence in Medicine, Dentistry, Engineering, Management, Research and an International School. The City also comprises of Villas, Apartments, IT Parks, Mini Market and all other supporting amenities to make EKC a self sustaining Township. This concept of To Learn, Live, Work and Play in a single campus will revolutionise the educational sector and will help in advancing the quality of education in the region.
                        </p>
                    </div>
                </div>
                <div  class="col-md-5"><img src="{{url('frontend/img/technical-campus.png')}}" alt=""></div>
            </div>
            <div class="row margin-top-20px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="padding-tb-50px background-light-grey">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Departments</h1>
                <span class="text-capitalize">The first knowledge city concept in kerala</span>
            </div>

            <div class="deprtments-carousel owl-carousel owl-theme wow fadeInUp">
                <div class="item margin-lr-15px">
                    <div class="background-gray margin-bottom-40px">
                        <a href="{{url('EKC-TC/civil/index')}}"><img src="{{url('frontend/img/EKC-TC/CIVIL ENGINEERING.jpg')}}" alt=""></a>
                        <div class="service-body padding-tb-15px padding-lr-10px">
                            <a href="{{url('EKC-TC/civil/index')}}" class="text-extra-large font-weight-300 d-block margin-bottom-10px font-3">CIVIL ENGINEERING</a>
                            <p>
                                Civil Engineering can be considered as the most versatile, simple and largest branch among all Engineering branches with a lot of diversity. Civil Engineering requires constant upgradation of knowledge in the field of advanced building...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item margin-lr-15px">
                    <div class="background-gray margin-bottom-40px">
                        <a href="#"><img src="{{url('frontend/img/EKC-TC/COMPUTER SCIENCE AND ENGINEERING.jpg')}}" alt=""></a>
                        <div class="service-body padding-tb-15px padding-lr-10px">
                            <a href="#" class="text-extra-large font-weight-300 d-block margin-bottom-10px font-3">COMPUTER SCIENCE AND ENGINEERING</a>
                            <p>
                                The Department provides extensive computing resources for research and education. This includes more than 100 high performance computers with Linux and Windows operating systems. The department also has IoT lab, AI etc...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item margin-lr-15px">
                    <div class="background-gray margin-bottom-40px">
                        <a href="#"><img src="{{url('frontend/img/EKC-TC/ELECTRONICS AND COMMUNICATION ENGINEERING.jpg')}}" alt=""></a>
                        <div class="service-body padding-tb-15px padding-lr-10px">
                            <a href="#" class="text-extra-large font-weight-300 d-block margin-bottom-10px font-3">ELECTRONICS AND COMMUNICATION ENGINEERING</a>
                            <p>
                                Department of Electronics and Communication Engineering introduces the concept of modern electronic devices, telecommunication, computer networking, control systems, microcontrollers etc...
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item margin-lr-15px">
                    <div class="background-gray margin-bottom-40px">
                        <a href="#"><img src="{{url('frontend/img/EKC-TC/MECHANICAL ENGINEERING.jpg')}}" alt=""></a>
                        <div class="service-body padding-tb-15px padding-lr-10px">
                            <a href="#" class="text-extra-large font-weight-300 d-block margin-bottom-10px font-3">MECHANICAL ENGINEERING</a>
                            <p>
                                Built on a long tradition of excellence, Mechanical Engineering has become synonymous with accomplishment and leadership in engineering education and research. The mission of the undergraduate curriculum is to prepare its...
                            </p>
                        </div>
                    </div>
                </div>
                
                
                <div class="item margin-lr-15px">
                    <div class="background-gray margin-bottom-40px">
                        <a href="#"><img src="{{url('frontend/img/EKC-TC/SAFETY & FIRE ENGINEERING.jpg')}}" alt=""></a>
                        <div class="service-body padding-tb-15px padding-lr-10px">
                            <a href="#" class="text-extra-large font-weight-300 d-block margin-bottom-10px font-3">SAFETY & FIRE ENGINEERING</a>
                            <p>
                                To mould engineers who can excel in major areas related to protection of people, property and the environment. Safety & Fire Engineering graduates have excellent career prospects in public & private sector industries, defence service, oil & natural gas...
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </section>

    <?php
        $newses = \Illuminate\Support\Facades\Cache::remember('TCnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Main')->orderBy('id', 'desc')->take(3)->get();
        });
    ?>
    @if(count($newses)>0)
    <section class="padding-tb-50px">
        <div class="container">
            <div class="text-center margin-bottom-35px fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Last News & Events</h1>
                <span class="text-capitalize">The first knowledge city concept in kerala</span>
            </div>
            <div class="row">
                 @foreach($newses as $news)
                    <div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
                        <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                            <div class="position-relative">
                                <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                    {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                                </div>
                                <a href="#">
                                    <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                                </a>
                            </div>
                            <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                            <hr>
                            <div class="padding-lr-30px">
                                <span class="margin-right-30px">By : <a href="#">Technical Campus</a></span>
                                <span class="margin-right-30px">In : <a href="#">News</a></span>
                            </div>
                            <hr class="margin-bottom-0px border-white">
                        </div>
                    </div>
                 @endforeach
            </div>
        </div>
    </section>
    @endif
@endsection

@section('scripts')

    <script>

        @foreach($notifications as $notification)
            $.niftyNoty({
                type: 'success',
                container: 'floating',
                floating: {
                    position: "top-right",
                    animationIn: "bounceInUp"

                },

                 icon: 'fa fa-times-circle', animationOut: "bounceInDown", title: '<a href="#">Admission 2019 Enquiry</a>',
                 message: '<a href="#"><h2>+91 9744-500-040, +91 9446-009-824</h2></a>',
                 
                closeBtn: true,
                onShow: function() {
                }
            });
        @endforeach

    </script> 

  <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection