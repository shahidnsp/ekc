@extends('frontend.EKC-TC.layout.app')

@section('content')
    <section>
    		<div class="container padding-tb-100px">
    			<div class="row">
    				<div class="col-md-6">
    					<div class="margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Principal's Desk</h1>
    					</div>
    					<div class="row">
    						<div class="col-md-6">
    							<img src="http://via.placeholder.com/255x300" alt="">
    						</div>
    						<div class="col-md-6">
    							<h5>The Principal</h5>
    							<h6 class="text-grey-2">Prof. Name</h6>
    							<p>
    								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    							</p>
    						</div>
    					</div>
    				</div>
    				<div class="col-md-6">
    					<div class="text-right margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Chairman’s Desk</h1>
    					</div>
    					<div class="row">
    						<div class="col-md-6 text-right">
    							<h5>The Principal</h5>
    							<h6 class="text-grey-2">Prof. Name</h6>
    							<p>
    								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    							</p>
    						</div>
    						<div class="col-md-6 text-right">
    							<img src="http://via.placeholder.com/255x300" alt="">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    	<section>
    		<div class="container padding-bottom-100px">
    			<div class="row">
    				<div class="col-md-6">
    					<div class="margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">CEO's Message</h1>
    					</div>
    					<div class="row">
    						<div class="col-md-6">
    							<img src="http://via.placeholder.com/255x300" alt="">
    						</div>
    						<div class="col-md-6">
    							<h5>The Principal</h5>
    							<h6 class="text-grey-2">Prof. Name</h6>
    							<p>
    								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    							</p>
    						</div>
    					</div>
    				</div>
    				<div class="col-md-6">
    					<div class="text-center margin-bottom-35px wow fadeInUp">
    						<h1 class="font-weight-300 text-title-large font-3">Governing Body</h1>
    					</div>
    					<div class="row">
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    					</div>
    					<div class="row">
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    						<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    							<div class="team with-hover text-center">
    								<div class="position-relative overflow-hidden">
    									<img src="http://via.placeholder.com/130x130" alt="">
    									<div class="hover-option bag-dark text-center padding-top-n-20">
    										<div class="position-relative hight-full">
    											<ul class="social-list light bottom-30px position-absolute">
    												<li>Designation</li>
    											</ul>
    										</div>
    									</div>
    								</div>
    								<div class="padding-5px">
    									<h4 class="margin-bottom-0px">Salim Elshakh</h4>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
@endsection

@section('scripts')

@endsection