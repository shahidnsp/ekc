@extends('frontend.EKC-TC.humanities.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p class="margin-bottom-20px">
                        To evolve into a center of excellence in Science and Humanities by providing foundation and support for various branches of engineering studies and also in life skills and communication skills. Thus to mould a socially committed citizens of our nation.
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <p class="margin-bottom-20px">
                        We equip our students with the fundamentals of Sciences and Humanities to achieve professional standards and ethical values of the community. Committed to mould our students with the capability of critical questioning, logical thinking, professional communication and ethical living.
                    </p>
                    <!-- <ul class="list-style-1 padding-0px">
                        <li>To educate and prepare students for leadership roles in Safety and Fire Engineering and to conduct research for the benefit of the society.</li>
                        <li>To make our students life-long learners capable of building their careers upon a solid foundation of knowledge.</li>
                        <li>To make our students competent in communicating technical materials and concepts in individual and group situations.</li>
                        <li>To develop students as socially committed professionals with sound engineering knowledge, creative minds, leadership qualities and practical skills.</li>
                        <li>To inculcate moral and ethical values among the students.</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection