@extends('frontend.EKC-TC.humanities.layout.app')

@section('content')
        <section class="padding-tb-70px">
            <div class="background-light-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/Prof.shihabudheen kunnath.jpg')}}" alt="">
                        </div>
                        <div class="col-md-9">
                            <div class="margin-bottom-35px wow fadeInUp">
                                <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                            </div> 
                            <h3 class="text-dark font-weight-600 font-3">Prof. Shihabudheen Kunnath</h3>
                            <p>
                                The Department of Science & Humanities is an integral part of the institution. A combined effort of the members of this department from various disciplines has made the department very vibrant in disseminating knowledge of science to the students fresh from schools. In order to reinforce the confidence of fresh students in acquiring mathematical and scientific skills bridge-courses are conducted before the regular classes.
                            </p>
                            <p>
                                The department aims to provide a strong foundation in the fundamental science with the basic tools of analysis as well as the knowledge of the principles on which engineering is based. The department offers courses in mathematics up to the fifth semester; engineering physics, engineering chemistry, life skills and professional communication skills in the first & second semesters; Business Economics in the third & fourth semesters.  The department offers excellent academic environment with a team of highly qualified faculty members to cater the needs of the students and to inspire them to develop their technical skills.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('scripts')

@endsection