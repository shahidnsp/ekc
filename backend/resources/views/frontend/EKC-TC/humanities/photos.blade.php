@extends('frontend.EKC-TC.humanities.layout.app')

@section('content')
    <section class="background-gray padding-tb-25px">
        <div class="container">
            <h6 class="font-weight-600 text-extra-large font-3 text-capitalize float-md-left padding-tb-10px">EKC-TC - Humanities & Fire Engineering Campus : Photos</h6>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="{{url('myhome')}}" class="text-grey-4">Home</a></li>
                <li><a href="{{url('EKC-TC/index')}}" class="text-grey-4">EKC-TC</a></li>
                <li><a href="#" class="text-grey-4">Gallery</a></li>
                <li class="active">Photos</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <section class="padding-tb-50px">
        <?php
           $photos = \Illuminate\Support\Facades\Cache::remember('TCHumanitiesphotos', 2*60, function() {
               return \App\Gallary::where('department','Engineering')->where('from','Humanities')->get();
           });
        ?>

       <div class="container margin-bottom-30px">
           <ul class="portfolio-filter">
               <li><a href="#" data-filter="all">All</a></li>
               @foreach($photos as $photo)
               <li><a href="#" data-filter="{{$photo->id}}">{{$photo->title}}</a></li>
               @endforeach
           </ul>
       </div>
       <div class="container">
           <div class="row filtr-container">
               @foreach($photos as $photo)
                   @foreach($photo->gallary_items as $gallary_item)
                   <div class="col-lg-4 col-md-6 filtr-item" data-category="all, {{$photo->id}}" data-sort="value">
                       <div class="portfolio style-1 thum-hover margin-bottom-30px border-radius-5">
                           <div class="item-thumbnail background-white">
                               <img src="{{url('images/'.$gallary_item->filename)}}" alt="">
                           </div>
                           <div class="hover-box white hidden">
                               <div class="flex-center">
                                   <a href="#" class="h3"><i class="fa fa-camera-retro"></i></a>
                               </div>
                           </div>
                       </div>
                   </div>
                   @endforeach
               @endforeach
           </div>
       </div>
        <div class="container">
            <div class="text-center">
                <a href="#" class="btn box-shadow margin-tb-50px padding-tb-10px btn-sm border-2 border-radius-30 btn-inline-block width-210px background-main-color text-white">Load More ...</a>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
     <script src="{{url('frontend/js/jquery.filterizr.min.js')}}"></script>
@endsection