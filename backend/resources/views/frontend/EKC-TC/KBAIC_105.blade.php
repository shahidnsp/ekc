@extends('frontend.EKC-TC.layout.app')

@section('content')
    <section>
            <div class="container">
                <div class="row margin-tb-50px">
                    <div class="col-lg-6 wow fadeInUp">
                        <img src="{{url('frontend/img/liberalarts.jpg')}}">            
                    </div>
                    <div class="col-lg-6 background-white">
                        <div class="margin-top-50px">
                            <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                                <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">KBA Innovation Club</h1>
                                <p class="margin-bottom-20px">
                                    Kerala Blockchain Academy Innovation Club (KBAIC) is a novel
                                    initiative by Kerala Blockchain Academy (KBA) to help students
                                    from academic institutions across the country to explore deep
                                    into the potential of the blockchain technology KBAIC provides
                                    an opportunity to work closely with experts and experiment the
                                    latest advancements in this domain. This will also help students
                                    to build their career in the disruptive world of blockchain
                                </p>
                                <p>
                                    kerala Blockchain Academy (KBA) is an initiative of Government of Kerala under Indian
                                    Institute of Information and Technology and Management Kerala with many
                                    International collaborations to explore the disruptive potential of the Blockchain Technology
                                    for achieving public good through capacity building to promote Research, Development and
                                    Entrepreneurship. Since its inception in 2017, the vibrant ecosystem of KBA offering
                                    Certification Programs, R&D activities and Consultancy, has attracted International attention
                                    KBA is an Associate Member and Official Training Partner of Linux Foundation Hyperledger
                                    Project
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('scripts')

@endsection