@extends('frontend.EKC-TC.computer.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/Prof. Adarsh T K.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <div class="margin-bottom-50px wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                        </div> 
                        <h3 class="text-dark font-weight-600 font-3">Prof. Adarsh T K </h3>
                        <p class="margin-bottom-20px">
                            Welcome to the Department of Computer Science & Engineering at Eranad Knowledge City- Technical Campus, Manjeri, The Department will aim to equip the students with the ability and skills to analyze, design and develop computer systems and their applications
                        </p>
                        <p>
                            The CSE Department regularly organizes national conferences, seminars, and value added courses. This provides a wide range of opportunities for faculty and students to bring out their potential and innovative skills in a variety of fields. The department has conducted International Conferences on Computing Communication intelligent System(I3CIS). The department regularly conducts National workshops in VR, Android etc 
                        </p>
                        <p>
                            Students are encouraged to actively participate in various associations like: ACT(Association of Computer Technocrats) , Free and Open-Source Software (FOSS) Club, Spoken Tutorial, Codechef Campus Chapter 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="margin-top-10px">
                            Our students have participated in National level Contest like Smart India hackathon, CSI in app and won certificate’s, Our Students and faculty regularly publish their research work in reputed international and national journals and attend and organize various international and national conferences, We also plan for value added national/international collaborations.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection