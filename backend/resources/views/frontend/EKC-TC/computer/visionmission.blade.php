@extends('frontend.EKC-TC.computer.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p>
                        To be recognized nationally and internationally for providing technical education in the field of computer science and engineering with strong research and teaching environment 
                    </p>
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <ul class="list-style-1 padding-0px">
                        <li>Encourage students to become self-motivated, problem solving individuals who can find and understand the knowledge needed to be successful in the profession.</li>
                        <li>Provide a learning ambience to enhance innovations, problem solving skills, leadership qualities, team-spirit and ethical responsibilities</li>
                        <li>Establish Industry Institute Interaction program to enhance the entrepreneurship skills</li>
                        <li>Provide exposure of latest tools and technologies in the area of engineering and technology.</li>
                        <li>Pursue creative research and new technologies in Computer Science and Engineering and across disciplines in order to serve the needs of industry, government, society, and the scientific community.</li>
                        <li>Responsible, Skilled and ethically strong Engineers</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection