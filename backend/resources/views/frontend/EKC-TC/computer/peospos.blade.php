@extends('frontend.EKC-TC.computer.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12 background-white">
                    <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Educational Objectives (PEOs) </h1>
                        <p class="text-large">Our graduates will be able to:</p>
                        <ul class="list-style-3 padding-0px margin-bottom-40px">
                            <li>Apply their knowledge and critical reasoning skills to solve emerging real-world problems.</li>
                            <li>Capable of analyzing and specifying the requirement of the Computer Science and Information Technology system to design and develop using the contemporary tools.</li>
                            <li>Competencies for communicating, planning, coordinating, organizing and decision making and they will have interpersonal skills and ethical responsibility.</li>
                            <li>Motivate students to pursue life-long learning and engage in research and higher studies.</li>
                        </ul>
                        <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Program Specific Outcomes (PSOs)</h1>
                        <p class="text-large">After completion of the course our graduates will have an ability to</p>
                        <ul class="list-style-4 padding-0px">
                            <li>Design and construct a computing system</li>
                            <li>Make effective use of modern tools and techniques</li>
                            <li>Understand and apply project management techniques</li>
                            <li>Function effectively as an individual and in a team for a common goal.</li>
                            <li>Take up research problems and apply computer science principles to solve them leading to publications, patents and incubator</li>
                            <li>Apply the knowledge of mathematics, science, engineering fundamentals, and an engineering specialization</li>
                            <li>Take up multidisciplinary projects and to carry out it as per industry standards</li>
                            <li>Able to participate and succeed in competitive examinations such as GATE, IES,GRE,NDA,CDS and in International level too.</li>
                            <li>Develop confidence for self-learning and ability for life-long learning</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection