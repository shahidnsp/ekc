@extends('frontend.EKC-TC.civil.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-TC/HOD-Sivakumar.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <div class="margin-bottom-50px wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                        </div> 
                        <h3 class="text-dark font-weight-600 font-3">Prof. Sivakumar B</h3>
                        <p class="margin-bottom-20px">
                            The career opportunities available to Civil Engineering graduates are indeed broad and diverse. The many specialties within the field allow for careers in construction, transportation, environmental assessment and pollution control, hydrology, structural engineering and geotechnical engineering.  Construction industry is currently growing with latest technologies and now there is a huge demand for Civil Engineers for satisfying the demand of infrastructural development. Civil Engineers will always be needed to maintain and repair existing facilities and structures and to construct new ones. The service of Civil Engineers is inevitable in the entire Government Departments, in private and public sector industries, research and educational institutions etc, in the modern race. 
                        </p>
                        <p>
                            The department aims to mould the students into excellent engineering professionals by exposing them to different aspect of Civil Engineering. Practical training and survey camp are given to the students to improve their practical skills. The department has well equipped laboratories to perform experiments and endeavor to provide better technical facilities in days to come. Our focus is on excellence teaching and training the students with emphasis to well equip them for challenges and opportunities. The excellent infrastructure, well equipped laboratories, teaching faculty of the best kind ensures quality education and bright future of students.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            It gives me immense pleasure to lead the Department of Civil Engineering. Our college is one of the leading institutions unique like a prism reflecting the manifold shades of learning and co-curricular activities. The college and specifically the Civil Engineering Department can legitimately claim excellence in various quality parameters defining a department and obviously it has become the first choice of many a discerning parent and their ward. The information provided here in our website, will, I am sure, amply prove that our claims are truly modest.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            The Department is striving hard towards the goal of providing innovative and quality education with high standard to achieve academic excellence. The department works with the objective of addressing critical challenges faced by the Industry, society and the academia. Perhaps even more important is our unceasing commitment to our students, helping them to learn, grow, develop, and achieve their goals in their pursuit to excel in their professional career.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection