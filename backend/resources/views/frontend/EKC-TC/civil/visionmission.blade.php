@extends('frontend.EKC-TC.civil.layout.app')

@section('content')
    <section>
        <div class="container">
            <div class="row margin-tb-100px">
                <div class="col-lg-12">
                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Vision</h1>
                    <p>
                        Emerge as an excellent centre for Civil Engineering education by building up professionally competent Civil Engineers and equip the students to face current and future challenges through excellence in technical education and research, with due care of societal responsibilities and nurture them for the benefit of the Nation and World.
                    </p>



                    <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Mission</h1>
                    <ul class="list-style-1 padding-0px">
                        <li>To provide an educational, professional and intellectual experience that enables the students to serve industry, profession and society through novel solutions. </li>
                        <li>To maintain an intellectually competitive and collaborative environment that stimulates students and faculty members to achieve their best in profession.</li>
                        <li>To provide an effective learning environment where in real time training, problem solving skills, team work, communication and leadership skills are emphasized.</li>
                        <li>To educate, inspire and mentor students to develop creativity and innovative research capabilities. </li>
                        <li>To develop highly skilled, professionally competent, outstanding and socially committed Civil Engineers with high ethical values, entrepreneurship - leadership qualities and social responsibilities to make them leaders in nation building. </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection