@extends('frontend.EKC-TC.civil.layout.app')

@section('content')
        <section class="background-gray padding-tb-25px">
            <div class="container">
                <h6 class="font-weight-600 text-extra-large font-3 text-capitalize float-md-left padding-tb-10px">EKC-TC - Civil Engineering Campus Facilities</h6>
                <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                    <li><a href="/" class="text-grey-4">EKC</a></li>
                    <li><a href="{{url('myhome')}}" class="text-grey-4">Home</a></li>
                    <li><a href="{{url('EKC-TC/civil/index')}}" class="text-grey-4">Civil</a></li>
                    <li class="active">Facilities</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </section>

        <section class="padding-tb-50px">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Library.jpg')}}" alt=""></a></div>
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">well furnished Library</h3>
                                        <p>
                                            The Eranad Knowledge City have a spacious well furnished Library having sufficient volume of books in each specialty. In addition to this, journals and E-journals, National and International Periodicals are also available. Apart from the central library, all the departments are equipped with dedicated department libraries.
                                        </p>
                                        <div class="meta">
                                            <span class="margin-right-20px text-extra-small pull-right">For E-Library : 
                                                <a href="http://164.100.247.17/index.html" target="_blank" class="text-main-color">Click Here</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Library.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-100px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">The Eranad Knowledge City have a spacious well furnished Library having sufficient volume of books</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Hostels.png')}}');">
                            <div class="z-index-2 position-relative padding-top-140px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">Separate hostels are provided to girls and boys with homely atmosphere</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">Separate hostels are provided</h3>
                                        <p>
                                            Separate hostels are provided to girls and boys with homely atmosphere and cleanliness under the care and supervision of the dedicated staff of hostel that can be approached for help and needs of the students. Each hostel is provided with phone connection and other amenities for the inmates. The management changes the menu of the hostel mess at regular intervals in consultation with mess committee.
                                        </p>
                                    </div>
                                </div>
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Hostels2.png')}}" alt=""></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Cafeteria.jpg')}}" alt=""></a></div>
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">clean and hygienic cafeteria</h3>
                                        <p>
                                            Our canteen facility serves a wide range of snacks, drinks and meals all through the day. Both vegetarian and non-vegetarian delicacies are available. All the food items are cooked and served in a clean and hygienic environment.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Cafeteria.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-100px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">Our canteen facility serves a wide range of snacks, drinks and meals</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Stores.jpg')}}');height: 240px;">
                            <div class="z-index-2 position-relative padding-top-75px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">To supply quality books, notebooks and other stationary items</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">general store</h3>
                                        <p>
                                            A general store is functioning in the campus to supply quality books, notebooks and other stationary items at reasonable price.
                                        </p>
                                    </div>
                                </div>
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Stores.jpg')}}" alt=""></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Transport.jpg')}}" alt=""></a></div>
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">Conveyance</h3>
                                        <p>
                                            College buses ply on a regular basis to the campus from Thirur, Perinthalmanna, Vengara, Mukkam, Nilambur, Valancheri, Edavanna, Pandikkad, Kondotty And Kozhikod town for the convenience of staff and students. 
                                        </p>
                                        <div class="meta">
                                            <span class="margin-right-20px text-extra-small pull-right">For route details and timing : 
                                                <a href="http://ekc.edu.in/BusTime.aspx" target="_blank" class="text-main-color">Click Here</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Transport.jpg')}}');height: 240px;">
                            <div class="z-index-2 position-relative padding-top-100px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">College buses ply on a regular basis to the campus from your home town</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-CyberCenter.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-140px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">The college has an excellent air-conditioned computer laboratory</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">cyber center</h3>
                                        <p>
                                            The college has an excellent air-conditioned computer laboratory provided with ultra modern, high performance computers and peripherals to meet the lab sessions in a result oriented way. The computer labs have high-speed Internet facility speed upto 3.2 mbps and new generation legal software packages to acquire the latest technics in computer and Internet system.
                                        </p>
                                    </div>
                                </div>
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-CyberCenter2.jpg')}}" alt=""></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Coffee.jpg')}}" alt=""></a></div>
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">coffee shop</h3>
                                        <p>
                                            Our Coffee Shop facility serves a wide range of Sweets, Snacks, Juice, Hot & Cool Drinks all through the day. Both vegetarian and non-vegetarian delicacies are available. All the food items are cooked and served in a clean and hygienic environment. Students can make use of Prepaid Coupon facility for purchasing items from Coffee Shop.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Coffee.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-120px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">Wide range of Sweets, Snacks, Juice, Hot & Cool Drinks all through the day</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-Radio.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-100px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">The station is programmed and produced by volunteers either students or staff members.</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">campus radio</h3>
                                        <p>
                                            Our Campus Radio station is equipped with advanced technology devices in order to attain international standards. The station is programmed and produced by volunteers either students or staff members. Different types of in house programs from the field of entertainment, academics, GK etc are broadcasted through campus radio.
                                        </p>
                                    </div>
                                </div>
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-Radio.jpg')}}" alt=""></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-entry border-1 border-grey-1 margin-bottom-35px">
                            <div class="row no-gutters">
                                <div class="img-in col-lg-5"><a href="#"><img src="{{url('frontend/img/facilities/EKC-DrivingSchool.jpg')}}" alt=""></a></div>
                                <div class="col-lg-7">
                                    <div class="padding-25px">
                                        <h3 class="font-weight-600 text-extra-large font-3 text-capitalize">driving school facility</h3>
                                        <p>
                                            Driving School Facility in our campus benefits the students and staffs to learn two wheeler and four wheeler driving and assist them to avail driving license. This is executed with the association of driving School in a very special economic rate. The training is conducted by certified trainers and the program is coordinated by a very dedicated team including staff members.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="background-cover background-overlay background-img-dark" style="background-image: url('{{url('frontend/img/facilities/EKC-DrivingSchool.jpg')}}');">
                            <div class="z-index-2 position-relative padding-top-100px">
                                <div class="padding-30px">
                                    <h2 class="text-medium margin-top-8px text-white"><a href="#" class="text-white">Driving School Facility in our campus benefits the students and staffs to learn two wheeler and four wheeler driving</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('scripts')

@endsection