@extends('frontend.EKC-TC.layout.app')

@section('content')
    <section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Administarteve Team</h1>
                <span class="text-capitalize">The first knowledge city concept in kerala</span>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/300x350" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Designation</small>
                        <p class="margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection