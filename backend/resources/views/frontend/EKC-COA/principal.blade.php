@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Principal's Message</h1>
                </div> 
                <div class="row">
                    <div class="col-md-4">
                        <img class="border-radius-5" src="{{url('frontend/img/EKC-COA/EKC-COA B.Arch Pricipal.jpg')}}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3 class="text-dark font-weight-600 font-3">Ar. Geeta Murar</h3>
                        <small class="font-weight-600">Chief Architect (Geeta Associates, Bangalore)</small><br/>
                        <small>B.Arch., AIIA</small>
                        <div class="padding-top-30px">
                            <p class="margin-bottom-20px">
                                Our college is set amidst sprawling acres of lush green
                                space, where students explore their creativity, nurture
                                their learning and take home the enjoyable experience.
                                Our campus provides a stimulating environment for their
                                all round development and indigenous personality.
                            </p>
                            <p class="margin-bottom-20px">
                                We at EKC, support the students to refine their creative
                                skills, communication skills, indulge in extracurricular
                                activities to help them shape as eminent
                                Architects/Planners.
                            </p>
                            <p class="margin-bottom-20px">
                                This year, we are proud to announce, that we have also
                                won citation award in NASA for Landscape trophy, which
                                indeed is a great achievement by our students who have
                                put in their hard work, guided by eminent Architect
                                faculties here. Our students also have outstanding
                                achievements in sports every year and have won the
                                championship awards this year.<br/>
                                Last year, our students had the pride of hosting Zonal
                                NASA, which was a grand success.
                            </p>
                            <p class="margin-bottom-20px">
                            We understand the needs of the students and lot of
                            workshops/seminars/conferences/trainings are held to
                            give them a cutting edge knowledge base.
                        </p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p class="margin-bottom-20px">Our faculties are our assets, their skills and creativity help in quenching the thirst of knowledge for the architecture students. We boast of having participation of practicing Architects in design studios to foster the student’s creativity and take pride in shaping the architecture ideas of the students. We now step into the upcoming academic year with a new zeal and enthusiasm.
                        </p>
                        <p>
                            Involved in conceptualising, designing, presentations to clients, working drawings, Co-ordinating with clients & consultants and execution. Worked with renowned architectural firms in Bangalore like Venkitaramanan Associates, & Thomas Associates, Started own practice in the year 2011 and Worked on various residences, apartments, interior projects. Currently, also handling five star hotel and Health care projects. Have good expertise in interior turn key projects Concentrating on Sustainability. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection