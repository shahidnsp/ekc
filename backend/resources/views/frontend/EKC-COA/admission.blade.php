@extends('frontend.EKC-COA.layout.app')

@section('content')
    <div class="padding-tb-80px background-light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 sticky-content">
                        <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                            <h1 class="font-weight-300 text-title-large font-3">Admission at Eranad Knowledge City - B.Arch</h1>
                        </div>
                        <h3>Introduction<a href="{{url('frontend/download/EKCBrochure2018.pdf')}}" download class="buttonDownload pull-right">Download Brochure</a></h3>
                        <p>
                            This prospectus contains the rules and regulations applicable for selection and admission to B.Arch. courses at Eranad Knowledge City College of Architecture, Cherukulam, Manjeri . 50% of the seats are filled from the list of the Common Entrance Examination conducted by the Government of Kerala. The remaining 35% Seats are under the management quota and 15% under NRI quota as per Govt. norms. Lateral entry seats are avail-able to diploma holders as per Government notification.
                        </p>
                        <h3>Faculty</h3>
                        <p>
                            The college has a team of highly qualified and well-experienced faculty. This includes a number of well-qualified retired professors in all the engineering fields. The competent team of faculty gives value based and character oriented education to the students with a fruitful student-parent-teacher relationship. This helps to achieve the best results in the university examinations.
                        </p>
                        <h3>Teaching Methodology</h3>
                        <p>
                            Teaching methods are highly sophisticated and upgraded utilizing the service of qualified & experienced faculty members. Structured Lectures, Seminars and Workshops are arranged with all electronic gadgets like LCD Projector, WI-FI systems and most modern legal version of software packages. By executing innovative ideas with the help of animations and demonstra-tions, the pedagogic method of Eranad Knowledge City excels incomparison with other institutes.
                        </p>
                        <h3>Courses Offered</h3>
                        <p class="text-red">Batchelor of Architecture (B.Arch) 5 year course - 40 intake</p>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>College of Architecture</th>
                                    <th>Course Duration</th>
                                    <th>No of Seats</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bachelor of Architecture (B.Arch)</td>
                                    <td>5 Years (10 Sem)</td>
                                    <td>80 Seats</td>
                                </tr>
                                <tr>
                                    <th class="text-red">Online Admission</th>
                                    <td colspan="3">
                                        <a class="text-main-color font-weight-600" href="https://ekccoa.linways.com/onlineadmission/barch/form_redirect.php?courseTypeId=14&formTypeId=2 " target="_blank">
                                            https://ekccoa.linways.com/onlineadmission/barch/apply_online 
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h3>Government Quota</h3>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <th>Academic Pre-requisites</th>
                                    <td>
                                        <ol>
                                            <li>Pass in +2 level examination with a minimum of 50% marks in aggregate. </li>
                                            <li>Pass in Mathematics</li>
                                            <li>NATA score minimum 80marks out of 200</li>
                                        </ol>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td>Minimum 17 years (KEAM PROSPECTUS)</td>
                                </tr>
                                <tr>
                                    <th>Deadlines</th>
                                    <td><a class="text-main-color" href="https://www.cee.kerala.gov.in/main.php" target="_blank">www.cee.kerala.gov.in</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <h3>NRI Quota (NO.Seats : 14)</h3>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <th>Academic Pre-requisites</th>
                                    <td>
                                        <ol>
                                            <li>Pass in +2 level examination with a minimum of 50% marks in aggregate. </li>
                                            <li>Pass in Mathematics</li>
                                            <li>NATA score minimum 80marks out of 200</li>
                                        </ol>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td>Minimum 17 years (KEAM PROSPECTUS)</td>
                                </tr>
                                <tr>
                                    <th>Deadlines</th>
                                    <td><a class="text-main-color" href="https://www.cee.kerala.gov.in/main.php" target="_blank">www.cee.kerala.gov.in</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <h3>NRI Quota (NO.Seats : 06)</h3>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <th>Academic Pre-requisites</th>
                                    <td>
                                        Candidates who have passed 10+2 Senior Secondary School Certificate Examination or examinations recognized as equivalent thereto , with Mathematics as a subject of examination, with 50% marks in aggregate / Candidates who have passed 10+3 Diploma (any stream) recognised by Central/State Governments with 50% aggregate marks / Candidates with International Baccalaureate Diploma, after 10 years of schooling, with not less than 50% marks in aggregate and with Mathematics as compulsory subject of examination are eligible to apply. In addition, candidates will have to score 40% marks (80 marks out of 200) in the NATA (National Aptitude Test in Architecture).
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td>Minimum 17 years (KEAM PROSPECTUS)</td>
                                </tr>
                            </tbody>
                        </table>
                        <p><b>Note:</b> Other Conditions: Those who are seeking admission under NRI quota shall produce evidence that the candidate is financially supported by an appropriately related NRIs</p>
                        <h2 class="text-main-color margin-top-50px">Nativity</h2>
                        <p>
                            Applicant should be an Indian citizen. Persons of Indian Origin (PIO)/Overseas Citizen of India (OCI) Card Holders will be treated on par with Indian citizens for the limited purpose of admission.
                        </p>
                        <p>
                            Both Keralites and Non-Keralites are eligible to apply for the Entrance Examination. Applicants of Kerala origin will be categorized as ‘Keralites’. Others will be categorized as 'Non-Keralites'.
                        </p>
                        <p>
                            A candidate will be considered as a ’Keralite’, for the limited purpose of eligibility for admission, if (a) he/she or his/her father/mother was born in Kerala OR (b) he/she has been a resident of Kerala for a period of 5 years within a period of 12 years OR (c) he/she has undergone his/her school studies from Standards VIII to XII in educational institution(s) in Kerala. The candidate should obtain relevant certificates in the application form as prescribed in the Prospectus to avail such benefit.
                        </p>
                        <p>
                            Candidates who are children of All India Service (AIS) officers allotted to Kerala cadre, claiming Keralite Status, should produce a certificate from the competent authority showing that the parent of the candidate is an AIS officer allotted to Kerala cadre. Non-Keralites are classified as <b>'Non-Keralite Category-I' and 'Non-Keralite Category-II'</b>.
                        </p>
                        <p>
                            ´Non-Keralite Category-I´ candidates are those who have undergone the qualifying course in Kerala, and who are the sons/daughters of Employees of the Government of India, or, employees who are serving in connection with the affairs of the Government of Kerala, subject to the condition that, all categories of the employees mentioned above should have served in Kerala for a minimum period of 2 years, or, have served in connection with the affairs of the Government of Kerala for a minimum period of 2 years. Such candidates will be eligible for admission to the Government seats under ‘State Merit’ only, for all the courses, which are filled on the basis of the rank in the Entrance Examination(s) or the rank list prepared by giving equal weightage to the marks in NATA and qualifying examination, as applicable.
                        </p>
                        <p>
                            Candidates who do not belong to the ‘Keralite’ or ‘Non-Keralite Category-I’ will be treated as ‘Non-Keralite Category-II'. They are eligible for admission ONLY to the ‘State Merit’ seats in the Private Self-financing Engineering Colleges in the State and in the Management Quota seats in Self-financing Engineering institutions sponsored by Government. Non-Keralite candidates will not be considered for admission against reserved seats.
                        </p>
                        <h2 class="text-main-color">Eligibility Conditions</h2>
                        <p>
                            Architecture Course : Candidates who have passed 10+2 Senior Secondary School Certificate Examination or examinations recognized as equivalent thereto, with Mathematics as a subject of examination, with 50% marks in aggregate / Candidates who have passed 10+3 Diploma (any stream) recognized by Central/State Governments with 50% aggregate marks / Candidates with International Baccalaureate Diploma, after 10 years of schooling, with not less than 50% marks in aggregate and with Mathematics as compulsory subject of examination are eligible to apply. In addition, candidates will have to score 40% marks (80 marks out of 200) in the NATA (National Aptitude Test in Architecture).
                        </p>
                        <p>
                            10+2 or equivalent examination  with Mathematics as a subject by securing 50% marks in aggregate  OR  10+3 Diploma (any stream) with 50% aggregate marks with Mathematics as one of the  subject
                        </p>
                        <p>
                            In addition candidates should have 70 Marks out of 200 (35%)  for (NATA) 2019 on or before 10.06.2019
                        </p>
                        <p>
                            Reserved category (SC/ST/SEBC) candidates should also have obtained 40% marks in NATA, as per guidelines of Council of Architecture. (For details, visit <a class="text-red" href="http://www.nata.in/" target="_blank">www.nata.in</a>).
                        </p>
                        <h5 class="text-main-color">Apply On-line : <a href="http://www.cee-kerala.org/" class="text-red" target="_blank">www.cee.kerala.gov.in</a></h5>
                        <p class="text-red font-weight-600"># Abstract only. Contact Admission Team for more clarification or refer KEAM  Prospectus</p>
                    </div>
                    <div class="col-lg-4 col-md-4 sticky-sidebar">
                        <div class="widget">
                            <p>
                                <a class="text-red font-weight-600" href="https://ekccoa.linways.com/onlineadmission/barch/form_redirect.php?courseTypeId=14&formTypeId=2 " target="_blank"><h4>B.Arch Online Admission.</h4></a>
                            </p>
                            <h4 class="widget-title clearfix margin-top-30px"><span>Contact Us</span></h4>
                            <h6 class="margin-top-20px text-red">Admission Enquiry :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9446-009-824</span>
                            <h6 class="margin-top-20px text-red">EKC - College of Architecture :</h6>
                            <span class="d-block"><i class="fa fa-phone text-main-color margin-right-10px" aria-hidden="true"></i> +91 9744-500-040</span>
                            <span class="d-block"><i class="fa fa-envelope text-main-color margin-right-10px" aria-hidden="true"></i> coa@eranadknowledgecity.com</span>

                            <h6 class="margin-top-20px">Address :</h6>
                            <span class="d-block"><i class="fa fa-map text-main-color margin-right-10px" aria-hidden="true"></i> Eranad Knowledge City, Cherukulam</span>
                            <span class="margin-left-30px">Manjeri Malappuram Dist</span>
                            <span class="margin-left-30px"><br/>Kerala PIN 676122 </span>
                            <h6 class="margin-top-20px">Email :</h6>
                            <span class="d-block"><i class="fa fa-envelope-open text-main-color margin-right-10px" aria-hidden="true"></i> office@eranadknowledgecity.com </span>
                            <h6 class="margin-top-20px">Locate Us :</h6>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62624.7163207098!2d76.09392957741646!3d11.184321113569137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba64a8eac9c7a6f%3A0x11439d8329094129!2sEranad+Knowledge+City+Technical+Campus!5e0!3m2!1sen!2sin!4v1553062313905" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section>
            <div class="row no-gutters">
                <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                    <div class="padding-30px">

                    </div>
                </div>
                <div class="col-lg-4 background-blue">
                    <div class="padding-lr-45px padding-tb-30px text-white">
                        <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                        <p>The College of Architecture is one among the institutions in the first phase of the Knowledge City Project in advancing educational standards throughout Kerala.</p>
                        <ul class="margin-0px padding-0px list-unstyled">
                            <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                            <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                            <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                            <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                            <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                            <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                            <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 background-green">
                    <form class="dark-form padding-lr-45px padding-tb-30px">
                        <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                        <div class="form-row">
                            <div class="form-group col-md-6 text-black">
                                <label >Full Name</label>
                                <input type="text" class="form-control" id="inputName4" placeholder="Name">
                            </div>
                            <div class="form-group col-md-6 text-black">
                                <label >Email Address</label>
                                <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group text-black">
                            <label >Contact Number</label>
                            <input type="Number" class="form-control" id="inputAddress" placeholder="1234 Main St">
                        </div>
                        <div class="form-group text-black">
                            <label>Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <a href="#" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</a>
                    </form>

                </div>

            </div>
        </section>
    
@endsection

@section('scripts')

@endsection