@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-20px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Programs - Academics</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-COA/B.Arch.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Bachelor of Architecture (B.Arch.)</h3>
                        <p>
                            Degree/Course of 5 year duration affiliated to APJ Abdul Kalam Technological University (KTU). Architectural education in C.A.T takes the challenge of preparing students to deal the uncertainty of future.  This demands to focus on
                        </p>
                        <ol>
                            <li>Sustainability to conserve nature for managing climate change.</li>
                            <li>Exposure to new technologies like BIM, Virtual Reality and GIS.</li>
                            <li>Researching social structure to understand cultural requirements.</li>
                        </ol>
                        <h3>Methodology adopted to meet the requirements are:</h3>
                        <ul>
                            <li>Lecture by experts</li>
                            <li>Hands on workshops</li>
                            <li>Technology drills and workshops</li>
                            <li>Taking studios outside</li>
                            <li>Design outreach to deal with real problems</li>
                            <li>Heritage documentation workshop at different location</li>
                        </ul>
                    </div>
                </div>
                <div class="row margin-top-40px">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-COA/B.DES.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">B.Des (Furniture and Interior Design)</h3>
                        <small>(Degree/Course of 4 year duration affiliated to APJ Abdul Kalam Technological University (KTU) &AICTE)</small>
                        <p>B.Des – Bachelor of Design – 4 year degree program</p>
                        <p>
                            Design education in C.A.T   leading to B.Des is one of its kind in the state of Kerala.  India Design Council, Government of India and Government of Kerala  are keen to take India as world design hub through innovation and design management initiatives. B.Des of C.A.T addresses both issues of perception and to link crafts with design.  The program focuses on three fundamental aspects that we believe would  enhance value, employability and marketability.
                        </p>
                        <ul>
                            <li>Development of new materials for new uses.</li>
                            <li>Integrating the latest fabrication techniques, taking cues from craft, using 3D printers, laser cutter and NC-CNC machines and simulation studies.</li>
                            <li>Firmly ground innovation and entrepreneurial aspects of management in the program.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection