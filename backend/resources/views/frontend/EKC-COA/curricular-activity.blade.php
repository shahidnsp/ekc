@extends('frontend.EKC-COA.layout.app')

@section('content')
	<section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-20px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Programs - Co-Curricular Activity</h1>
                </div> 
                <div class="row">
                    <!-- <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-COA/B.Arch.jpg')}}" alt="">
                    </div> -->
                    <div class="col-md-12">
                        <!-- <h3 class="text-dark font-weight-600 font-3">Bachelor of Architecture (B.Arch.)</h3> -->
                        <p class="coming-soon">
                            we will update soon
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection