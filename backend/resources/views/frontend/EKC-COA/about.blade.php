@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section>
            <div class="container">
                <div class="row margin-tb-100px">
                    <div class="col-lg-6 background-white">
                        <div class="">
                            <div class="font-weight-300 wow fadeInUp">
                                <small class="text-main-color">Since 2013</small>
                                <h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Welcome to College Of Architecture<br/> Eranad Knowledge City</h1>
                                <p>
                                    The College of Architecture is one among the institutions in the first phase of the Knowledge City Project. The Knowledge City concept is first of its kind in Kerala and is set to pave the way for a radical revolution in advancing educational standards throughout Kerala. Eranad Knowledge City is set to be at the forefront of this revolution. At par with popular international education institutions, the Knowledge City will have a wide spectrum of educational institution in one single place, covering all major attributes of educational and living standards.
                                </p>
                                <p>
                                    The Eranad Knowledge City College of Architecture is an amalgamation of the best of all worlds- in terms of academics, design competitions, co-curricular and extra-curricular activities. The college encourages students to think outside the box, and gives them ample opportunity to do so. The students of Eranad Knowledge City College of Architecture are provided with the best of infrastructure for learning which comprise of tutorial halls, studios, computer centers, Laboratories apart from extensive outdoor facilities for real-time experimental learning. These apart, they are also provided with Wi-Fi enabled 20 mbps broadband internet facility for extending their knowledge horizon and learning experience
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeInUp">
                        <img src="{{url('frontend/img/EKC-COA/collage.png')}}" alt="" class="border-radius-10">
                    </div>
                    <div class="col-lg-12 background-white">
                        <div class="">
                            <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                                <p>
                                    The College of Architecture is the one that has features most important to you, as an individual person. Success as a student often depends on knowing who you are and what you want. The subject of architecture straddles two intertwined paths, engineering and the arts, and most people are drawn to one or the other. So, the college will fully accommodate your passion or may fill the gaps of your enthusiasm for architecture.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-tb-100px">
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-eye icon-large text-pink"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                            <p>
                                World class technology institutes on a welcoming campus with a unique college spirit,
                                which challenges and inspires all of our students to achieve their ultimate potential.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-paper-plane icon-large text-yellow"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                            <p>
                                Nurture learners who are creative, self-confident and principled in association with national
                                and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-lightbulb icon-large text-green"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                            <p>
                                Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                                in the teaching and learning process.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section>
        <div class="row no-gutters">
            <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                <div class="padding-30px">

                </div>
            </div>
            <div class="col-lg-4 background-blue">
                <div class="padding-lr-45px padding-tb-30px text-white">
                    <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                    <p>The College of Architecture is one among the institutions in the first phase of the Knowledge City Project in advancing educational standards throughout Kerala.</p>
                    <ul class="margin-0px padding-0px list-unstyled">
                        <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                        <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                        <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                        <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                        <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 background-green">
                <form class="dark-form padding-lr-45px padding-tb-30px">
                    <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6 text-black">
                            <label >Full Name</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6 text-black">
                            <label >Email Address</label>
                            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-black">
                        <label >Contact Number</label>
                        <input type="Number" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group text-black">
                        <label>Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <a href="#" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</a>
                </form>

            </div>

        </div>
    </section>
@endsection

@section('scripts')

@endsection