@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section class="background-gray padding-tb-25px">
        <div class="container">
            <h6 class="font-weight-600 text-extra-large font-3 text-capitalize float-md-left padding-tb-10px">EKC - Colleges of Architecture : Downloads</h6>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="{{url('myhome')}}" class="text-grey-4">Home</a></li>
                <li><a href="{{url('EKC-COA/index')}}" class="text-grey-4">EKC-COA</a></li>
                <li class="active">Downloads</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>

    <?php
        $downloads = \Illuminate\Support\Facades\Cache::remember('TCArchitectureDownloads', 2*60, function() {
            return \App\Download::where('department','Architecture')->get();
        });
    ?>
    <section class="padding-tb-50px background-light-grey text-center">
        <div class="container">
            <div class="row">
                @foreach($downloads as $download)
                    @if($download->type=='Photo')
                    <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                        <div class="downloads-item box-shadow-hover">
                            <i class="fa fa-file-image text-green" aria-hidden="true"></i>
                            <p>{{$download->name}}</p>
                            <a href="{{url('images/'.$download->file)}}" download="#">Download Now</a>
                        </div>
                    </div>
                    @elseif($download->type=='Link')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-link text-pink" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{$download->link}}" target="_blank">Visit Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='PDF')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-pdf text-pink" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('pdf/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='Doc')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-word text-yellow" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('doc/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @elseif($download->type=='Excel')
                        <div class="col-md-2 col-sm-3 col-6 margin-bottom-30px">
                            <div class="downloads-item box-shadow-hover">
                                <i class="fa fa-file-word text-yellow" aria-hidden="true"></i>
                                <p>{{$download->name}}</p>
                                <a href="{{url('excel/'.$download->file)}}" download="#">Download Now</a>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

@endsection

@section('scripts')

@endsection