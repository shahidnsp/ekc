@extends('frontend.EKC-COA.layout.app')

@section('content')
    <?php
        $sliders = \Illuminate\Support\Facades\Cache::remember('ARsliders', 2*60, function() {
            return \App\Slider::where('department','Architecture')->get();
        });
    ?>
    <!-- =========== Revelution Slider =========== -->
    <div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                @foreach($sliders as $slider)
                <li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>

     <?php
        $notifications=\App\Notification::where('department','Architecture')->get();
     ?>
    @if(count($notifications)>0)
    <section>
        <div class="breaking-news">
            <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                @foreach($notifications as $notification)
                <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                @endforeach
            </marquee>
        </div>
    </section>
    @endif
    <section class="padding-top-100px">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-md-7">
                    <div class="fadeInUp margin-bottom-20px">
                        <h1 class="font-weight-300 text-title-large font-3">Welcome to College of Architecture</h1>
                    </div>
                    <div class="margin-bottom-10px wow fadeInUp">
                        <p class="affiliated">Affiliated to Calicut University.</p>
                        <p class="approved">Approved by COA, Delhi and Government of Kerala.</p>
                        <p>
                            At EKC COA, we emphasize on an educational system that is rooted in architectural values and tradition to strengthen human values and professional ethics. This inspirational learning environment evolves over a design philosophy which centered around four pillars of learning that is; learning to know, to be, to do, and to live together. All these enhances learning experience as student spent time in workshops and design studios conceptualizing, discussing, working, debating, and presenting a wide variety of projects ranging from studio projects, completion design entries to unique live projects, engaging themselves with client and communities. The vision of the institution is to provide a serene and tranquil environment to nurture innovative thoughts in creative minds through EXPOSURE, EXPERIENCE and PRACTICE.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <img class="border-radius-10" src="{{url('frontend/img/EKC-COA/Architecture.jpg')}}" alt="">
                </div>
                <div class="col-md-12">
                    <div class="margin-bottom-60px wow fadeInUp">
                        <p>
                            The College of Architecture is one among the institutions in the first phase of the Knowledge City Project. The Knowledge City concept is first of its kind in Kerala and is set to pave the way for a radical revolution in advancing educational standards throughout Kerala. Eranad Knowledge City is set to be at the forefront of this revolution. At par with popular international education institutions, the Knowledge City will have a wide spectrum of educational institution in one single place, covering all major attributes of educational and living standards.This prospectus contains the rules and regulations applicable for selection and admission to B.Arch. courses at Eranad Knowledge City College of Architecture, Cherukulam, Manjeri . 50% of the seats are filled from the list of the Common Entrance Examination
                        </p>
                    </div>
                </div>
            </div>
            <div class="row margin-top-20px">
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-eye icon-large text-pink"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                        <p>
                            World class technology institutes on a welcoming campus with a unique college spirit,
                            which challenges and inspires all of our students to achieve their ultimate potential.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-paper-plane icon-large text-yellow"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                        <p>
                            Nurture learners who are creative, self-confident and principled in association with national
                            and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center hvr-grow">
                        <i class="fa fa-lightbulb icon-large text-green"></i>
                        <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                        <p>
                            Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                            in the teaching and learning process.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Facilities</h1>
                <span class="text-capitalize">The First Knowledge City Concept In Kerala</span>
            </div>

            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Library.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-main-color">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Well Furnished Library
                            </h2>
                            <p>The EKC have a spacious well furnished Library having sufficient volume of books, National and International available.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Radio.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-green">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Campus Radio
                            </h2>
                            <p>Our Campus Radio station is equipped with advanced technology devices in order to attain international standards.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-yellow">
                        <div class="text-left z-index-2 position-relative text-black">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                General Store
                            </h2>
                            <p>A general store is functioning in the campus to supply quality books, notebooks and other stationary items at reasonable price.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Stores.jpg')}}" alt="">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="padding-lr-30px padding-tb-60px background-overlay background-pink">
                        <div class="text-left z-index-2 position-relative text-white">
                            <h2 class="text-extra-large  margin-bottom-25px">
                                Conveyance
                            </h2>
                            <p>College buses ply on a regular basis to the campus from Thirur, Perinthalmanna, Vengara, Mukkam, Nilambur, Valancheri, etc...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="{{url('frontend/img/EKC-COA/EKC-Transport.jpg')}}" alt="">
                </div>
            </div>

        </div>
    </section>
    <?php
        $newses = \Illuminate\Support\Facades\Cache::remember('Arnewses', 2*60, function() {
            return \App\News::where('department','Architecture')->orderBy('id', 'desc')->take(4)->get();
        });
    ?>
    @if(count($newses)>0)
    <section class="padding-tb-100px">
        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Latest News & Event</h1>
                <span class="text-capitalize">The First Knowledge City Concept In Kerala</span>
            </div>
            <div class="row">
                @foreach($newses as $news)
                <div class="col-lg-3 col-md-6 sm-mb-45px">
                    <div class="blog-item thum-hover border-radius-15 hidden background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date border-radius-15 z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                        <hr>
                        <div class="padding-lr-30px">
                            <small class="margin-right-30px">By : <a href="#">Admin</a></small>
                        </div>
                        <hr class="margin-bottom-0px border-white">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif

@endsection

@section('scripts')
    <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
@endsection