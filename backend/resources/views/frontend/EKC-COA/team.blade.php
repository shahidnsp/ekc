@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section class="padding-top-50px">
        <div class="container">
            <div class="text-center margin-bottom-20px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">The website in maintenance mode.</h1>
                <span class="text-capitalize">Plaese contact this number :<b> +91 9744-500-040</b></span>
            </div>
            <div class="row wow fadeInUp">
                <div  class="offset-3 col-md-6"><img src="{{url('frontend/img/workerman.jpg')}}" alt=""></div>
            </div>
        </div>
    </section>
    <!-- <section class="padding-tb-100px">

        <div class="container">
            <div class="text-center margin-bottom-35px wow fadeInUp">
                <h1 class="font-weight-300 text-title-large font-3">Our Non Teaching Staff</h1>
                <span class="opacity-7">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elitdunt</span>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Web Designer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Amar Salah</a></h4>
                        <small>Programmer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Moh Elkheir</a></h4>
                        <small>Sales Manager</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.6s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sana Ali</a></h4>
                        <small>Web Designer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sara Elshakh</a></h4>
                        <small>Web Designer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Amar Salah</a></h4>
                        <small>Programmer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Moh Elkheir</a></h4>
                        <small>Sales Manager</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.6s">
                    <div class="team with-hover text-center hvr-float">
                        <div class="margin-bottom-20px position-relative overflow-hidden">
                            <img src="http://via.placeholder.com/370x400" alt="">
                            <div class="hover-option bag-dark text-center padding-top-n-20">
                                <div class="position-relative hight-full">
                                    <ul class="social-list light bottom-30px position-absolute">
                                        <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h4 class="margin-bottom-0px"><a href="#">Sana Ali</a></h4>
                        <small>Web Designer</small>
                        <p class="text-grey-2 margin-top-10px">
                            There are many variations of passages of Lorem Ipsum available
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </section> -->
@endsection

@section('scripts')

@endsection