@extends('frontend.EKC-COA.layout.app')

@section('content')
        <section class="padding-top-70px padding-bottom-30px">
            <div class="background-light-grey">
                <div class="text-center margin-bottom-50px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Think Big, Think Ahead</h1>
                    <span class="text-capitalize">The team behind Eranad knowledge City ...</span>
                </div> 
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="border-radius-150" src="{{url('frontend/img/directors/Dr. C.P.A Bava Haji.png')}}" alt="">
                        </div>
                        <div class="col-md-9">
                            <h2 class="font-weight-300 text-title-small font-3">CHAIRMAN & MANAGING TRUSTEE</h2>
                            <h3 class="text-dark font-weight-600 font-3">Dr. C.P.A Bava Haji</h3>
                            <p>
                                An esteemed authority with a rich and wide experience in various Business, Educational and Philanthropic activities. He has developed his ventures in India, Middle East and Mosambique. As the Chairman of Crescent International Group, he has a presence in different verticles like infrastructure development, tourism, manufacturing, retail and education activities.
                            </p>
                            <p>
                                He is the Chairman of Malabar Academic City, Malabar Dental College, Al-Ameen Engineering College, Al-Ameen Law College, and Crescent International Group, Dubai.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="container padding-tb-20px">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Adv. Shihab Mecheri.png')}}" alt="">
                                </div>
                                <div class="col-md-7">
                                    <h2 class="font-weight-300 text-title-small font-3">EXECUTIVE DIRECTOR <small class="text-medium font-weight-600">- General Secretary</small></h2>
                                    <p class="font-weight-300"></p>
                                    <h3 class="text-dark font-weight-600 font-3">Adv. Shihab Mecheri</h3>
                                    <p class="text-capitalize">HR. Academics & strategic management</p>
                                    <p>
                                        A law Graduate and MBA from Calicut University. Actively engaged as CEO of Malabar
                                        Academic City and Supriya Speciality Hospital and DQI Analytics. He also acts as General
                                        Secretary of Knowledge City Educational and Charitable Trust and Al Hind Educational and Charitable Trust.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/M.Veerankutti Haji.png')}}" alt="">
                                </div>
                                <div class="col-md-7">
                                    <h2 class="font-weight-300 text-title-small font-3">TREASURER</h2>
                                    <h3 class="text-dark font-weight-600 font-3">M.Veerankutti Haji</h3>
                                    <p>
                                        A professional man with a sound experience to back his credentials. Treasurer of Al-Ameen Engineering College. Activiley engaged as Treasurer of Al-Ameen Engineering
                                        College and Al-Ameen Law College.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container padding-tb-20px">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Mujeeb Rahman Kurikkal.png')}}" alt="">
                                </div>
                                <div class="col-md-7">
                                    <h2 class="font-weight-300 text-title-small font-3">EXECUTIVE DIRECTOR <small class="text-medium font-weight-600">- Vice Chairman</small></h2>
                                    <h3 class="text-dark font-weight-600 font-3">Mujeeb Rahman Kurikkal</h3>
                                    <p class="text-capitalize">Finance and Facility Management</p>
                                    <p>
                                        As a part of Kurikkal Business group, he ventured infrastructure projects, manufacturing,
                                        logistics and plantations. He also acts as Vice chairman of Knowledge City Educational and
                                        Charitable Trust and Al-Hind Educational and Charitable Trust.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Kamarudheen K.P.png')}}" alt="">
                                </div>
                                <div class="col-md-7">
                                    <h2 class="font-weight-300 text-title-small font-3">EXECUTIVE DIRECTOR <small class="text-medium font-weight-600">- Secretary</small></h2>
                                    <h3 class="text-dark font-weight-600 font-3">Kamarudheen K.P</h3>
                                    <p class="text-capitalize">Projects and Infrastructure Development</p>
                                    <p>
                                        As a Civil Engineering Professional, he initiated many infrastructure projects and educational institutions. Activiley engaged as Secretary of Al-Ameen Engineering College and Al-Ameen Law College. He also acts as Secretary of Knowledge City Educational
                                        and Charitable Trust and Al-Hind Educational and Charitable Trust.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="padding-bottom-70px">
            <div class="background-light-grey">
                <div class="text-center margin-bottom-20px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">Other Board of Directors</h1>
                    <span class="text-capitalize">The team Behind EKC</span>
                </div> 
                <div class="container padding-tb-20px">
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/A.M Mohammed Ali.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">A.M Mohammed Ali</h2>
                                    <p>
                                        Chairman of Nirman Constructions. Nirman Constructions is the leading
                                        construction company who built Haj House Karipur, Manjeri Stadium,
                                        MES Medical College, and MEA Engineering College. He is the Chairman of
                                        Al-Huda English School and Director of Unity Women's College.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Latheef V.P.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Latheef V.P</h2>
                                    <p>
                                        Chairman of Forum Group of Companies, Dubai and engaged in various
                                        business and service activities in Middle East, Africa and India. He is also the Vice Chairman of MH School Edappal.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Latheef V.P.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Latheef V.P</h2>
                                    <p>
                                        Chairman of Forum Group of Companies, Dubai and engaged in various
                                        business and service activities in Middle East, Africa and India. He is also the Vice Chairman of MH School Edappal.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Mehaboob Hassan Kurikkal.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Mehaboob Hassan Kurikkal</h2>
                                    <p>
                                        As a senior member of Kurikkal Business Group, he has a wide and deep experience in Business and service sectors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Abdul Hameed Kurikkal.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Abdul Hameed Kurikkal</h2>
                                    <p>
                                        As a member of Kurikkal Business Group, he actively engaged in various
                                        business and service activities. he is a Director board member of Unity
                                        Women's College and H.M College, Manjeri.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/C.P Moideen.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">C.P Moideen</h2>
                                    <p>
                                        He is activley engaged in retail business and real estate projects in Middle East. Secretary of Al- Ameen Engineering College.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Adv. Yasir Arafath Kalliyath.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Adv. Yasir Arafath Kalliyath</h2>
                                    <p>
                                        A member of Kalliyath Family from Tirur, he is the Chairman and Managing Director of Bharathi Steels. he is the Director of Benchmark International School, Homested projects, VRC Hospital and Al-Ameen Engineering
                                        College.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Kovval Amoo Mohammed.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Kovval Amoo Mohammed</h2>
                                    <p>
                                        Chairman of Kovval Group of Companies, Dubai. The group has strong
                                        presence in Trading real estate and retail sector. The group involves Al-
                                        Malmas Trading LLC, Malmas Al-Madina Investments LLC, Al-Heyam Electronics LLC, Park Shopping Center LLC Ltd. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Mohammed Haris T.P.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Mohammed Haris T.P</h2>
                                    <p>
                                        An expert in infrastructure development, logistics and real estate
                                        projects. Director board member of Al- Ameen Engineering College &
                                        College of Engineering Technology, Kannur. Based in Dubai, he is engaged in High tower projects and Al-Malik documents clearing Estabishments.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/M.Abdul Rahman Haji.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">M.Abdul Rahman Haji</h2>
                                    <p>
                                        Actively engaged in various business and academic ventures. Director board member of Al-Ameen Engineering College.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Dr. T.P Ali Akbar.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Dr. T.P Ali Akbar</h2>
                                    <p>
                                        Post graduate in General Medicine and completed MBBS from JJM
                                        Medical College, Davangare. Consultant Internal medicine physician
                                        based at Saudi Arabia.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Dr. Mansoor C Abdulla.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Dr. Mansoor C Abdulla</h2>
                                    <p>
                                        Post Graduate in General Medicine from Kasturba Medical College, Manipal and completed MBBS from Calicut Medical College. Currently, professor in Department of Internal Medicine, MES Medical College, Perinthalmanna.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Mahmood PP.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Mahmood PP</h2>
                                    <p>
                                        Chairman of AKR Business Group, Dubai. Actively engaged in various business sectors in India and UAE.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/Prof. Ahamed Koya.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">Prof. Ahamed Koya</h2>
                                    <p>
                                        Prof.Ahamed Koya is one of the senior academecian in the field of Engineering and Technology. He is an M.Tech from IIT, Mumbai. He is the Vice-Chairman of Al-Ameen Engineering College and Director of Daya Hospital, Thrissur.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-20px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="border-radius-150" src="{{url('frontend/img/directors/C.K Sulaiman.png')}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2 class="font-weight-300 text-title-small font-3">C.K Sulaiman</h2>
                                    <p>
                                        Mr. C.K Sulaiman engaged in various Business, Service and Philanthropic activities based in Kerala. CKM Business group and Sree Ram Auto fuels are his ventures.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('scripts')

@endsection