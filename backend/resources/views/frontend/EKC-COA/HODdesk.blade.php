@extends('frontend.EKC-COA.layout.app')

@section('content')
    <section class="padding-tb-70px">
        <div class="background-light-grey">
            <div class="container">
                <div class="margin-bottom-20px wow fadeInUp">
                    <h1 class="font-weight-300 text-title-large font-3">HOD’s Desk</h1>
                </div> 
                <div class="row">
                    <div class="col-md-3">
                        <img class="border-1 border-grey" src="{{url('frontend/img/EKC-COA/EKC-COA B.Arch HOD.jpg')}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <h3 class="text-dark font-weight-600 font-3">Ar. Nived Lal P.</h3>
                        <small>B.Arch., MCP - Master of City Planning (IIT Kharagpur)</small>
                        <p class="margin-bottom-20px margin-top-20px">
                            Welcome to Eranad Knowledge City College of Architecture. I congratulate you all on your decision to choose Architecture as your field of study. Architecture is the art and science of creating built environment by modifying physical environment for the required use. As an art, Architecture is essentially an abstract which involves the manipulation of the relationships of spaces, volumes, planes, masses and voids. In this world of rapid urbanization and globalization due to the modern technical advancement, it is important to deliver academic excellence in educational institutions. Understanding each student’s uniqueness and identity is very important in developing their skills, attitude and knowledge. 
                        </p>
                        <p>
                            We at Eranad Knowledge City College of Architecture are focusing on comprehensive student’s centric learning approach which includes various workshops, site visits, seminars, exhibitions, study tours and other academic and cultural events apart from the normal architectural curriculum to develop the students to the global level competition. As an institution we are not just committed to produce good architects, but ideal citizens to work for the betterment of our society. 
                        </p>
                        <!-- <p class="coming-soon">
                            we will update soon
                        </p> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection