@extends('frontend.layout.app')

@section('content')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <object data="{{url('frontend/download/EKCBrochure2018.pdf')}}" type="application/pdf" width="100%" height="600"></object>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection