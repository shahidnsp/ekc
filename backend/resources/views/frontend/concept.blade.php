@extends('frontend.layout.app')

@section('content')

	<section class="margin-tb-70px">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="font-weight-300 wow fadeInUp">
						<h1 class="font-weight-300 text-title-large font-3 margin-bottom-60px margin-top-30px">Knowledge City Concept</h1>
						<blockquote class="background-white padding-30px font-3  text-large">
							<i class="fa fa-quote-left margin-right-10px"></i>
								Inspired by the motto "People living harmony with nature, EKC incorporates the principles of new urbanism, To Learn, Live, Work and Play. Our
								developments follow a natural pattern which never harm the nature.
							<i class="fa fa-quote-right margin-left-10px"></i>
						</blockquote>
					</div>
				</div>
				<div class="col-md-3">
					<div class="dream padding-30px background-yellow border-radius-10">
						<h1 class="font-weight-300 text-title-large font-3 margin-bottom-10px">THE DREAM</h1>
						<p>
							Devote yourself to an idea. Go make it happen. Struggle on it. Overcome all changes, It's sure your dream will be a reality one day!
						</p>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-3 wow fadeInUp">
                    <div class="item hvr-bob">
                        <div class="hvr-bob">
                            <div class="text-left">
                                <h3 class="margin-tb-10px">
                                    <i class="fas fa-graduation-cap text-icon-large d-inlin-block margin-right-20px text-blue"></i> Learn
                                </h3>
                                <ul class="concept-list padding-0px">
                                    <li>Schools and Colleges</li>
                                    <li>Playground and Nursery Schools</li>
                                    <li>Management Institutes</li>
                                    <li>Executive Education Centres</li>
                                    <li>Post-graduate Institutes</li>
                                    <li>Adventure Training Institutes</li>
                                    <li>Vocational Training Institutes</li>
                                    <li>Sports & Music Academies</li>
                                    <li>Edutainment Centres</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 wow fadeInUp">
                    <div class="item hvr-bob">
                        <div class="hvr-bob">
                            <div class="text-left">
                                <h3 class="margin-tb-10px">
                                    <i class="fas fa-briefcase text-icon-large d-inlin-block margin-right-20px text-green"></i> Work
                                </h3>
                                <ul class="concept-list padding-0px">
                                    <li>IT/ITES</li>
                                    <li>Biotechnology Institutes</li>
                                    <li>R&D Centres Training Centres</li>
                                    <li>Fashion Design Centres</li>
                                    <li>Film Production Houses</li>
                                    <li>Animation Studios</li>
                                    <li>Professional Services</li>
                                    <li>Convention Centre</li>
                                    <li>City Management services</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 wow fadeInUp">
                    <div class="item hvr-bob">
                        <div class="hvr-bob">
                            <div class="text-left">
                                <h3 class="margin-tb-10px">
                                    <i class="fas fa-calendar-check text-icon-large d-inlin-block margin-right-20px text-yellow"></i> Live
                                </h3>
                                <ul class="concept-list padding-0px">
                                    <li>Villas and Apartments</li>
                                    <li>Studio and Serviced Apartments</li>
                                    <li>Rental Housing and Retiree Housing</li>
                                    <li>Family Entertainment Centre</li>
                                    <li>Health and Wellness Centres</li>
                                    <li>Spiritual Centres</li>
                                    <li>24x7 Power, Water Supply and Connectivity</li>
                                    <li>Social Infrastructure</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 wow fadeInUp">
                    <div class="item hvr-bob">
                        <div class="hvr-bob">
                            <div class="text-left">
                                <h3 class="margin-tb-10px">
                                    <i class="fas fa-futbol text-icon-large d-inlin-block margin-right-20px text-pink"></i> Play
                                </h3>
                                <ul class="concept-list padding-0px">
                                    <li>Budget and Luxury Hotels</li>
                                    <li>Clubs, Resorts and Spas</li>
                                    <li>Cricket Ground</li>
                                    <li>Golf Academy</li>
                                    <li>Theme Park & Eco Safari</li>
                                    <li>Water and Adventure Sports</li>
                                    <li>Nature Trail & Shopping Centres</li>
                                    <li>Street Food to Themed Restaurants</li>
                                    <li>Museums and Art Centres</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>

	<section class="margin-tb-50px">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="margin-bottom-40px font-weight-300 wow fadeInUp">
						<h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">The Master Plan</h1>
						<p>
							Among the first planned knowledge cities in Kerala, EKC optimally balance nature and urban infrastructure. The master plan of EKC has been designed in collaboration with internationally acclaimed architects. The master plan is well considered land character, building frontage, preservation of soil and vegetation and other design guidelines are also been taken into consideration while making the master plan.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<img src="{{url('frontend/img/plan.png')}}">
				</div>
				<div class="col-lg-4">
					<ol>
						<li>Entry.</li>
						<li>Food Court.</li>
						<li>Corporate Office.</li>
						<li>Helipad.</li>
						<li>Mosque.</li>
						<li>Engineering College.</li>
						<li>Business School.</li>
						<li>Architecture College.</li>
						<li>Staff Quarters.</li>
						<li>Luxury Villas.</li>
						<li>Sports Complex.</li>
						<li>Public School.</li>
						<li>Apartments.</li>
						<li>Polytechnic College.</li>
						<li>Pharmacy College.</li>
						<li>Garden Convention Center.</li>
						<li>Students Hostel.</li>
						<li>College of Commerce & Sciences.</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<h1 class="font-weight-300 text-title-large font-3 margin-bottom-40px">Our Vision</h1>
					<ul class="list-style-1 padding-0px">
						<li>To provide a wide range of education, research, IT and residential facilities of international standards.</li>
						<li>To be the nerve centre of Knowledge and Education providing high quality academic education creating aspiration in students towards the highest level of achievement.</li>
						<li>To create a society of progressive thinking individuals who will contribute to the individual development of the global community by initiating positive changes in the social fabric.</li>
						<li>To provide the finest quality education that will evolve in step with the changes that takes place globally in the field of education, commerce and arts.</li>
						<li>To send a new qualitative standard in education by employing innovative approaches.</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section>
        <div class="row no-gutters">
            <div class="col-lg-4 background-overlay" style="background-image: url('{{url('frontend/img/footer-contact.jpg')}}');">
                <div class="padding-30px">

                </div>
            </div>
            <div class="col-lg-4 background-blue">
                <div class="padding-lr-45px padding-tb-30px text-white">
                    <h1 class="font-weight-300 text-title-med font-3 margin-bottom-20px">Get in touch</h1>
                    <p>The first Knowledge City concept in kerala. EKC offers a wide Spectrum of Education, Research, Career and Residential opportunities to its customers.</p>
                    <ul class="margin-0px padding-0px list-unstyled">
                        <li class="padding-tb-7px"><i class="far fa-hospital margin-right-10px"></i> Eranad Knowledge City</li>
                        <li class="padding-tb-7px"><i class="far fa-map margin-right-10px"></i> Cherukulam, Manjeri,</li>
                        <li class="padding-tb-7px"><i class="margin-right-30px"></i> Malappuram District, Kerala</li>
                        <li class="padding-tb-7px"><i class="far fa-bookmark margin-right-10px"></i> PIN 676-122</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9744-500-040</li>
                        <li class="padding-tb-7px"><i class="fas fa-phone margin-right-10px"></i> Mob: +91 9544-500-040</li>
                        <li class="padding-tb-7px"><i class="far fa-envelope-open margin-right-10px"></i> office@eranadknowledgecity.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 background-green">
                <form class="dark-form padding-lr-45px padding-tb-30px">
                    <h1 class="font-weight-300 text-black text-title-med font-3 margin-bottom-20px">Contact Us</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6 text-black">
                            <label >Full Name</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6 text-black">
                            <label >Email Address</label>
                            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-black">
                        <label >Contact Number</label>
                        <input type="Number" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group text-black">
                        <label>Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <a href="#" class="btn-sm  btn-lg btn-block border-2 border-back text-black text-center font-weight-bold text-uppercase rounded-0 padding-5px">Send Now</a>
                </form>

            </div>

        </div>
    </section>

@endsection

@section('scripts')

@endsection