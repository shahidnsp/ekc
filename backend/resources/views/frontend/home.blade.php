@extends('frontend.layout.app')

@section('content')

    <?php
        $sliders = \Illuminate\Support\Facades\Cache::remember('sliders', 2*60, function() {
            return \App\Slider::where('department','Main')->get();
        });
    ?>

    <!-- =========== Revelution Slider =========== -->
    	<div id="rev_slider_18_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="the-nile-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    		<div id="rev_slider_18_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
    			<ul>
    				<!-- SLIDE  -->
    				@foreach($sliders as $slider)
    				<li data-index="rs-6{{$slider->id}}" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    					<!-- MAIN IMAGE -->
    					<img src="{{url('images/'.$slider->photo)}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-blurstart="-2" data-blurend="0" data-offsetstart="2 2" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
    				</li>
    				@endforeach
    				{{--<!-- SLIDE  -->
    				<li data-index="rs-65" data-transition="3dcurtain-horizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    					<!-- MAIN IMAGE -->
    					<img src="{{url('frontend/img/slider-2.jpg')}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Power0.easeIn" data-scalestart="115" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="2 3" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
    				</li>
    				<!-- SLIDE  -->
    				<li data-index="rs-67" data-transition="3dcurtain-vertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="frontend/img/slider-3.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    					<!-- MAIN IMAGE -->
    					<img src="{{url('frontend/img/slider-3.jpg')}}" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="2 3" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
    				</li>--}}
    			</ul>
    			<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    		</div>
    	</div>
    	 <?php
            $notifications=\App\Notification::where('department','Main')->get();
         ?>
         @if(count($notifications)>0)
        <section>
            <div class="breaking-news">
                <marquee behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                    @foreach($notifications as $notification)
                    <a href="{{$notification->link}}" target="_blank">{{$notification->title}}</a>
                    @endforeach
                </marquee>
            </div>
        </section>
        @endif

    	<!-- =========== Our Vision and Mission =========== -->
    	<section class="padding-top-100px">
    		<div class="text-center margin-bottom-50px wow fadeInUp">
    			<h1 class="font-weight-300 text-title-large font-3">Welcome to Eranad Knowledge City</h1>
    			<span class="text-capitalize">The first knowledge city concept in kerala</span>
    		</div>
    		<div class="container">
    			<div class="row">
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-eye icon-large text-pink"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Vision</h2>
                            <p>
                                World class technology institutes on a welcoming campus with a unique college spirit,
                                which challenges and inspires all of our students to achieve their ultimate potential.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.2s">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-paper-plane icon-large text-yellow"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Mission</h2>
                            <p>
                                Nurture learners who are creative, self-confident and principled in association with national
                                and international strategic partners. We develop global citizens who are ready to make important contribution to the world around them.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 sm-mb-30px wow fadeInUp" data-wow-delay="0.4s">
                        <div class="text-center hvr-grow">
                            <i class="fa fa-lightbulb icon-large text-green"></i>
                            <h2 class="text-extra-large text-main-color margin-tb-10px">Our Philosophy</h2>
                            <p>
                                Every student has an individual and a cultural set of experience. We inculcate their skills and interests, which must be considered
                                in the teaching and learning process.
                            </p>
                        </div>
                    </div>
                </div>
    		</div>
    	</section>
        <section class="padding-tb-50px">
            <div class="container">
                <div class="about-us ">
                    <div class="row no-gutters margin-0px padding-0px box-shadow">
                        <div class="col-lg-6 background-grey-4">
                            <div class="padding-45px sm-padding-30px">
                                <div class="margin-bottom-40px font-weight-300 wow fadeInUp">
                                    <small class="text-main-color">Since 2013</small>
                                    <h1 class="font-weight-300 text-title-large font-3">About Eranad Knowledge City</h1>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 wow fadeInUp">
                                        <div class="text-left">
                                            <p>
                                                Eranad Knowledge City,
                                                Kerala's first planned knowledge city is being developed
                                                by an association of Kerala's leading academecians and entrepreneurs.
                                            </p>
                                            <p>
                                                Located in the centre
                                                part of God's own country, Kerala, the lush green 100 acre
                                                Campus is developed in the picturesque landscape of green
                                                hills, 8 kilometers away from
                                                Manjeri town in Malappuram
                                                District. Master plan for EKC is
                                                developed by internationally
                                                renowned design consultants
                                                and it is based on "People living harmony with nature". The
                                                master plan (current development plan - 100 acres) incorporates the principles of new
                                                urbanism. Our developments
                                                follows a natural pattern, and
                                                without harming the nature.
                                            </p>
                                            <p>
                                                The first phase of EKC
                                                has launched in 2012 and became operational in 2013. The
                                                first phase included an Engineering college, Arcitecture
                                                college, Commerce and Science college and a Public school.
                                                EKC is also having tie
                                                up with MNC'S like TCS, sify
                                                for conducting online public
                                                exams. For encouraging startups, EKC has tie ups with Kerala start up mission and engaging in various funded projects
                                                with the confidence of completion of first phase within
                                                4 years of its launching, EKC now moving to its next stage,
                                                phase 2. The second phase
                                                will feature a garden convention centre, sports complex,
                                                polytechnic college and a
                                                pharmacy college. Student
                                                hostels, and apartments also
                                                will be a part of this phase.
                                                Planning for different sports
                                                academies, phase 2 will
                                                emerge as a sporting destination of nation reputation.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 background-overlay wow fadeIn" style="background-image: url('frontend/img/About Eranad Knowledge City.png');">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="padding-bottom-50px">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 wow fadeInUp">
                        <div class="item hvr-bob">
                            <div class="hvr-bob">
                                <div class="text-left">
                                    <h3 class="margin-tb-10px">
                                        <i class="fas fa-graduation-cap text-icon-large d-inlin-block margin-right-20px text-blue"></i> Learn
                                    </h3>
                                    <ul class="concept-list padding-0px">
                                        <li>Schools and Colleges</li>
                                        <li>Playground and Nursery Schools</li>
                                        <li>Management Institutes</li>
                                        <li>Executive Education Centres</li>
                                        <li>Post-graduate Institutes</li>
                                        <li>Adventure Training Institutes</li>
                                        <li>Vocational Training Institutes</li>
                                        <li>Sports & Music Academies</li>
                                        <li>Edutainment Centres</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 wow fadeInUp">
                        <div class="item hvr-bob">
                            <div class="hvr-bob">
                                <div class="text-left">
                                    <h3 class="margin-tb-10px">
                                        <i class="fas fa-briefcase text-icon-large d-inlin-block margin-right-20px text-green"></i> Work
                                    </h3>
                                    <ul class="concept-list padding-0px">
                                        <li>IT/ITES</li>
                                        <li>Biotechnology Institutes</li>
                                        <li>R&D Centres Training Centres</li>
                                        <li>Fashion Design Centres</li>
                                        <li>Film Production Houses</li>
                                        <li>Animation Studios</li>
                                        <li>Professional Services</li>
                                        <li>Convention Centre</li>
                                        <li>City Management services</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 wow fadeInUp">
                        <div class="item hvr-bob">
                            <div class="hvr-bob">
                                <div class="text-left">
                                    <h3 class="margin-tb-10px">
                                        <i class="fas fa-calendar-check text-icon-large d-inlin-block margin-right-20px text-yellow"></i> Live
                                    </h3>
                                    <ul class="concept-list padding-0px">
                                        <li>Villas and Apartments</li>
                                        <li>Studio and Serviced Apartments</li>
                                        <li>Rental Housing and Retiree Housing</li>
                                        <li>Family Entertainment Centre</li>
                                        <li>Health and Wellness Centres</li>
                                        <li>Spiritual Centres</li>
                                        <li>24x7 Power, Water Supply and Connectivity</li>
                                        <li>Social Infrastructure</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 wow fadeInUp">
                        <div class="item hvr-bob">
                            <div class="hvr-bob">
                                <div class="text-left">
                                    <h3 class="margin-tb-10px">
                                        <i class="fas fa-futbol text-icon-large d-inlin-block margin-right-20px text-pink"></i> Play
                                    </h3>
                                    <ul class="concept-list padding-0px">
                                        <li>Budget and Luxury Hotels</li>
                                        <li>Clubs, Resorts and Spas</li>
                                        <li>Cricket Ground</li>
                                        <li>Golf Academy</li>
                                        <li>Theme Park & Eco Safari</li>
                                        <li>Water and Adventure Sports</li>
                                        <li>Nature Trail & Shopping Centres</li>
                                        <li>Street Food to Themed Restaurants</li>
                                        <li>Museums and Art Centres</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

         <?php
            $testimonials=\App\Testimonial::where('department','Main')->get();
         ?>

        @if(count($testimonials)>0)
    	<section class="padding-tb-50px">
    		<div class="container">
    			<div class="text-center margin-bottom-35px wow fadeInUp">
    				<h1 class="font-weight-300 text-title-large font-3">Testimonials</h1>
                    <span class="text-capitalize">The first knowledge city concept in kerala</span>
    			</div>
    			<div class="testimonial-carousel owl-carousel owl-theme wow fadeInUp">
    			    @foreach($testimonials as $testimonial)
    				<div class="item margin-lr-15px">
    					<div class="background-gray padding-30px">
    						<div class="float-left width-50px margin-right-20px">
    							<img class="border-radius-100" src="{{url('images/'.$testimonial->photo)}}" alt="">
    						</div>
    						<h4 class="margin-bottom-0px">{{$testimonial->name}}</h4>
    						<small>{{$testimonial->designation}}</small>
    						<hr>
    						<p>{{$testimonial->description}}</p>
    					</div>
    				</div>
    				@endforeach
    			</div>
    		</div>
    	</section>
    	@endif
    	<?php
            $newses = \Illuminate\Support\Facades\Cache::remember('newses', 2*60, function() {
                return \App\News::where('department','Main')->orderBy('id', 'desc')->take(3)->get();
            });
        ?>

        @if(count($newses)>0)
    	<section class="padding-tb-50px">
    		<div class="container">
    			<div class="text-center margin-bottom-35px fadeInUp">
    				<h1 class="font-weight-300 text-title-large font-3">Latest News & Updates</h1>
                    <span class="text-capitalize">The first knowledge city concept in kerala</span>
    			</div>
    			<div class="row">
    			    @foreach($newses as $news)
    				<div class="col-lg-4 col-md-6 sm-mb-30px wow fadeInUp">
    					<div class="blog-item thum-hover background-white hvr-float hvr-sh2">
    						<div class="position-relative">
    							<div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
    								{{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
    							</div>
    							<a href="#">
    								<div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
    							</a>
    						</div>
    						<a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
    						<hr>
    						<div class="padding-lr-30px">
    							<span class="margin-right-30px">By : <a href="#">Technical Campus</a></span>
    							<span class="margin-right-30px">In : <a href="#">News</a></span>
    						</div>
    						<hr class="margin-bottom-0px border-white">
    					</div>
    				</div>
    				@endforeach

    			</div>
    		</div>
    	</section>
        @endif

@endsection

@section('scripts')
    <!-- REVOLUTION JS FILES -->
    <script src="{{url('frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="{{url('frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{url('frontend/js/extensions/revolution.extension.video.min.js')}}"></script>



    </script>

@endsection
