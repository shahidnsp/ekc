@extends('frontend.layout.app')

@section('content')

    <section class="background-gray padding-tb-25px">
        <div class="container">
            <h6 class="font-weight-600 text-extra-large font-3 text-capitalize float-md-left padding-tb-10px">EKC - Latest Updates and News</h6>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="/" class="text-grey-4">EKC</a></li>
                <li><a href="{{url('myhome')}}" class="text-grey-4">Home</a></li>
                <li class="active">Updates</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>

    <section class="padding-tb-100px">
        <div class="container">
            <div class="row">
                @foreach($newses as $news)
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                {{\Carbon\Carbon::parse($news->created_at)->format('d/m Y')}}
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="{{url('images/'.$news->photo)}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">{{mb_strimwidth( $news->title, 0, 50, '...' )}}</a>
                        <hr>
                    </div>
                </div>
                @endforeach
                {{--<div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 margin-bottom-20px wow fadeInUp">
                    <div class="blog-item thum-hover background-white hvr-float hvr-sh2">
                        <div class="position-relative">
                            <div class="date z-index-10 width-50px padding-10px background-main-color text-white text-center position-absolute top-20px left-20px">
                                17/03 2019
                            </div>
                            <a href="#">
                                <div class="item-thumbnail background-dark"><img src="http://via.placeholder.com/350x195')}}" alt=""></div>
                            </a>
                        </div>
                        <a href="#" class="text-extra-large margin-tb-20px d-block padding-lr-30px">Don’t spend time beating on a wall, hoping to trans ...</a>
                        <hr>
                    </div>
                </div>--}}
            </div>
            <div class="row">
                {!! $newses->links() !!}
            </div>
        </div>
    </section>
@endsection

@section('scripts')

@endsection