	@extends('frontend.layout.app')

	@section('content')

	<div class="padding-tb-100px background-overlay fixed" style="background-image: url('{{url('frontend/img/about-ekc-1.jpg')}}');">
		<div class="container z-index-2 position-relative">
			<div class="row">
				<div class="col-md-7">
					<div class="background-dark-alpha padding-50px text-white">
						<a href="#" class="font-weight-300 text-title-large font-3 d-block margin-bottom-20px">Technical Campus</a>
						<small class="text-second-color text-medium">Affiliated to APJ Abdul Kalam Technological University Approved by AICTE Delhi and Government of Kerala</small>
						<div class="margin-bottom-20px">
							EKC Technical Campus is the flagship project
							of Eranad Knowledge City EKC TC is lead by
							team of educated, enlightened, experienced
							technocrats with a vision firmly determined
							to promote high quality education and striv
							ing to provide every facility for achieving ex
							cellence. The college is affiliated to the APJ
							Abdul Kalam Technical University and Calicut
							University, Kerala
						</div>
						<a href="{{url('EKC-TC/index')}}" class="btn btn-sm background-main-color border-radius-2 text-white padding-lr-15px border-0 opacity-8">View More</a>
					</div>
				</div>
				<div class="col-md-5"></div>

			</div>
		</div>
	</div>
	<div class="padding-tb-100px background-overlay fixed" style="background-image: url('{{url('frontend/img/home-Architecture.jpg')}}');">
		<div class="container z-index-2 position-relative">
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-7">
					<div class="background-dark-alpha padding-50px text-white">
						<a href="#" class="font-weight-300 text-title-large font-3 d-block margin-bottom-20px">College of Architecture</a>
						<small class="text-second-color text-medium">Affiliated to Calicut University, Approved by COA, Delhi and Government of Kerala</small>
						<div class="margin-bottom-20px">
							At EKC COA, we emphasize on an educational
							system that is rooted in architectural values
							and tradition to strengthen human values and
							professional ethics. This inspirational learning
							environment evolves over a design philosophy
							which centered around four pillars of learning
							that is; learning to know, to be, to do, and to
							live together.
						</div>
						<a href="{{url('EKC-COA/index')}}" class="btn btn-sm background-main-color border-radius-2 text-white padding-lr-15px border-0 opacity-8">View More</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="padding-tb-100px background-overlay fixed" style="background-image: url('{{url('frontend/img/home-CommerceSciences.jpg')}}');">
		<div class="container z-index-2 position-relative">
			<div class="row">
				<div class="col-md-7">
					<div class="background-dark-alpha padding-50px text-white">
						<a href="#" class="font-weight-300 text-title-large font-3 d-block margin-bottom-20px">College of Commerce & Sciences</a>
						<small class="text-second-color text-medium">Affiliated to Calicut University, Approved by Government of Kerala</small>
						<div class="margin-bottom-20px">
							The College of Commerce & Sciences offers
							programs that create graduates who are
							aware of and can interact with contemporary
							thoughts and research in their respective
							specializations and utilize such in their pro
							fessional lives. The College creates a stim
							ulating academic environment in which the
							students are given hands on expertise on the
							concepts they learn by assisting them with in
							dustry internships, creative assignments and
							participation in various discussion forums.
						</div>
						<a href="{{url('EKC-CS/index')}}" class="btn btn-sm background-main-color border-radius-2 text-white padding-lr-15px border-0 opacity-8">View More</a>
					</div>
				</div>
				<div class="col-md-5"></div>

			</div>
		</div>
	</div>
	<div class="padding-tb-100px background-overlay fixed" style="background-image: url('{{url('frontend/img/home-Public.jpg')}}');">
		<div class="container z-index-2 position-relative">
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-7">
					<div class="background-dark-alpha padding-50px text-white sm-tb-15px">
						<a href="#" class="font-weight-300 text-title-large font-3 d-block margin-bottom-20px ">Public School</a>
						<small class="text-second-color text-medium">CBSE Syllabus</small>
						<div class="margin-bottom-20px">
							To create an exemplary society with respon
							sible citizen, who are Enlightened with values
							and empowered with Soft skill and life skill, and
							there by provide a steady stream of leaders to
							the nation Capable of being on the driving seat
							of the world.
						</div>
						<a href="{{url('EKC-SCHOOL/index')}}" class="btn btn-sm background-main-color border-radius-2 text-white padding-lr-15px border-0 opacity-8">View More</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	@endsection

	@section('scripts')

	@endsection