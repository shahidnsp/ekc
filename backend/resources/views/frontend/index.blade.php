<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="Eranad Knowledge City , EKC, Engineering college ,BTech, , BAarch ,BBA, Commerce and Science College ,ekc Public School" />
    <meta property="og:site_name" content="Eranad Knowledge City , EKC, Engineering college ,BTech, , BAarch ,BBA, Commerce and Science College ,ekc Public School" />
    <meta property="og:url" content="http://ekc.edu.in/" />
    <meta property="og:description" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram" />
    <meta property="og:type" content="website" />
	<title>Eranad Knowledge City  Cherukulam  Manjeri Malappuram Kerala | EKC  Engineering College near Manjeri Malappuram Kerala</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram">
    <meta name="keywords" content="Eranad Knowledege city  Cherukulam  Manjeri Malappuram Kerala,   EKC  Engineering college near Manjeri  ,Engineering college near Malappuram , ekc technical campus peace public school Ekc architecture college, BAarch course in Manjeri malappuram kerala, Commerce &amp; Science college in Manjeri  malappuram kerala  ,ekc public school  Manjeri malappuram kerala , Kerala engineering college, Malappuram engineering college  , Best Engineering College , Best Architecture College , Best Commerce and Science College , Best BBA College , BTech in Civil in Malappuram  , Btech in Mechancial in malappuram" />
    <link href="{{url('frontend/img/favicon.png')}}" rel="icon">
	<link rel="stylesheet" href="{{url('frontend/css/landing-page.css')}}" />
	<link rel="stylesheet" href="{{url('frontend/css/bootstrap-alert.css')}}" />
	<link rel="stylesheet" href="{{url('frontend/css/nifty.css')}}" />
	<link rel="stylesheet" href="{{url('frontend/css/animate.min.css')}}" />  
</head>
<style>
    .alert-title a{
             border-bottom:0px; 
    }
</style>
<body>
    <div id="container" class="effect aside-float aside-bright mainnav-lg"></div>
    <div id="wrapper">
		<div id="bg"></div>
		<div id="overlay"></div>
		<div id="main">
			<header id="header">
				<h1><a href="#">Eranad Knowledge City</a></h1>
				<p>The first knowledge city concept in kerala</p>
				<nav>
					<ul>
						<li><a href="{{url('myhome')}}" class="icon fa fa-university">home</a></li>
						<li><a href="{{url('institutions')}}" class="icon fa fa-graduation-cap">Institutions</a></li>
						<li><a href="{{url('about')}}" class="icon fa fa-info">about Us</a></li>
						<li><a href="{{url('admission')}}" class="icon fa fa-certificate">admission</a></li>
						<li><a href="{{url('360')}}" class="icon fa fa-street-view">360<nobr>°</nobr></a></li>
						<li><a href="{{url('videos')}}" class="icon fa fa-play">My EKC</a></li>
						<li><a href="{{url('contact')}}" class="icon fa fa-address-book">Contact Us</a></li>
						<li><a href="{{url('updates')}}" class="icon fa fa-newspaper-o">Updates</a></li>
						
					</ul>
				</nav>
				<!-- <nav>
					<ul>
						<li>FOR NEXT LINE</li>
					</ul>
				</nav> -->
			</header>
			<footer id="footer">
				<a href="https://www.google.com/maps/dir/11.1202696,76.1204065/ekc+college/@11.1373225,76.1203546,13z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x3ba6314866b57ad3:0xcd66216a4125ccb4!2m2!1d76.1890392!2d11.1310694" target="_blank">
					<span class="fa fa-map-marker" aria-hidden="true"> </span>
					<span class="copyright">&copy; <a href="#">EKC</a></span>
				</a>
			</footer>
		</div>
	</div>
	<script>
		window.onload = function() { document.body.className = ''; }
		window.ontouchmove = function() { return false; }
		window.onorientationchange = function() { document.body.scrollTop = 0; }
	</script>
    
    <!--JAVASCRIPT-->
    <!--=================================================-->
    <script src="{{url('frontend/js/jquery.min.js')}}"></script>
    <script src="{{url('frontend/js/nifty.min.js')}}"></script>


    <?php
        $notifications=\App\Popup::where('department','Main')->get();
     ?>

  	<script>

  	    @foreach($notifications as $notification)
            $.niftyNoty({
                type: 'success',
                container: 'floating',
                floating: {
                    position: "top-right",
                    animationIn: "bounceInUp"

                },

                /* icon: 'fa fa-times-circle', animationOut: "bounceInDown", title: '<a href="{{$notification->link}}">Admission 2019 Enquiry</a>',
                 message: '<a href="{{url('admission')}}"><h2>+91 9744-500-040, +91 9446-009-824</h2></a>',*/
                 icon: 'fa fa-times-circle', animationOut: "bounceInDown", title: '<a href="{{$notification->link}}">{{$notification->title}}</a>',
                 message: '<a href="{{$notification->link}}"><h2>{{$notification->description}}</h2></a>',
                closeBtn: true,
                onShow: function() {
                }
            });
        @endforeach

    </script> 
</body>
</html>
