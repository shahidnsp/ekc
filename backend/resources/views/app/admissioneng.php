<div class="container-fluid pt-30">
<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<!-- Title -->
<div class="row heading-bg">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h5 class="txt-dark">Admission</h5>
    </div>
    <!-- Breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li class="pull-right">
                <label class="switch">
                    <input ng-model="show1" type="checkbox">
                    <span class="slider"></span>
                </label>
                <label class="switch">
                    <input ng-model="show2" type="checkbox">
                    <span class="slider"></span>
                </label>
                <label class="switch">
                    <input ng-model="show3" type="checkbox">
                    <span class="slider"></span>
                </label>
            </li>
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!-- /Title -->

<div class="row">
    <div class="col-md-12">
        <div ng-show="admissionedit" class="row">
            <div class="panel panel-default card-view">
                <header class="widget-header">
                    <h5 class="widget-title">New Admission</h5>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <form class="form-horizontal"  ng-submit="addAdmission();">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-4">
                                <input type="date" id="firstname" class="form-control" ng-model="newadmission.date" required=""/>
                            </div>

                            <label for="" class="col-sm-1 control-label">Name</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Course</label>
                            <div class="col-sm-8">
                                <select  class="form-control" ng-model="newadmission.course" required="">
                                    <option value="">Select</option>
                                    <option value="Civil Engineering">Civil Engineering</option>
                                    <option value="Mechanical Engineering">Mechanical Engineering</option>
                                    <option value="Electronics & Communication Engineering">Electronics & Communication Engineering</option>
                                    <option value="Computer Science and Engineering">Computer Science and Engineering</option>
                                    <option value="Safety & Fire Engineering">Safety & Fire Engineering</option>
                                    <option value="Science & Humanities">Science & Humanities</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-8">
                                <textarea   class="form-control" ng-model="newadmission.address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Father Name</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.fathername" />
                            </div>

                            <label for="" class="col-sm-1 control-label">Student Phone</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.studentphone"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Father Phone</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.fatherphone" />
                            </div>

                            <label for="" class="col-sm-1 control-label">Caste</label>
                            <div class="col-sm-3">
                                <select  class="form-control" ng-model="newadmission.caste" required="">
                                    <option value="">Select</option>
                                    <option value="General">General</option>
                                    <option value="OBC">OBC</option>
                                    <option value="OEC">OEC</option>
                                    <option value="SC/ST">SC/ST</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">+2 Reg</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.plustworegno" />
                            </div>

                            <label for="" class="col-sm-1 control-label">Year Of Passing</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.yearofpass"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">+2 Mark</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.plustwomark" />
                            </div>

                            <label for="" class="col-sm-1 control-label">+2 Percentage</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.plustwomarkper"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-4">
                                <select  class="form-control" ng-model="newadmission.gender" required="">
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <label for="" class="col-sm-1 control-label">DOB</label>
                            <div class="col-sm-3">
                                <input type="date"  class="form-control" ng-model="newadmission.dateofbirth" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">KEAM Reg</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.keamregno" />
                            </div>

                            <label for="" class="col-sm-1 control-label">JEE Reg</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.jeeregno"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">JEE Score</label>
                            <div class="col-sm-4">
                                <input type="text"  class="form-control" ng-model="newadmission.jeescore" />
                            </div>

                            <label for="" class="col-sm-1 control-label">NATA Score</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newadmission.natascore"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">EKC Score</label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control" ng-model="newadmission.ekcscore" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 text-right">
                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                <button type="button" class="btn btn-default btn-xs" ng-click="cancelAdmission();">Cancel</button>
                            </div>
                        </div>

                    </form>
                    <div class="lightBoxGallery" ng-show="imageViewAdmission">
                        <a class="example-image-link" href="images/{{curAdmission.photo}}" data-lightbox="item-list" data-title="">
                            <img src="images/{{curAdmission.photo}}" width="100px">
                        </a>
                    </div>
                    <hr>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div>

    </div>
</div>
<div ng-hide="showSingle">
    <div class="row">
        <div class="panel panel-default card-view">
            <div class="panel-body">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_length" >
                                    <label>
                                        Show
                                        <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button  ng-hide="admissionedit" ng-click="newAdmission();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add Admission</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="dataTables_filter pull-right">
                                    <label>
                                        Search:
                                        <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                    <table id="datable_1" class="table table-hover display  pb-30" >
                        <thead>
                        <tr>
                            <th>$</th>
                            <th>AppNo</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Course</th>
                            <th>Address</th>
                            <th>Father Name</th>
                            <th>Student Phone</th>
                            <th>Father Phone</th>
                            <th>Caste</th>
                            <th>+2 Reg</th>
                            <th>Year of Pass</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th>+2 Mark</th>
                            <th>+2 %</th>
                            <th>KEAM Reg</th>
                            <th>JEE Reg</th>
                            <th>JEE Score</th>
                            <th>NATA Score</th>
                            <th>EKC Score</th>
                            <th ng-show="show1">Created_at</th>
                            <th ng-show="show2">Updated_at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>$</th>
                            <th>AppNo</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Course</th>
                            <th>Address</th>
                            <th>Father Name</th>
                            <th>Student Phone</th>
                            <th>Father Phone</th>
                            <th>Caste</th>
                            <th>+2 Reg</th>
                            <th>Year of Pass</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th>+2 Mark</th>
                            <th>+2 %</th>
                            <th>KEAM Reg</th>
                            <th>JEE Reg</th>
                            <th>JEE Score</th>
                            <th>NATA Score</th>
                            <th>EKC Score</th>
                            <th ng-show="show1">Created_at</th>
                            <th ng-show="show2">Updated_at</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <tr ng-repeat="admission in listCount  = (admissions | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                            <td>{{admission.appno}}</td>
                            <td>{{admission.date | date:'dd-M-yyyy'}}</td>
                            <td>{{admission.name}}</td>
                            <td>{{admission.course}}</td>
                            <td>{{admission.address}}</td>
                            <td>{{admission.fathername}}</td>
                            <td>{{admission.studentphone}}</td>
                            <td>{{admission.fatherphone}}</td>
                            <td>{{admission.caste}}</td>
                            <td>{{admission.plustworegno}}</td>
                            <td>{{admission.yearofpass}}</td>
                            <td>{{admission.gender}}</td>
                            <td>{{admission.dateofbirth | date:'dd-M-yyyy'}}</td>
                            <td>{{admission.plustwomark}}</td>
                            <td>{{admission.plustwomarkper}}</td>
                            <td>{{admission.keamregno}}</td>
                            <td>{{admission.jeeregno}}</td>
                            <td>{{admission.jeescore}}</td>
                            <td>{{admission.natascore}}</td>
                            <td>{{admission.ekcscore}}</td>
                            <td ng-show="show1">{{admission.created_at}}</td>
                            <td ng-show="show2">{{admission.updated_at}}</td>
                            <td>
                                <div  class="btn-group btn-group-xs" role="group">
                                    <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editAdmission(admission);">
                                        <i class="ti-pencil"></i>
                                    </button>
                                    <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteAdmission(admission);">
                                        <i class="ti-trash"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearfix" ng-show="admissions.length > numPerPage">
            <pagination
                ng-model="currentPage"
                total-items="listCount.length"
                max-size="maxSize"
                items-per-page="numPerPage"
                boundary-links="true"
                class="pagination-sm pull-right"
                previous-text="&lsaquo;"
                next-text="&rsaquo;"
                first-text="&laquo;"
                last-text="&raquo;"
                ></pagination>
        </div>

    </div>
</div>
</div>