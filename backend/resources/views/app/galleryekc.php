<div class="container-fluid pt-30">
    <div show-during-resolve class="alert alert-info">
        <strong>Loading....Please Wait</strong>
    </div>

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Gallery</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="pull-right">
                    <label class="switch">
                        <input ng-model="show1" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show2" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show3" type="checkbox">
                        <span class="slider"></span>
                    </label>
                </li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div ng-show="galleryedit" class="row">
                <div class="panel panel-default card-view">
                    <header class="widget-header">
                        <h5 class="widget-title">New Gallery</h5>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body">
                        <form class="form-horizontal"  ng-submit="addGallery();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-8">
                                    <input type="text" id="firstname" class="form-control" ng-model="newgallery.title" required=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-8">
                                    <input type="text" id="firstname" class="form-control" ng-model="newgallery.description"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Photos</label>
                                <div class="col-sm-8">
                                    <input  class="form-control" accept="image/*" ng-file-model="newgallery.photos" type="file" multiple/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 text-right">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button type="button" class="btn btn-default btn-xs" ng-click="cancelGallery();">Cancel</button>
                                </div>
                            </div>
                        </form>
                        <div class="lightBoxGallery" ng-show="imagesviewgallary">
                            <div class="container">
                                <div class="row">
                                    <div ng-repeat="file in newgallery.gallary_items">
                                        <div class="col-md-2">

                                            <a class="example-image-link" href="images/{{file.filename}}" data-lightbox="item-list" data-title="">
                                                <img src="images/{{file.filename}}" width="100px">
                                            </a>
                                            <button style="height: 25px;width: 25px;" type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteGalleryImage(file);">
                                                <i class="ti-trash">
                                                </i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>

        </div>
    </div>
    <div ng-hide="showSingle">
        <div class="row">
            <div class="panel panel-default card-view">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="galleryedit" ng-click="newGallery();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add Gallery</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <table id="datable_1" class="table table-hover display  pb-30" >
                            <thead>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Photos</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Photos</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr ng-repeat="gallery in listCount  = (gallerys | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{gallery.title}}</td>
                                <td>{{gallery.description}}</td>
                                <td>
                                    <img ng-repeat="gallary_item in gallery.gallary_items" style="width: 50px;height: 40px;" class="img-responsive" src="images/{{gallary_item.filename}}" alt="Image description">
                                </td>
                                <td ng-show="show1">{{gallery.created_at}}</td>
                                <td ng-show="show2">{{gallery.updated_at}}</td>
                                <td ng-show="show3">{{gallery.user.name}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editGallery(gallery);">
                                            <i class="ti-pencil"></i>
                                        </button>
                                        <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteGallery(gallery);">
                                            <i class="ti-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="gallerys.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>

        </div>
    </div>
</div>