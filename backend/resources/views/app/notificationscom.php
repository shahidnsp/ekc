<div class="container-fluid pt-30">
    <div show-during-resolve class="alert alert-info">
        <strong>Loading....Please Wait</strong>
    </div>

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Notifications</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="pull-right">
                    <label class="switch">
                        <input ng-model="show1" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show2" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show3" type="checkbox">
                        <span class="slider"></span>
                    </label>
                </li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div ng-show="notificationedit" class="row">
                <div class="panel panel-default card-view">
                    <header class="widget-header">
                        <h5 class="widget-title">New Notification</h5>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body">
                        <form class="form-horizontal"  ng-submit="addNotification();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-4">
                                    <input type="text" id="firstname" class="form-control" ng-model="newnotification.title" required=""/>
                                </div>

                                <!--<div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-4">
                                        <input  class="form-control" accept="image/*" ng-file-model="newnotification.photos" type="file" multiple/>
                                    </div>

                                </div>-->
                                <div class="form-group">
                                    <label for="" class="col-sm-1 control-label">Link</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="firstname" class="form-control" ng-model="newnotification.link"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" ng-model="newnotification.description" required=""></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Department</label>
                                <div class="col-sm-8">
                                    <select  class="form-control" ng-model="newnotification.from" required="">
                                        <option value="">Select</option>
                                        <option value="Main">Main</option>
                                        <option value="BBA">BBA</option>
                                        <option value="BCOMFinance">B.COM Finance</option>
                                        <option value="BSCCS">B.SC Computer Science</option>
                                        <option value="BAMCAJ">BA.Mass Communication and Journalism</option>
                                        <option value="BCOMCORP">B.COM Corporation</option>
                                        <option value="BCOMCA">B.COM Computer Application</option>
                                        <option value="MCOMFinance">M.COM Finance</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 text-right">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button type="button" class="btn btn-default btn-xs" ng-click="cancelNotification();">Cancel</button>
                                </div>
                            </div>

                        </form>
                        <div class="lightBoxGallery" ng-show="curnotification">
                            <a class="example-image-link" href="images/{{curNotification.photo}}" data-lightbox="item-list" data-title="">
                                <img src="images/{{curNotification.photo}}" width="100px">
                            </a>
                        </div>
                        <hr>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>

        </div>
    </div>
    <div ng-hide="showSingle">
        <div class="row">
            <div class="panel panel-default card-view">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="notificationedit" ng-click="newNotification();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add Notification</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <table id="datable_1" class="table table-hover display  pb-30" >
                            <thead>
                            <tr>
                                <th>$</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Department</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>$</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Department</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr ng-repeat="notification in listCount  = (notifications | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{notification.title}}</td>
                                <td>{{notification.description}}</td>
                                <td>
                                    <span ng-show="notification.from=='Main'">Main Website</span>
                                    <span ng-show="notification.from=='BBA'">BBA</span>
                                    <span ng-show="notification.from=='BCOMFinance'">B.COM Finance</span>
                                    <span ng-show="notification.from=='BSCCS'">B.SC Computer Science</span>
                                    <span ng-show="notification.from=='BAMCAJ'">BA.Mass Communication and Journalism</span>
                                    <span ng-show="notification.from=='BCOMCORP'">B.COM Corporation</span>
                                    <span ng-show="notification.from=='BCOMCA'">B.COM Computer Application</span>
                                    <span ng-show="notification.from=='MCOMFinance'">M.COM Finance</span>
                                </td>
                                <td ng-show="show1">{{notification.created_at}}</td>
                                <td ng-show="show2">{{notification.updated_at}}</td>
                                <td ng-show="show3">{{notification.user.name}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editNotification(notification);">
                                            <i class="ti-pencil"></i>
                                        </button>
                                        <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteNotification(notification);">
                                            <i class="ti-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="notifications.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                ></pagination>
            </div>

        </div>
    </div>
</div>