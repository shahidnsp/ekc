<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div>
<!-- Title -->
<div class="row heading-bg">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h5 class="txt-dark">User</h5>
    </div>
    <!-- Breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li class="pull-right">
                <label class="switch">
                    <input ng-model="show1" type="checkbox">
                    <span class="slider"></span>
                </label>
                <label class="switch">
                    <input ng-model="show2" type="checkbox">
                    <span class="slider"></span>
                </label>
                <label class="switch">
                    <input ng-model="show3" type="checkbox">
                    <span class="slider"></span>
                </label>
            </li>
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!-- /Title -->

<div class="row">
    <div class="col-md-12">
        <div ng-show="useredit" class="row">
            <div class="panel panel-default card-view">
                <header class="widget-header">
                    <h5 class="widget-title">New User</h5>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <form class="form-horizontal"  ng-submit="addUser();">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="newuser.name" required=""/>
                            </div>
                            <label for="" class="col-sm-1 control-label">Type</label>
                            <div class="col-sm-3">
                                <select class="form-control" ng-model="newuser.isAdmin" required="">
                                    <option value="1">Administrator</option>
                                    <option value="0" selected>User</option>
                                </select>
                            </div>
                        </div>
                        <div ng-if="newuser.isAdmin==1" class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="alert alert-warning alert-dismissable  col-sm-8">
                                <i class="zmdi zmdi-check"></i>Yay! Administrator can manage Users.....
                            </div>
                            <div class="col-sm-2"></div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" ng-model="newuser.email" required=""/>
                            </div>

                            <label for="" class="col-sm-1 control-label">Password</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newuser.pass" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Photo</label>
                            <div class="col-sm-8">
                                <input  class="form-control" accept="image/*" ng-file-model="newuser.photos" type="file" multiple/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 text-right">
                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                <button type="button" class="btn btn-default btn-xs" ng-click="cancelUser();">Cancel</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div>

    </div>
</div>
<div ng-hide="resetPermission">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="useredit" ng-click="newUser();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add User</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <div  class="tab-struct custom-tab-2 mt-40">
                            <ul role="tablist" class="nav nav-tabs" id="myTabs_15">
                                <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_15" href="#home_15" target="_blank">Working Users</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent_15">
                                <div  id="home_15" class="tab-pane fade active in" role="tabpanel">
                                    <div class="table-responsive">
                                        <table id="expenseTable" class="table table-striped table-bordered table-hover" style="border-bottom: 8px solid #448aff;">
                                            <thead>
                                            <tr class="bg-grey">
                                                <!--  <th>$</th>-->
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Photo</th>
                                                <th>Permission</th>
                                                <th ng-show="show1">Created_at</th>
                                                <th ng-show="show2">Updated_at</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="user in listCount  = (users | filter:filterlist) | orderBy:'-created_at'">
                                                <!-- <td>{{$index+1}}</td>-->
                                                <td><a>{{user.name}}</a></td>
                                                <td>{{user.email}}</td>
                                                <td>
                                                    <img style="width: 50px;height: 50px;" src="images/{{user.photo}}" alt=""/>
                                                </td>
                                                <td>
                                                    <button style="height: 25px;width: 25px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="permissionMode(user);">
                                                        <i class="ti-key"></i>
                                                    </button>
                                                </td>
                                                <td ng-show="show1">{{user.created_at}}</td>
                                                <td ng-show="show2">{{user.updated_at}}</td>
                                                <td>
                                                    <div  class="btn-group btn-group-xs" role="group">
                                                        <button style="height: 25px;width: 25px;margin-right: 5px;"  type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editUser(user);">
                                                            <i class="ti-pencil"></i>
                                                        </button>
                                                        <button style="height: 25px;width: 25px;"  type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteUser(user);">
                                                            <i class="ti-trash"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>
</div>
</div>

<div ng-show="resetPermission">
<div class="row">
<div class="col-md-12">
    <div class="row">
        <div class="row heading-bg">
            <div class="col-sm-1"></div>
            <!-- Breadcrumb -->
            <ol class="breadcrumb pull-left">
                <li><a ng-click="backToUsers($event);"><span><i class="ti-arrow-left"></i></span></a></li>
                <li class="active"><span>{{curUser.name}}</span></li>
            </ol>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->
    </div>
    <div>
        Select the pages you want add for this user from available pages and then you can add write,edit (White color is for not allowed) permission for those pages
    </div>
</div>
<div class="col-md-6">
<ul class="list-group">

<li class="list-group-item active">
    Permited pages
    <button class="btn btn-xs btn-danger pull-right" ng-click="savePermission($event)">Save</button>
</li>
<li class="list-group-item">
    <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterMypermission" />
</li>
<li class="list-group-item" style="background: #3b3e47;color: #ffff">Dashboard</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='Dashboard'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>

<li class="list-group-item" style="background: #3b3e47;color: #ffff">EKC Website</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='EKCWebsite'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>

<li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Engineering</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='Engineering'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>

<li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Architecture</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='Architecture'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>

<li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Commerce</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='Commerce'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>

<li class="list-group-item" style="background: #3b3e47;color: #ffff">Public School</li>
<li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission" ng-if="mypermision.type=='Public'">
    {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="ti-arrow-right"></i></a>
    <div class="btn-group btn-group-xs pull-right">
        <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
            write
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
            Edit
        </label>&nbsp;
        <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
            Delete
        </label>
    </div>
</li>
</ul>
</div>
<div class="col-md-6">
    <ul class="list-group">

        <li class="list-group-item active">Available pages</li>
        <li class="list-group-item">
            <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterPage" />
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">Dashboard</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='Dashboard'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">EKC Website</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='EKCWebsite'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Engineering</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='Engineering'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Architecture</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='Architecture'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">College Of Commerce</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='Commerce'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

        <li class="list-group-item" style="background: #3b3e47;color: #ffff">Public School</li>
        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage" ng-if="page.type=='Public'">
            <a href ng-click="addToList(page);"><i class="ti-arrow-left"></i></a> {{page.name}}
        </li>

    </ul>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row text-center">
            <button ng-click="backToUsers($event)" class="btn btn-primary text-center">Back</button>
            <button ng-click="savePermission($event)" class="btn btn-primary text-center">Update Permission</button>
            <br/><br/>
        </div>

    </div>

</div>
</div>