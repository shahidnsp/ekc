<div class="container-fluid pt-30">
    <div show-during-resolve class="alert alert-info">
        <strong>Loading....Please Wait</strong>
    </div>

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">News & Events</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="pull-right">
                    <label class="switch">
                        <input ng-model="show1" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show2" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show3" type="checkbox">
                        <span class="slider"></span>
                    </label>
                </li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div ng-show="newsedit" class="row">
                <div class="panel panel-default card-view">
                    <header class="widget-header">
                        <h5 class="widget-title">New News</h5>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body">
                        <form class="form-horizontal"  ng-submit="addNews();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-4">
                                    <input type="text" id="firstname" class="form-control" ng-model="newnews.title" required=""/>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-4">
                                        <input  class="form-control" accept="image/*" ng-file-model="newnews.photos" type="file" multiple/>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <!-- <textarea class="form-control" ng-model="newnews.description" required=""></textarea>-->
                                    <div ckeditor ng-model="newnews.description"></div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 text-right">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button type="button" class="btn btn-default btn-xs" ng-click="cancelNews();">Cancel</button>
                                </div>
                            </div>
                        </form>
                        <div class="lightBoxGallery" ng-show="imageViewNews">
                            <a class="example-image-link" href="images/{{curNews.photo}}" data-lightbox="item-list" data-title="">
                                <img src="images/{{curNews.photo}}" width="100px">
                            </a>
                        </div>
                        <hr>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>

        </div>
    </div>
    <div ng-hide="showSingle">
        <div class="row">
            <div class="panel panel-default card-view">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="newsedit" ng-click="newNews();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add News</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <table id="datable_1" class="table table-hover display  pb-30" >
                            <thead>
                            <tr>
                                <th>$</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>$</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr ng-repeat="news in listCount  = (newses | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{news.title}}</td>
                                <td>{{news.description}}</td>
                                <td>
                                    <img style="width: 50px;height: 40px;" class="img-responsive" src="images/{{news.photo}}" alt="Image description">
                                </td>
                                <td ng-show="show1">{{news.created_at}}</td>
                                <td ng-show="show2">{{news.updated_at}}</td>
                                <td ng-show="show3">{{news.user.name}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editNews(news);">
                                            <i class="ti-pencil"></i>
                                        </button>
                                        <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteNews(news);">
                                            <i class="ti-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="newses.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                ></pagination>
            </div>

        </div>
    </div>
</div>