<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>EKC-Administration</title>
	<meta name="description" content="Winkle is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Winkle Admin, Winkleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">




	<!-- Morris Charts CSS -->
   {{-- <link href="../vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

	<!-- bootstrap-select CSS -->
	<link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>


	<!-- vector map CSS -->
	<link href="../vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>
--}}
	<!-- Custom CSS -->
	<link href="css/app.css" rel="stylesheet" type="text/css">
	<link href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css" rel="stylesheet" type="text/css">

</head>

<body ng-controller="HomeController">
    <http-request-loader></http-request-loader>
	<!-- Preloader -->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!-- /Preloader -->
    <div class="wrapper theme-1-active navbar-top-light">
		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="#">
							<img class="brand-img" src="../img/logo.png" alt="brand"/>
							<span class="brand-text">EKC</span>
						</a>
					</div>
				</div>
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="ti-align-left"></i></a>
				<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="ti-search"></i></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="ti-more"></i></a>
				<form id="search_form" role="search" class="top-nav-search collapse pull-left">
					<div class="input-group">
						<input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="ti-search"></i></button>
						</span>
					</div>
				</form>
			</div>
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
					<li>
						<a id="open_right_sidebar" href="#" target="_blank"><i class="ti-settings  top-nav-icon"></i></a>
					</li>

					<li class="dropdown alert-drp">
						<a href="#" target="_blank" class="dropdown-toggle" data-toggle="dropdown"><i class="ti-bell top-nav-icon"></i><span class="top-nav-icon-badge">0</span></a>
						<ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
							<li>
								<div class="notification-box-head-wrap">
									<span class="notification-box-head pull-left inline-block">notifications</span>
									<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)" target="_blank"> clear all </a>
									<div class="clearfix"></div>
									<hr class="light-grey-hr ma-0"/>
								</div>
							</li>
							<li ng-repeat="notice in notices">
                               <span>@{{notice.message}} <span class="text-danger">(@{{notice.order.client.firstname}} @{{notice.order.client.lastname}} - @{{notice.order.orderno}})</span></span>
							</li>
							<li>
								<div class="notification-box-bottom-wrap">
									<hr class="light-grey-hr ma-0"/>
									<a class="block text-center read-all" href="javascript:void(0)"> read all </a>
									<div class="clearfix"></div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown auth-drp">
						<a class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="images/1.jpg" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
							<li>
								<a href="#profile"><i class="zmdi zmdi-account"></i><span>Profile</span></a>
							</li>
							<li class="divider"></li>
							<li class="sub-menu show-on-hover">
								<a href="#" class="dropdown-toggle pr-0 level-2-drp"><i class="zmdi zmdi-check text-success"></i> available</a>
								<ul class="dropdown-menu open-left-side">
									<li>
										<a href="#" target="_blank"><i class="zmdi zmdi-check text-success"></i><span>available</span></a>
									</li>
									<li>
										<a href="#" target="_blank"><i class="zmdi zmdi-circle-o text-warning"></i><span>busy</span></a>
									</li>
									<li>
										<a href="#" target="_blank"><i class="zmdi zmdi-minus-circle-outline text-danger"></i><span>offline</span></a>
									</li>
								</ul>
							</li>
							<li class="divider"></li>
							<li>
							    <a onclick="document.getElementById('myform').submit();"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
							    <form id="myform" method="POST" action="/logout">@csrf</form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<!-- /Top Menu Items -->

		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span>
					<hr/>
				</li>
				<li>
					<a href="#dashboard"><div class="pull-left"><i class="ti-image mr-20"></i> <span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
				</li>

				<li ng-show="showEKC">
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-map mr-20"></i><span class="right-nav-text">EKC Website</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
					{{--<ul id="ecom_dr" class="collapse collapse-level-1">
						<li ng-repeat="menu in user.menu" ng-if="menu.type=='Basic'" ng-class="menuClass('@{{menu.route}}')">
							<a href="#@{{menu.route}}">@{{menu.title}}</a>
						</li>
					</ul>--}}
					<ul id="ecom_dr" class="collapse collapse-level-1">
						{{--<li ng-class="menuClass('teamsekc')">
							<a href="#teamsekc">Teams</a>
						</li>
						<li ng-class="menuClass('testimonialsekc')">
                            <a href="#testimonialsekc">Testimonials</a>
                        </li>
                        <li ng-class="menuClass('slidersekc')">
                            <a href="#slidersekc">Sliders</a>
                        </li>
                        <li ng-class="menuClass('downloadsekc')">
                            <a href="#downloadsekc">Downloads</a>
                        </li>
                        <li ng-class="menuClass('newsekc')">
                            <a href="#newsekc">News & Events</a>
                        </li>
                        <li ng-class="menuClass('galleryekc')">
                            <a href="#galleryekc">Gallery</a>
                        </li>
                        <li ng-class="menuClass('brochuresekc')">
                            <a href="#brochuresekc">Brochures</a>
                        </li>
                        <li ng-class="menuClass('notificationsekc')">
                            <a href="#notificationsekc">Notifications</a>
                        </li>--}}

                        <li ng-repeat="menu in user.menu" ng-if="menu.type=='EKCWebsite'" ng-class="menuClass('@{{menu.route}}')">
                            <a href="#@{{menu.route}}"><div class="pull-left"><i class="mr-20"></i><span class="right-nav-text">@{{menu.title}}</span></div><div class="clearfix"></div></a>
                        </li>
					</ul>
				</li>


                <li ng-show="showENG">
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_eng"><div class="pull-left"><i class="ti-envelope mr-20"></i><span class="right-nav-text">College Of Engineering</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>

                    <ul id="ecom_eng" class="collapse collapse-level-1">
                        <li ng-repeat="menu in user.menu" ng-if="menu.type=='Engineering'" ng-class="menuClass('@{{menu.route}}')">
                            <a href="#@{{menu.route}}">@{{menu.title}}</a>
                        </li>
                    </ul>
                </li>


                <li ng-show="showARC">
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_arch"><div class="pull-left"><i class="ti-shopping-cart  mr-20"></i><span class="right-nav-text">College Of Architecture</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>

                    <ul id="ecom_arch" class="collapse collapse-level-1">
                        <li ng-repeat="menu in user.menu" ng-if="menu.type=='Architecture'" ng-class="menuClass('@{{menu.route}}')">
                            <a href="#@{{menu.route}}">@{{menu.title}}</a>
                        </li>
                    </ul>
                </li>

                <li ng-show="showCOM">
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_comm"><div class="pull-left"><i class="ti-clipboard mr-20"></i><span class="right-nav-text">College Of Commerce</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>

                    <ul id="ecom_comm" class="collapse collapse-level-1">
                        <li ng-repeat="menu in user.menu" ng-if="menu.type=='Commerce'" ng-class="menuClass('@{{menu.route}}')">
                            <a href="#@{{menu.route}}">@{{menu.title}}</a>
                        </li>
                    </ul>
                </li>


                <li ng-show="showPUB">
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_public"><div class="pull-left"><i class="ti-basketball mr-20"></i><span class="right-nav-text">Public School</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>

                    <ul id="ecom_public" class="collapse collapse-level-1">
                        <li ng-repeat="menu in user.menu" ng-if="menu.type=='Public'" ng-class="menuClass('@{{menu.route}}')">
                            <a href="#@{{menu.route}}">@{{menu.title}}</a>
                        </li>
                    </ul>
                </li>

                <li ng-if="user.isAdmin==1">
                    <a href="#user"><div class="pull-left"><i class="ti-user mr-20"></i> <span class="right-nav-text">Users</span></div><div class="clearfix"></div></a>
                </li>

			</ul>
		</div>
		<!-- /Left Sidebar Menu -->


          <!-- Main Content -->
        		<div class="page-wrapper">
                    <div ng-view></div>
        		</div>

             <!-- Main Content -->


 </div>
    <!-- /#wrapper -->

    <!-- JavaScript -->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Data table JavaScript -->
    <script src="js/jquery.dataTables.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>

    <!-- Progressbar Animation JavaScript -->
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="js/dropdown-bootstrap-extended.js"></script>

    <!-- Sparkline JavaScript -->
    <script src="js/jquery.sparkline.min.js"></script>

    <!-- Owl JavaScript -->
    <script src="js/owl.carousel.min.js"></script>

    <!-- Switchery JavaScript -->
    <script src="js/switchery.min.js"></script>

    <!-- EChartJS JavaScript -->
  {{--  <script src="../vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
    <script src="../vendors/echarts-liquidfill.min.js"></script>--}}

    <!-- Vector Maps JavaScript -->
 {{--   <script src="../vendors/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="../vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="dist/js/vectormap-data.js"></script>--}}

    <!-- Toast JavaScript -->
    <script src="js/jquery.toast.min.js"></script>

    <!-- Piety JavaScript -->
    <script src="js/jquery.peity.min.js"></script>
    <script src="js/peity-data.js"></script>

    <!-- Morris Charts JavaScript -->
  {{--  <script src="../vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="../vendors/bower_components/morris.js/morris.min.js"></script>
    <script src="../vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>--}}

    <!-- Bootstrap Select JavaScript -->
    <script src="js/bootstrap-select.min.js"></script>

    <!-- Flot Charts JavaScript -->
 {{--   <script src="../vendors/bower_components/Flot/excanvas.min.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.resize.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/bower_components/Flot/jquery.flot.crosshair.js"></script>
    <script src="../vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="dist/js/flot-data.js"></script>--}}
<script src="ckeditor/ckeditor.js"></script>
    <!-- Init JavaScript -->
    <script src="js/init.js"></script>
    <script src="js/dashboard2-data.js"></script>
    <script src="js/angular.js"></script>
    <script src="js/angularscript.js"></script>


    <script>
        function validate(ev) {
            if (!ev) {
                ev = window.event;
            }

            if (!ev.ctrlKey && ev.key.length === 1 && (isNaN(+ev.key) || ev.key === " ")) {
                return ev.preventDefault();
            }
        }
    </script>
</body>

</html>
