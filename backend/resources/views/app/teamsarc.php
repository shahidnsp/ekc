<div class="container-fluid pt-30">
    <div show-during-resolve class="alert alert-info">
        <strong>Loading....Please Wait</strong>
    </div>

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Team</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="pull-right">
                    <label class="switch">
                        <input ng-model="show1" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show2" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show3" type="checkbox">
                        <span class="slider"></span>
                    </label>
                </li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div ng-show="teamedit" class="row">
                <div class="panel panel-default card-view">
                    <header class="widget-header">
                        <h5 class="widget-title">New Team</h5>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body">
                        <form class="form-horizontal"  ng-submit="addTeam();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" id="firstname" class="form-control" ng-model="newteam.name" required=""/>
                                </div>

                                <label for="" class="col-sm-1 control-label">Designation</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" ng-model="newteam.designation"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-8">
                                    <input type="text" id="firstname" class="form-control" ng-model="newteam.phone"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Photo(200x250)</label>
                                <div class="col-sm-8">
                                    <input  class="form-control" accept="image/*" ng-file-model="newteam.photos" type="file" multiple/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Facebook</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" ng-model="newteam.facebook" />
                                </div>

                                <label for="" class="col-sm-1 control-label">LinkedIn</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" ng-model="newteam.linkedin"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Google+</label>
                                <div class="col-sm-4">
                                    <input type="text"  class="form-control" ng-model="newteam.gplus" />
                                </div>

                                <label for="" class="col-sm-1 control-label">Twitter</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" ng-model="newteam.twitter"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" ng-model="newteam.description" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 text-right">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button type="button" class="btn btn-default btn-xs" ng-click="cancelTeam();">Cancel</button>
                                </div>
                            </div>
                        </form>
                        <div class="lightBoxGallery" ng-show="imageViewTeam">
                            <a class="example-image-link" href="images/{{curTeam.photo}}" data-lightbox="item-list" data-title="">
                                <img src="images/{{curTeam.photo}}" width="100px">
                            </a>
                        </div>
                        <hr>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>
        </div>
    </div>
    <div ng-hide="showSingle">
        <div class="row">
            <div class="panel panel-default card-view">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="teamedit" ng-click="newTeam();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add Team</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <table id="datable_1" class="table table-hover display  pb-30" >
                            <thead>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Facebook</th>
                                <th>LinkedIn</th>
                                <th>Google+</th>
                                <th>Twitter</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th>Order</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Facebook</th>
                                <th>LinkedIn</th>
                                <th>Google+</th>
                                <th>Twitter</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th>Order</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr ng-repeat="team in listCount  = (teams | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{team.name}}</td>
                                <td>{{team.designation}}</td>
                                <td>{{team.facebook}}</td>
                                <td>{{team.linkedin}}</td>
                                <td>{{team.gplus}}</td>
                                <td>{{team.twitter}}</td>
                                <td>{{team.description}}</td>
                                <td>
                                    <img style="width: 50px;height: 40px;" class="img-responsive" src="images/{{team.photo}}" alt="Image description">
                                </td>
                                <td>
                                    <input type="text" ng-model="team.order" class="form-control" ng-change="changeOrder(team);"/>
                                </td>
                                <td ng-show="show1">{{team.created_at}}</td>
                                <td ng-show="show2">{{team.updated_at}}</td>
                                <td ng-show="show3">{{team.user.name}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editTeam(team);">
                                            <i class="ti-pencil"></i>
                                        </button>
                                        <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteTeam(team);">
                                            <i class="ti-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="teams.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                ></pagination>
            </div>

        </div>
    </div>
</div>