<div class="container-fluid pt-30">
    <div show-during-resolve class="alert alert-info">
        <strong>Loading....Please Wait</strong>
    </div>

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Download</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="pull-right">
                    <label class="switch">
                        <input ng-model="show1" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show2" type="checkbox">
                        <span class="slider"></span>
                    </label>
                    <label class="switch">
                        <input ng-model="show3" type="checkbox">
                        <span class="slider"></span>
                    </label>
                </li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div ng-show="downloadedit" class="row">
                <div class="panel panel-default card-view">
                    <header class="widget-header">
                        <h5 class="widget-title">New Download</h5>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body">
                        <form class="form-horizontal"  ng-submit="addDownload();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" id="firstname" class="form-control" ng-model="newdownload.name" required=""/>
                                </div>

                                <label for="" class="col-sm-1 control-label">Type</label>
                                <div class="col-sm-3">
                                    <select class="form-control" ng-model="newdownload.type">
                                        <option value="Photo">Photo</option>
                                        <option value="Link">Link</option>
                                        <option value="PDF">PDF</option>
                                        <option value="Doc">Doc(docx)</option>
                                        <option value="Excel">Excel(xlx)</option>
                                    </select>
                                </div>
                            </div>
                            <div ng-if="newdownload.type=='Photo'" class="form-group">
                                <label for="" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-4">
                                    <input  class="form-control" accept="image/*" ng-file-model="newdownload.photos" type="file" multiple/>
                                </div>
                                <label for="" class="col-sm-1 control-label">Department</label>
                                <div class="col-sm-3">
                                    <select  class="form-control" ng-model="newdownload.from" required="">
                                        <option value="">Select</option>
                                        <option value="Main">Main</option>
                                        <option value="Civil">Civil Engineering</option>
                                        <option value="Mechanical">Mechanical Engineering</option>
                                        <option value="Electronics">Electronics & Communication Engineering</option>
                                        <option value="Computer">Computer Science and Engineering</option>
                                        <option value="Safety">Safety & Fire Engineering</option>
                                        <option value="Humanities">Science & Humanities</option>
                                    </select>
                                </div>
                            </div>
                            <div ng-if="newdownload.type=='PDF'" class="form-group">
                                <label for="" class="col-sm-2 control-label">PDF</label>
                                <div class="col-sm-4">
                                    <input  class="form-control" accept="application/pdf" ng-file-model="newdownload.photos" type="file" multiple/>
                                </div>
                                <label for="" class="col-sm-1 control-label">Department</label>
                                <div class="col-sm-3">
                                    <select  class="form-control" ng-model="newdownload.from" required="">
                                        <option value="">Select</option>
                                        <option value="Main">Main</option>
                                        <option value="Civil">Civil Engineering</option>
                                        <option value="Mechanical">Mechanical Engineering</option>
                                        <option value="Electronics">Electronics & Communication Engineering</option>
                                        <option value="Computer">Computer Science and Engineering</option>
                                        <option value="Safety">Safety & Fire Engineering</option>
                                        <option value="Humanities">Science & Humanities</option>
                                    </select>
                                </div>
                            </div>
                            <div ng-if="newdownload.type=='Link'" class="form-group">
                                <label for="" class="col-sm-2 control-label">Link</label>
                                <div class="col-sm-4">
                                    <input ng-model="newdownload.link"  class="form-control"  type="text"/>
                                </div>
                                <label for="" class="col-sm-1 control-label">Department</label>
                                <div class="col-sm-3">
                                    <select  class="form-control" ng-model="newdownload.from" required="">
                                        <option value="">Select</option>
                                        <option value="Main">Main</option>
                                        <option value="Civil">Civil Engineering</option>
                                        <option value="Mechanical">Mechanical Engineering</option>
                                        <option value="Electronics">Electronics & Communication Engineering</option>
                                        <option value="Computer">Computer Science and Engineering</option>
                                        <option value="Safety">Safety & Fire Engineering</option>
                                        <option value="Humanities">Science & Humanities</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 text-right">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-floppy-o"></i> Save</button>
                                    <button type="button" class="btn btn-default btn-xs" ng-click="cancelDownload();">Cancel</button>
                                </div>
                            </div>
                            <hr>
                        </form>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>

        </div>
    </div>
    <div ng-hide="showSingle">
        <div class="row">
            <div class="panel panel-default card-view">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap col-md-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_length" >
                                        <label>
                                            Show
                                            <select name="default-datatable_length" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                            entries
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button  ng-hide="downloadedit" ng-click="newDownload();" class="btn btn-primary btn-xs" type="button"><i class="fa fa-plus"></i> Add Download</button>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dataTables_filter pull-right">
                                        <label>
                                            Search:
                                            <input type="text" class="form-control input-sm" id="filter-list" placeholder="Search" ng-model="filterlist">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <table id="datable_1" class="table table-hover display  pb-30" >
                            <thead>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>File</th>
                                <th>Department</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>$</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>File</th>
                                <th>Department</th>
                                <th ng-show="show1">Created_at</th>
                                <th ng-show="show2">Updated_at</th>
                                <th ng-show="show3">User</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr ng-repeat="download in listCount  = (downloads | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{download.name}}</td>
                                <td>{{download.type}}</td>
                                <td>
                                    <a href="images/{{download.file}}" target="_blank"><img ng-if="download.type=='Photo'" style="width: 50px;height: 40px;" class="img-responsive" src="images/{{download.file}}" alt="Image description"></a>
                                    <span ng-if="download.type=='Link'"><a href="{{download.link}}" target="_blank">{{download.link}}</a></span>
                                    <span ng-if="download.type=='PDF'" ><a href="pdf/{{download.file}}" target="_blank">{{download.file}}</a></span>
                                </td>
                                <td>
                                    <span ng-show="download.from=='Main'">Main Website</span>
                                    <span ng-show="download.from=='Civil'">Civil Engineering</span>
                                    <span ng-show="download.from=='Mechanical'">Mechanical Engineering</span>
                                    <span ng-show="download.from=='Electronics'">Electronics & Communication Engineering</span>
                                    <span ng-show="download.from=='Computer'">Computer Science and Engineering</span>
                                    <span ng-show="download.from=='Safety'">Safety & Fire Engineering</span>
                                    <span ng-show="download.from=='Humanities'">Science & Humanities</span>
                                </td>
                                <td ng-show="show1">{{download.created_at}}</td>
                                <td ng-show="show2">{{download.updated_at}}</td>
                                <td ng-show="show3">{{download.user.name}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button style="height: 25px;width: 25px;margin-right: 5px;" type="button" class="btn btn-primary btn-icon-anim btn-circle" ng-click="editDownload(download);">
                                            <i class="ti-pencil"></i>
                                        </button>
                                        <button style="height: 25px;width: 25px;"   type="button" class="btn btn-icon-anim btn-circle btn-danger" ng-click="deleteDownload(download);">
                                            <i class="ti-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="downloads.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                ></pagination>
            </div>

        </div>
    </div>
</div>