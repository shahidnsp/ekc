var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'cp.ngConfirm',
        'angular.http.request.loader',
        'googlechart',
        '720kb.datepicker',
        'ngTagsInput',
        'vd.directive.advanced_select',
        'ui-notification',
        'ckeditor',
        'UserInfoService',
        'BrochureService',
        'TeamService',
        'TestimonialService',
        'SliderService',
        'DownloadService',
        'NewsService',
        'NotificationService',
        'GalleryService',
        'BrochureService',
        'UserService',
        'ContactService',
        'VideoService',
        'PopupService',
        'AdmissionService'
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/user', {
			templateUrl: 'template/user',
			controller: 'UserController'
		})
        .when('/teamsekc', {
            templateUrl: 'template/teamsekc',
            controller: 'TeamsEKCController'
        })
        .when('/testimonialsekc', {
            templateUrl: 'template/testimonialsekc',
            controller: 'TestimonialEKCController'
        })
        .when('/slidersekc', {
            templateUrl: 'template/slidersekc',
            controller: 'SliderEKCController'
        })
        .when('/downloadsekc', {
            templateUrl: 'template/downloadsekc',
            controller: 'DownloadEKCController'
        })
        .when('/newsekc', {
            templateUrl: 'template/newsekc',
            controller: 'NewsEKCController'
        })
        .when('/notificationsekc', {
            templateUrl: 'template/notificationsekc',
            controller: 'NotificationEKCController'
        })
        .when('/galleryekc', {
            templateUrl: 'template/galleryekc',
            controller: 'GalleryEKCController'
        } )
        .when('/brochuresekc', {
            templateUrl: 'template/brochuresekc',
            controller: 'BrochureEKCController'
        } )
        .when('/contactsekc', {
            templateUrl: 'template/contactsekc',
            controller: 'ContactEKCController'
        } )
        .when('/videosekc', {
            templateUrl: 'template/videosekc',
            controller: 'VideoEKCController'
        } )
        .when('/popupekc', {
            templateUrl: 'template/popupekc',
            controller: 'PopupEKCController'
        } )

 //Engineering

        .when('/testimonialseng', {
            templateUrl: 'template/testimonialseng',
            controller: 'TestimonialEngController'
        })
        .when('/sliderseng', {
            templateUrl: 'template/sliderseng',
            controller: 'SliderEngController'
        })
        .when('/downloadseng', {
            templateUrl: 'template/downloadseng',
            controller: 'DownloadEngController'
        })
        .when('/newseng', {
            templateUrl: 'template/newseng',
            controller: 'NewsEngController'
        })
        .when('/notificationseng', {
            templateUrl: 'template/notificationseng',
            controller: 'NotificationEngController'
        })
        .when('/galleryeng', {
            templateUrl: 'template/gallaryeng',
            controller: 'GalleryEngController'
        } )
        .when('/brochureseng', {
            templateUrl: 'template/brochureseng',
            controller: 'BrochureEngController'
        } )
        .when('/teamseng', {
            templateUrl: 'template/teamseng',
            controller: 'TeamsEngController'
        } )
        .when('/contactseng', {
            templateUrl: 'template/contactseng',
            controller: 'ContactEngController'
        } )
        .when('/videoseng', {
            templateUrl: 'template/videoseng',
            controller: 'VideoEngController'
        } )
        .when('/popupeng', {
            templateUrl: 'template/popueng',
            controller: 'PopupEngController'
        } )
        .when('/admissioneng', {
            templateUrl: 'template/admissioneng',
            controller: 'AdmissionEngController'
        })

//Architecture
        .when('/brochuresarc', {
            templateUrl: 'template/brochuresarc',
            controller: 'BrochureArcController'
        } )
        .when('/testimonialsarc', {
            templateUrl: 'template/testimonialsarc',
            controller: 'TestimonialArcController'
            })
        .when('/slidersarc', {
            templateUrl: 'template/slidersarc',
            controller: 'SliderArcController'
        })
        .when('/downloadsarc', {
            templateUrl: 'template/downloadsarc',
            controller: 'DownloadArcController'
        })
        .when('/newsarc', {
            templateUrl: 'template/newsarc',
            controller: 'NewsArcController'
        })
        .when('/notificationsarc', {
            templateUrl: 'template/notificationsarc',
            controller: 'NotificationArcController'
        })
        .when('/galleryarc', {
            templateUrl: 'template/gallaryarc',
            controller: 'GalleryArcController'
        } )
        .when('/brochuresarc', {
            templateUrl: 'template/brochuresarc',
            controller: 'BrochureArcController'
        } )
        .when('/teamsarc', {
            templateUrl: 'template/teamsarc',
            controller: 'TeamsArcController'
            } )
        .when('/contactsarc', {
            templateUrl: 'template/contactsarc',
            controller: 'ContactArcController'
        } )
        .when('/videosarc', {
            templateUrl: 'template/videosarc',
            controller: 'VideoArcController'
        } )
        .when('/popuparc', {
            templateUrl: 'template/popuparc',
            controller: 'PopupArcController'
        } )
        .when('/admissionarc', {
            templateUrl: 'template/admissionarc',
            controller: 'AdmissionArcController'
        })

//Public
        .when('/brochurespub', {
            templateUrl: 'template/brochurespub',
            controller: 'BrochurePubController'
        } )
        .when('/testimonialspub', {
            templateUrl: 'template/testimonialspub',
            controller: 'TestimonialPubController'
        })
        .when('/sliderspub', {
            templateUrl: 'template/sliderspub',
            controller: 'SliderPubController'
        })
        .when('/downloadspub', {
            templateUrl: 'template/downloadspub',
            controller: 'DownloadPubController'
        })
        .when('/newspub', {
            templateUrl: 'template/newspub',
            controller: 'NewsPubController'
        })
        .when('/notificationspub', {
            templateUrl: 'template/notificationspub',
            controller: 'NotificationPubController'
        })
        .when('/gallerypub', {
            templateUrl: 'template/gallarypub',
            controller: 'GalleryPubController'
        } )
        .when('/brochurespub', {
            templateUrl: 'template/brochurespub',
            controller: 'BrochurePubController'
        } )
       .when('/teamspub', {
            templateUrl: 'template/teamspub',
            controller: 'TeamsPubController'
       } )
        .when('/contactspub', {
            templateUrl: 'template/contactspub',
            controller: 'ContactPubController'
        } )
        .when('/videospub', {
            templateUrl: 'template/videospub',
            controller: 'VideoPubController'
        } )
        .when('/popuppub', {
            templateUrl: 'template/popuppub',
            controller: 'PopupPubController'
        } )
        .when('/admissionpub', {
            templateUrl: 'template/admissionpub',
            controller: 'AdmissionPubController'
        })

//commerce
       .when('/brochurescom', {
            templateUrl: 'template/brochurescom',
            controller: 'BrochureComController'
        } )
        .when('/testimonialscom', {
            templateUrl: 'template/testimonialscom',
            controller: 'TestimonialComController'
        })
        .when('/sliderscom', {
            templateUrl: 'template/sliderscom',
            controller: 'SliderComController'
        })
        .when('/downloadscom', {
            templateUrl: 'template/downloadscom',
            controller: 'DownloadComController'
        })
        .when('/newscom', {
            templateUrl: 'template/newscom',
            controller: 'NewsComController'
        })
        .when('/notificationscom', {
             templateUrl: 'template/notificationscom',
             controller: 'NotificationComController'
        })
        .when('/gallerycom', {
             templateUrl: 'template/gallarycom',
             controller: 'GalleryComController'
        } )
        .when('/brochurescom', {
             templateUrl: 'template/brochurescom',
             controller: 'BrochureComController'
        } )
       .when('/teamscom', {
             templateUrl: 'template/teamscom',
             controller: 'TeamsComController'
       } )
        .when('/contactscom', {
            templateUrl: 'template/contactscom',
            controller: 'ContactComController'
        } )
        .when('/videoscom', {
            templateUrl: 'template/vdeoscom',
            controller: 'VideoComController'
        } )
        .when('/popupcom', {
            templateUrl: 'template/popupcom',
            controller: 'PopupComController'
        } )
        .when('/admissioncom', {
            templateUrl: 'template/admissioncom',
            controller: 'AdmissionComController'
        } )
       .otherwise({
            redirectTo: 'template/dashboard'
        });
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();

                        reader.onload = (function (i) {
                            return function(e) {
                                var value = {
                                    lastModified: changeEvent.target.files[i].lastModified,
                                    lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                    name: changeEvent.target.files[i].name,
                                    size: changeEvent.target.files[i].size,
                                    type: changeEvent.target.files[i].type,
                                    data: e.target.result
                                };
                                values.push(value);
                            }

                        })(i);

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }


                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });

    app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });


    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        }
    });





