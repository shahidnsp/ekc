/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/apiweb/contacts/:contactsId', {
            contactsId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);