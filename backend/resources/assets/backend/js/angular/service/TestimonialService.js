/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('TestimonialService',[]).factory('Testimonial',['$resource',
    function($resource){
        return $resource('/apiweb/testimonials/:testimonialsId', {
            testimonialsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);