/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/apiweb/user/:userId', {
            userId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);