/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('NewsService',[]).factory('News',['$resource',
    function($resource){
        return $resource('/apiweb/news/:newsId', {
            newsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);