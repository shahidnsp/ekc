/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('DownloadService',[]).factory('Download',['$resource',
    function($resource){
        return $resource('/apiweb/download/:downloadId', {
            downloadId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);