/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('AdmissionService',[]).factory('Admission',['$resource',
    function($resource){
        return $resource('/apiweb/admission/:admissionId', {
            admissionId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);