/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('TeamService',[]).factory('Team',['$resource',
    function($resource){
        return $resource('/apiweb/teams/:teamsId', {
            teamsId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);