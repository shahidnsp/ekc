angular.module('NotificationService',[]).factory('Notification',['$resource',
    function($resource){
        return $resource('/apiweb/notification/:notificationId', {
           notificationId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);