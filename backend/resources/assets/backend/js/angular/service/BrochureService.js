/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('BrochureService',[]).factory('Brochure',['$resource',
    function($resource){
        return $resource('/apiweb/brochure/:brochureId', {
            brochureId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);