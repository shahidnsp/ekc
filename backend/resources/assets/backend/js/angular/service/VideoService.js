/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('VideoService',[]).factory('Video',['$resource',
    function($resource){
        return $resource('/apiweb/videos/:videosId', {
            videosId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);