/**
 * Created by Shahid Neermunda on 11/05/2017.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
    var UserInfo = {};

    UserInfo.query=function(){
        return $http.get('/apiweb/userinfo')
    };

    UserInfo.get = function(id){
        return $http.get('/apiweb/user/'+id);
    };

    UserInfo.save = function(id,data){
        return $http.put('/apiweb/userinfo/'+id,data);
    };

    UserInfo.getAllPage=function(id){
        return $http.get('/apiweb/getAllPages/'+id);
    };

    UserInfo.getUserPermission=function(userId){
        return $http.get('/apiweb/getPermission/?id='+userId);
    };

    UserInfo.setUserPermission = function(data){
        return $http.post('/apiweb/changePermission',data);
    };

    return UserInfo;
}
]);
