angular.module('PopupService',[]).factory('Popup',['$resource',
    function($resource){
        return $resource('/apiweb/popup/:popupId', {
            popupId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);