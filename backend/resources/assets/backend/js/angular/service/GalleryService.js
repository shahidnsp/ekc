/**
 * Created by Shahid Neermunda on 08/02/19.
 */

angular.module('GalleryService',[]).factory('Gallery',['$resource',
    function($resource){
        return $resource('/apiweb/gallery/:galleryId', {
            galleryId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);