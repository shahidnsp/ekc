/**
 * Created by Shahid Neermunda on 07/02/19.
 */

angular.module('SliderService',[]).factory('Slider',['$resource',
    function($resource){
        return $resource('/apiweb/slider/:sliderId', {
            sliderId: '@id'
        }, {
            update: {
                method: 'POST'
            }
        });
    }
]);