app.controller('BrochureEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Brochure){

    $scope.brochures = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadBrochure() {
        $scope.brochures = [];
        Brochure.query({department:'Engineering'},function (brochure) {
            $scope.brochures = brochure;
        });
    }

    loadBrochure();

    $scope.otificationedit = false;

    $scope.newBrochure = function () {
        $scope.brochureedit = true;
        $scope.newbrochure = new Brochure();
        $scope.newbrochure.department='Engineering';
        $scope.newbrochure.from='Main';
        $scope.curBrochure = {};
    };
    $scope.editBrochure = function (thisBrochure) {
        $scope.brochureedit = true;
        $scope.curBrochure = thisBrochure;
        $scope.newbrochure = angular.copy(thisBrochure);
        $anchorScroll();
    };

    $scope.cancelBrochure = function () {
        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
    };

    $scope.addBrochure = function () {
        if ($scope.curBrochure.id) {
            $scope.newbrochure.$update(function (flash) {
                angular.extend($scope.curBrochure, $scope.curBrochure, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brochure Updated Successfully');
            });

        } else {
            $scope.newbrochure.$save(function (brochures) {
                $scope.brochures.push(brochures);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Brochure Saved Successfully');
            });
        }

        $scope.brochureedit = false;
        $scope.newbrochure = new Brochure();
        angular.element("input[type='file']").val(null);
        //loadBrochure();
    };

    $scope.deleteBrochure = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        item.$delete(function () {
                            var curIndex = $scope.brochures.indexOf(item);
                            $scope.brochures.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Brochure Removed Successfully'
                            });
                        });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});