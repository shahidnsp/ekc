app.controller('NewsEngController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, News){

    $scope.newses = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNews() {
        $scope.newses = [];
        News.query({department:'Engineering'},function (news) {
            $scope.newses = news;
        });
    }

    loadNews();


    $scope.newsedit = false;
    $scope.imageViewNews=false;

    $scope.newNews = function () {
        $scope.newsedit = true;
        $scope.newnews = new News();
        $scope.newnews.department='Engineering';
        $scope.newnews.from='Main';
        $scope.curNews = {};
    };
    $scope.editNews = function (thisNews) {
        $scope.newsedit = true;
        $scope.imageViewNews=true;
        $scope.curNews = thisNews;
        $scope.newnews = angular.copy(thisNews);
        $anchorScroll();
    };

    $scope.cancelNews = function () {
        $scope.newsedit = false;
        $scope.newnews = new News();
    };

    $scope.addNews = function () {
        if ($scope.curNews.id) {
            $scope.newnews.$update(function (flash) {
                angular.extend($scope.curNews, $scope.curNews, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Updated Successfully');
            });

        } else {
            $scope.newnews.$save(function (newss) {
                $scope.newses.push(newss);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Saved Successfully');
            });
        }

        $scope.newsedit = false;
        $scope.newnews = new News();
        angular.element("input[type='file']").val(null);
        //loadNews();
    };

    $scope.deleteNews = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.newses.indexOf(item);
                            $scope.newses.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'News Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/news/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.newses.indexOf(item);
                                $scope.newses.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'News Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});
