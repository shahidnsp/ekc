app.controller('UserController', function($scope,$http,$filter,$anchorScroll, $ngConfirm, ngNotify, User,UserInfo){
    $scope.users = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadUser() {
        $scope.users = [];
        User.query(function (users) {
            $scope.users = users;
        });
    }

    loadUser();

    $scope.useredit = false;

    $scope.newUser = function () {
        $scope.useredit = true;
        $scope.newuser = new User();
        $scope.newuser.isAdmin=0;
        $scope.newuser.enrollment_date=formatDate(new Date());
        $scope.curUser = {};
    };

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }

    $scope.editUser = function (thisFlash) {
        $scope.useredit = true;
        $scope.curUser = thisFlash;
        $scope.newuser = angular.copy(thisFlash);
        $scope.newuser.enrollment_date=formatDate(thisFlash.enrollment_date);
        if($scope.newuser.isleaved==0) {
            $scope.newuser.leave_date = formatDate(new Date());
            $scope.newuser.isleaved=false;
        }
        else {
            $scope.newuser.leave_date = formatDate($scope.newuser.leave_date);
            $scope.newuser.isleaved=true;
        }
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelUser = function () {
        $scope.useredit = false;
        $scope.newuser = new User();
    };

    $scope.addUser = function () {
        if ($scope.curUser.id) {
            $scope.newuser.$update(function (flash) {
                angular.extend($scope.curUser, $scope.curUser, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Updated Successfully');
            });

        } else {
            $scope.newuser.$save(function (users) {
                $scope.users.push(users);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Saved Successfully');
            });
        }

        $scope.useredit = false;
        $scope.newuser = new User();
        //loadUser();
    };

    $scope.deleteUser = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.users.indexOf(item);
                            $scope.users.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'User Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/user/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.users.indexOf(item);
                                $scope.users.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'User Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        $scope.showUser = false;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
        $scope.showUser = true;
        $scope.showHostel=false;
        $scope.showMess=false;
    };

    $scope.savePermission = function (ev){
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Would you like to Reset Permission ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function(scope){

                        UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                            .success(function(data){
                                $ngConfirm({theme: 'dark',buttons: {ok: {text: "Ok!",btnClass: 'btn-primary',keys: ['enter']}},content:'Permission Changed Successfully'});

                                $scope.resetPermission = false;
                                $scope.backToUsers();
                            });
                    }
                },
                // short hand button definition
                close: function(scope){
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };
});