app.controller('TestimonialArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Testimonial){

    $scope.testimonials = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTestimonial() {
        $scope.testimonials = [];
        Testimonial.query({department:'Architecture'},function (testimonials) {
            $scope.testimonials = testimonials;
        });
    }

    loadTestimonial();


    $scope.testimonialedit = false;
    $scope.imageViewTestimonial=false;

    $scope.newTestimonial = function () {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.newtestimonial.department='Architecture';
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisFlash) {
        $scope.testimonialedit = true;
        $scope.imageViewTestimonial=true;
        $scope.curTestimonial = thisFlash;
        $scope.newtestimonial = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function (flash) {
                angular.extend($scope.curTestimonial, $scope.curTestimonial, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });

        } else {
            $scope.newtestimonial.$save(function (testimonials) {
                $scope.testimonials.push(testimonials);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }

        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
        angular.element("input[type='file']").val(null);
        //loadTestimonial();
    };

    $scope.deleteTestimonial = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.testimonials.indexOf(item);
                            $scope.testimonials.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Testimonial Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/testimonials/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.testimonials.indexOf(item);
                                $scope.testimonials.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Testimonial Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});
