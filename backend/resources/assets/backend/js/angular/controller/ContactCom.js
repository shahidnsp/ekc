app.controller('ContactComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Contact){

    $scope.contacts = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadContact() {
        $scope.contacts = [];
        Contact.query({department:'Commerce'},function (contact) {
            $scope.contacts = contact;
        });
    }

    loadContact();

    $scope.otificationedit = false;

    $scope.newContact = function () {
        $scope.contactedit = true;
        $scope.newcontact = new Contact();
        $scope.newcontact.department='Commerce';
        $scope.curContact = {};
    };
    $scope.editContact = function (thisContact) {
        $scope.contactedit = true;
        $scope.curContact = thisContact;
        $scope.newcontact = angular.copy(thisContact);
        $anchorScroll();
    };

    $scope.cancelContact = function () {
        $scope.contactedit = false;
        $scope.newcontact = new Contact();
    };

    $scope.addContact = function () {
        if ($scope.curContact.id) {
            $scope.newcontact.$update(function (flash) {
                angular.extend($scope.curContact, $scope.curContact, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Updated Successfully');
            });

        } else {
            $scope.newcontact.$save(function (contacts) {
                $scope.contacts.push(contacts);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Contact Saved Successfully');
            });
        }

        $scope.contactedit = false;
        $scope.newcontact = new Contact();
        angular.element("input[type='file']").val(null);
        //loadContact();
    };

    $scope.deleteContact = function (item) {
        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.contacts.indexOf(item);
                            $scope.contacts.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Contact Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/contact/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.contacts.indexOf(item);
                                $scope.contacts.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Contact Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});