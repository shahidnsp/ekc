app.controller('HomeController', function($scope,$http,$location,$interval,UserInfo){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "activemenu" : "";
    };

    $scope.user = {};

    $scope.showEKC=false;
    $scope.showENG=false;
    $scope.showARC=false;
    $scope.showCOM=false;
    $scope.showPUB=false;
    $scope.showDash=false;

    UserInfo.query().success(function(data){
        $scope.user = data;
        checkExist(data);
        console.log(data);
    });

    function checkExist(user){
        var menues=user.menu;
        var length=menues.length;
        for(var i=0;i<length;i++){
            switch (menues[i].type){
                case 'EKCWebsite':
                    $scope.showEKC=true;
                    break;
                case 'Engineering':
                    $scope.showENG=true;
                    break;
                case 'Architecture':
                    $scope.showARC=true;
                    break;
                case 'Commerce':
                    $scope.showCOM=true;
                    break;
                case 'Public':
                    $scope.showPUB=true;
                    break;
                default:
                    $scope.showDash=true;

            }
        }
    }






    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100,500];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 100;
    /* Nav menu */

});


app.directive('clickAnywhereButHere', function($document){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function() {
                // magic here.
                scope.$apply(attr.clickAnywhereButHere);
            })
        }
    }
});