app.controller('DownloadArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Download){

    $scope.downloads = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadDownload() {
        $scope.downloads = [];
        Download.query({department:'Architecture'},function (downloads) {
            $scope.downloads = downloads;
        });
    }
    loadDownload();

    $scope.downloadedit = false;

    $scope.newDownload = function () {
        $scope.downloadedit = true;
        $scope.newdownload = new Download();
        $scope.newdownload.department='Architecture';
        $scope.newdownload.type='PDF';
        $scope.curDownload = {};
    };
    $scope.editDownload = function (thisFlash) {
        $scope.downloadedit = true;
        $scope.curDownload = thisFlash;
        $scope.newdownload = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelDownload = function () {
        $scope.downloadedit = false;
        $scope.newdownload = new Download();
    };

    $scope.addDownload = function () {
        if ($scope.curDownload.id) {
            $scope.newdownload.$update(function (flash) {
                angular.extend($scope.curDownload, $scope.curDownload, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Download Updated Successfully');
            });

        } else {
            $scope.newdownload.$save(function (downloads) {
                $scope.downloads.push(downloads);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Download Saved Successfully');
            });
        }

        $scope.downloadedit = false;
        $scope.newdownload = new Download();
        //loadDownload();
    };

    $scope.deleteDownload = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.downloads.indexOf(item);
                            $scope.downloads.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Download Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/download/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.downloads.indexOf(item);
                                $scope.downloads.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Download Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
