app.controller('AdmissionArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Admission){

    $scope.admissions = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadAdmission() {
        $scope.admissions = [];
        Admission.query({department:'Architecture'},function (admissions) {
            $scope.admissions = admissions;
        });
    }

    loadAdmission();


    $scope.admissionedit = false;
    $scope.imageViewAdmission=false;

    $scope.newAdmission = function () {
        $scope.admissionedit = true;
        $scope.newadmission = new Admission();
        $scope.newadmission.department='Architecture';
        //$scope.newadmission.from='Main';
        $scope.curAdmission = {};
    };
    $scope.editAdmission = function (thisFlash) {
        $scope.admissionedit = true;
        $scope.imageViewAdmission=true;
        $scope.curAdmission = thisFlash;
        $scope.newadmission = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelAdmission = function () {
        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
    };

    $scope.addAdmission = function () {
        if ($scope.curAdmission.id) {
            $scope.newadmission.$update(function (flash) {
                angular.extend($scope.curAdmission, $scope.curAdmission, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Updated Successfully');
            });

        } else {
            $scope.newadmission.$save(function (admissions) {
                $scope.admissions.push(admissions);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Admission Saved Successfully');
            });
        }

        $scope.admissionedit = false;
        $scope.newadmission = new Admission();
        //loadAdmission();
    };

    $scope.deleteAdmission = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.admissions.indexOf(item);
                         $scope.admissions.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Admission Removed Successfully'
                         });

                         });*/

                        $http.get('apiweb/admissions/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.admissions.indexOf(item);
                                $scope.admissions.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Admission Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };

});
