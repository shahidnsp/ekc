
app.controller('NotificationPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Notification){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadNotification() {
        $scope.notifications = [];
        Notification.query({department:'Public'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadNotification();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newNotification = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Notification();
        $scope.newnotification.department='Public';
        $scope.curNotification = {};
    };
    $scope.editNotification = function (thisNotification) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curNotification = thisNotification;
        $scope.newnotification = angular.copy(thisNotification);
        $anchorScroll();
    };

    $scope.cancelNotification = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
    };

    $scope.addNotification = function () {
        if ($scope.curNotification.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curNotification, $scope.curNotification, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Notification Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Notification Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Notification();
        angular.element("input[type='file']").val(null);
        //loadNotification();
    };

    $scope.deleteNotification = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.notifications.indexOf(item);
                            $scope.notifications.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Notification Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/notification/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Notification Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
