app.controller('TeamsArcController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Team){

    $scope.teams = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadTeam() {
        $scope.teams = [];
        Team.query({department:'Architecture'},function (teams) {
            $scope.teams = teams;
        });
    }

    loadTeam();


    $scope.teamedit = false;
    $scope.imageViewTeam=false;

    $scope.newTeam = function () {
        $scope.teamedit = true;
        $scope.newteam = new Team();
        $scope.newteam.department='Architecture';
        $scope.curTeam = {};
    };
    $scope.editTeam = function (thisFlash) {
        $scope.teamedit = true;
        $scope.imageViewTeam=true;
        $scope.curTeam = thisFlash;
        $scope.newteam = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelTeam = function () {
        $scope.teamedit = false;
        $scope.newteam = new Team();
    };

    $scope.addTeam = function () {
        if ($scope.curTeam.id) {
            $scope.newteam.$update(function (flash) {
                angular.extend($scope.curTeam, $scope.curTeam, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Team Updated Successfully');
            });

        } else {
            $scope.newteam.$save(function (teams) {
                $scope.teams.push(teams);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                $scope.newteam = {};
                ngNotify.set('Team Saved Successfully');

            });
        }

        $scope.teamedit = false;
        $scope.newteam = new Team();
        angular.element("input[type='file']").val(null);
        //loadTeam();
    };

    $scope.deleteTeam = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.teams.indexOf(item);
                            $scope.teams.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Team Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/teams/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.teams.indexOf(item);
                                $scope.teams.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Team Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };


    $scope.changeOrder=function(item){
        $http.post('apiweb/changeOrder/'+item.id+'/'+item.order)
            .then(function onSuccess(response) {
                angular.extend(item, item, response.data);

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
    }



});
