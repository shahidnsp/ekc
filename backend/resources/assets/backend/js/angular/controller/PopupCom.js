
app.controller('PopupComController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Popup){

    $scope.notifications = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadPopup() {
        $scope.notifications = [];
        Popup.query({department:'Commerce'},function (notification) {
            $scope.notifications = notification;
        });
    }

    loadPopup();


    $scope.notificationedit = false;
    $scope.curnotification=false;

    $scope.newPopup = function () {
        $scope.notificationedit = true;
        $scope.newnotification = new Popup();
        $scope.newnotification.department='Commerce';
        $scope.curPopup = {};
    };
    $scope.editPopup = function (thisPopup) {
        $scope.notificationedit = true;
        $scope.curnotification=true;
        $scope.curPopup = thisPopup;
        $scope.newnotification = angular.copy(thisPopup);
        $anchorScroll();
    };

    $scope.cancelPopup = function () {
        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
    };

    $scope.addPopup = function () {
        if ($scope.curPopup.id) {
            $scope.newnotification.$update(function (flash) {
                angular.extend($scope.curPopup, $scope.curPopup, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Popup Updated Successfully');
            });

        } else {
            $scope.newnotification.$save(function (notifications) {
                $scope.notifications.push(notifications);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Popup Saved Successfully');
            });
        }

        $scope.notificationedit = false;
        $scope.newnotification = new Popup();
        angular.element("input[type='file']").val(null);
        //loadPopup();
    };

    $scope.deletePopup = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                         var curIndex = $scope.notifications.indexOf(item);
                         $scope.notifications.splice(curIndex, 1);

                         $ngConfirm({
                         theme: 'dark',
                         buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                         content: 'Popup Removed Successfully'
                         });
                         });*/

                        $http.get('apiweb/popup/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.notifications.indexOf(item);
                                $scope.notifications.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Popup Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
