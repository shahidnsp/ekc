app.controller('SliderComController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Slider){

    $scope.sliders = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadSlider() {
        $scope.sliders = [];
        Slider.query({department:'Commerce'},function (sliders) {
            $scope.sliders = sliders;
        });
    }

    loadSlider();


    $scope.slideredit = false;

    $scope.newSlider = function () {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.newslider.department='Commerce';
        $scope.curSlider = {};
    };
    $scope.editSlider = function (thisFlash) {
        $scope.slideredit = true;
        $scope.curSlider = thisFlash;
        $scope.newslider = angular.copy(thisFlash);
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function (flash) {
                angular.extend($scope.curSlider, $scope.curSlider, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });

        } else {
            $scope.newslider.$save(function (sliders) {
                $scope.sliders.push(sliders);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }

        $scope.slideredit = false;
        $scope.newslider = new Slider();
        angular.element("input[type='file']").val(null);
        //loadSlider();
    };

    $scope.deleteSlider = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.sliders.indexOf(item);
                            $scope.sliders.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Slider Removed Successfully'
                            });

                        });*/

                        $http.get('apiweb/slider/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.sliders.indexOf(item);
                                $scope.sliders.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Slider Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });

    };



});
