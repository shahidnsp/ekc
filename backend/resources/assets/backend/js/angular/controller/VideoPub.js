
app.controller('VideoPubController', function($scope,$http,$anchorScroll,$modal, $ngConfirm, ngNotify, Video){

    $scope.videos = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadVideo() {
        $scope.videos = [];
        Video.query({department:'Public'},function (video) {
            $scope.videos = video;
        });
    }

    loadVideo();


    $scope.otificationedit = false;

    $scope.newVideo = function () {
        $scope.videoedit = true;
        $scope.newvideo = new Video();
        $scope.newvideo.department='Public';
        $scope.curVideo = {};
    };
    $scope.editVideo = function (thisVideo) {
        $scope.videoedit = true;
        $scope.curVideo = thisVideo;
        $scope.newvideo = angular.copy(thisVideo);
        $anchorScroll();
    };

    $scope.cancelVideo = function () {
        $scope.videoedit = false;
        $scope.newvideo = new Video();
    };

    $scope.addVideo = function () {
        if ($scope.curVideo.id) {
            $scope.newvideo.$update(function (flash) {
                angular.extend($scope.curVideo, $scope.curVideo, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Updated Successfully');
            });

        } else {
            $scope.newvideo.$save(function (videos) {
                $scope.videos.push(videos);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Video Saved Successfully');
            });
        }

        $scope.videoedit = false;
        $scope.newvideo = new Video();
        angular.element("input[type='file']").val(null);
        //loadVideo();
    };

    $scope.deleteVideo = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.videos.indexOf(item);
                            $scope.videos.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Video Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/video/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.videos.indexOf(item);
                                $scope.videos.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Video Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});