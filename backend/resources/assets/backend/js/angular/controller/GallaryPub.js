app.controller('GalleryPubController', function($scope,$http,$interval,$anchorScroll,$modal, $ngConfirm, ngNotify, Gallery){

    $scope.gallerys = [];

    $scope.show1 = false;
    $scope.show2 = false;
    $scope.show3 = false;

    function loadGallery() {
        $scope.gallerys = [];
        Gallery.query({department:'Public'},function (gallerys) {
            $scope.gallerys = gallerys;
        });
    }

    loadGallery();


    $scope.galleryedit = false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.newgallery.department='Public';
        $scope.curGallery = {};
    };
    $scope.editGallery = function (thisFlash) {
        $scope.galleryedit = true;
        $scope.curGallery = thisFlash;
        $scope.imagesviewgallary = true;
        $scope.newgallery = angular.copy(thisFlash);
        $scope.files=thisFlash.gallary_items;
        $anchorScroll();
        $scope.showSingle = false;
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {
        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function (flash) {
                angular.extend($scope.curGallery, $scope.curGallery, flash);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });

        } else {
            $scope.newgallery.$save(function (gallerys) {
                $scope.gallerys.push(gallerys);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });
                ngNotify.set('Gallery Saved Successfully');
            });
        }

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
        angular.element("input[type='file']").val(null);
        //loadGallery();
    };

    $scope.deleteGallery = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the item ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        /*item.$delete(function () {
                            var curIndex = $scope.gallerys.indexOf(item);
                            $scope.gallerys.splice(curIndex, 1);

                            $ngConfirm({
                                theme: 'dark',
                                buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                content: 'Gallery Removed Successfully'
                            });
                        });*/

                        $http.get('apiweb/gallery/'+item.id)
                            .then(function onSuccess(response) {
                                var curIndex = $scope.gallerys.indexOf(item);
                                $scope.gallerys.splice(curIndex, 1);

                                $ngConfirm({
                                    theme: 'dark',
                                    buttons: {ok: {text: "Ok!", btnClass: 'btn-primary', keys: ['enter']}},
                                    content: 'Gallery Removed Successfully'
                                });

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
    $scope.deleteGalleryImage = function (item) {

        $ngConfirm({
            title: 'Warning...................!',
            content: 'Do you really need to delete the Image ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function (scope) {
                        console.log(item);
                        $http.get('apiweb/gallery/delete-image/'+item.id)
                            .then(function onSuccess(response) {
                                $ngConfirm('Image Removed Successfully');

                                /*get index of item*/
                                var curIndex = $scope.newgallery.gallary_items.indexOf(item);

                                /*remove item from array using current index*/
                                $scope.newgallery.gallary_items.splice(curIndex, 1);

                            }, function myError(response) {
                                $scope.myWelcome = response.statusText;
                            });
                    }
                },
                // short hand button definition
                close: function (scope) {
                    //$ngConfirm('the user clicked close');
                }
            }
        });
    };
});
