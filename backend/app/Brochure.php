<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model
{
    //
    protected $fillable = [
        'title','filename','department','user_id','from'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
