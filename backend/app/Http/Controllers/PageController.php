<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;

class PageController extends Controller
{
    public function showHomePage(){
        return view('frontend.home');
    }

    public function showInstitutionsPage(){
        return view('frontend.institutions');
    }

    public function showAboutPage(){
        return view('frontend.about');
    }

    public function showConceptPage(){
        return view('frontend.concept');
    }

    public function showDirectorsPage(){
        return view('frontend.directors');
    }

    public function showGoverningbodyPage(){
        return view('frontend.governingbody');
    }

    public function showPrincipalPage(){
        return view('frontend.principal');
    }

    public function showAdmissionPage(){
        return view('frontend.admission');
    }

    public function showPlacementsPage(){
        return view('frontend.placements');
    }

    public function showFacilitiesPage(){
        return view('frontend.facilities');
    }

    public function showUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('newses', 2*60, function() {
            return \App\News::where('department','Main')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.updates',compact('newses'));
    }

    public function showPhotosPage(){
        return view('frontend.photos');
    }

    public function showVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('videos', 2*60, function() {
            return \App\Video::where('department','Main')->orderBy('id', 'desc')->paginate(6);
        });

        return view('frontend.videos',compact('videos'));
    }

    public function showContactPage(){
        return view('frontend.contact');
    }

    public function showOnlinegrievancePage(){
        return view('frontend.online-grievance');
    }

    public function showScholarshipsPage(){
        return view('frontend.scholarships');
    }

    public function showCareerPage(){
        return view('frontend.career');
    }
    
    public function show360Page(){
        return view('frontend.360');
    }

    public function showLiberalartsPage(){
        return view('frontend.liberalarts');
    }

    public function showBrochurePage(){
        return view('frontend.brochure');
    }

    /**
     * Validates given data
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'description' => 'required',
        ]);
    }
    public function postContact(Request $request){

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        return Redirect::back()->with('message', ['Thank You for Contacting with us.We respond you shortly.']);
    }



    //EKC Engineering.......................

    public function showTCIndexPage(){
        return view('frontend.EKC-TC.index');
    }

    public function showTCAboutPage(){
        return view('frontend.EKC-TC.about');
    }

    public function showTCDirectorsPage(){
        return view('frontend.EKC-TC.directors');
    }

    public function showTCGoverningbodyPage(){
        return view('frontend.EKC-TC.governingbody');
    }

    public function showTCPrincipalPage(){
        return view('frontend.EKC-TC.principal');
    }
    
    public function showTCAdministrativeTeamPage(){
        return view('frontend.EKC-TC.administrative-team');
    }

    public function showTCCommitteesPage(){
        return view('frontend.EKC-TC.committees');
    }

    public function showTCUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Main')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.updates',compact('newses'));
    }

    public function showTCPhotosPage(){
        return view('frontend.EKC-TC.photos');
    }

    public function showTCVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Main')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.videos',compact('videos'));
    }

    public function showTCContactPage(){
        return view('frontend.EKC-TC.contact');
    }

    public function showTCAdmissionPage(){
        return view('frontend.EKC-TC.admission');
    }

    public function showTCFacilitiesPage(){
        return view('frontend.EKC-TC.facilities');
    }

    public function showTCDownloadsPage(){
        return view('frontend.EKC-TC.downloads');
    }

    public function showTCLifePage(){
        return view('frontend.EKC-TC.life');
    }

    public function showTCCareerPage(){
        return view('frontend.EKC-TC.career');
    }

    // LIFE AT EKC

    public function showTCKBAIC_105Page(){
        return view('frontend.EKC-TC.KBAIC_105');
    }

    //CIVIL ENGINEERING
    public function showTCCivilIndexPage(){
        return view('frontend.EKC-TC.civil.index');
    }

    public function showTCCivilAboutPage(){
        return view('frontend.EKC-TC.civil.about');
    }

    public function showTCCivilProfilePage(){
        return view('frontend.EKC-TC.civil.profile');
    }

    public function showTCCivilVisionMissionPage(){
        return view('frontend.EKC-TC.civil.visionmission');
    }

    public function showTCCivilPeosposPage(){
        return view('frontend.EKC-TC.civil.peospos');
    }

    public function showTCCivilHOD_deskPage(){
        return view('frontend.EKC-TC.civil.HOD-desk');
    }

    public function showTCCivilStaffPage(){
        return view('frontend.EKC-TC.civil.staff');
    }

    public function showTCCivilCareerPage(){
        return view('frontend.EKC-TC.civil.career');
    }

    public function showTCCivilAdmissionPage(){
        return view('frontend.EKC-TC.civil.admission');
    }

    public function showTCCivilFacilitiesPage(){
        return view('frontend.EKC-TC.civil.facilities');
    }

    public function showTCCivilDownloadsPage(){
        return view('frontend.EKC-TC.civil.downloads');
    }

    public function showTCCivilUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCCivilnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Civil')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.civil.updates',compact('newses'));
    }

    public function showTCCivilPhotosPage(){
        return view('frontend.EKC-TC.civil.photos');
    }

    public function showTCCivilVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCCivilvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Civil')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.civil.videos',compact('videos'));
    }

    public function showTCCivilContactPage(){
        return view('frontend.EKC-TC.civil.contact');
    }

    //MECHANICAL ENGINEERING
    public function showTCMechanicalIndexPage(){
        return view('frontend.EKC-TC.mechanical.index');
    }

    public function showTCMechanicalAboutPage(){
        return view('frontend.EKC-TC.mechanical.about');
    }

    public function showTCMechanicalProfilePage(){
        return view('frontend.EKC-TC.mechanical.profile');
    }

    public function showTCMechanicalHOD_deskPage(){
        return view('frontend.EKC-TC.mechanical.HOD-desk');
    }

    public function showTCMechanicalVisionMissionPage(){
        return view('frontend.EKC-TC.mechanical.visionmission');
    }

    public function showTCMechanicalPeosposPage(){
        return view('frontend.EKC-TC.mechanical.peospos');
    }

    public function showTCMechanicalStaffPage(){
        return view('frontend.EKC-TC.mechanical.staff');
    }

    public function showTCMechanicalCareerPage(){
        return view('frontend.EKC-TC.mechanical.career');
    }

    public function showTCMechanicalAdmissionPage(){
        return view('frontend.EKC-TC.mechanical.admission');
    }

    public function showTCMechanicalFacilitiesPage(){
        return view('frontend.EKC-TC.mechanical.facilities');
    }

    public function showTCMechanicalDownloadsPage(){
        return view('frontend.EKC-TC.mechanical.downloads');
    }

    public function showTCMechanicalUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCMechanicalnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Mechanical')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.mechanical.updates',compact('newses'));
    }

    public function showTCMechanicalPhotosPage(){
        return view('frontend.EKC-TC.mechanical.photos');
    }

    public function showTCMechanicalVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCMechanicalvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Mechanical')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.mechanical.videos',compact('videos'));
    }

    public function showTCMechanicalContactPage(){
        return view('frontend.EKC-TC.mechanical.contact');
    }


    //Electronic ENGINEERING
    public function showTCElectronicIndexPage(){
        return view('frontend.EKC-TC.electronic.index');
    }

    public function showTCElectronicAboutPage(){
        return view('frontend.EKC-TC.electronic.about');
    }

    public function showTCElectronicProfilePage(){
        return view('frontend.EKC-TC.electronic.profile');
    }

    public function showTCElectronicHOD_deskPage(){
        return view('frontend.EKC-TC.electronic.HOD-desk');
    }

    public function showTCElectronicVisionMissionPage(){
        return view('frontend.EKC-TC.electronic.visionmission');
    }

    public function showTCElectronicPeosposPage(){
        return view('frontend.EKC-TC.electronic.peospos');
    }

    public function showTCElectronicStaffPage(){
        return view('frontend.EKC-TC.electronic.staff');
    }

    public function showTCElectronicCareerPage(){
        return view('frontend.EKC-TC.electronic.career');
    }

    public function showTCElectronicAdmissionPage(){
        return view('frontend.EKC-TC.electronic.admission');
    }

    public function showTCElectronicFacilitiesPage(){
        return view('frontend.EKC-TC.electronic.facilities');
    }

    public function showTCElectronicDownloadsPage(){
        return view('frontend.EKC-TC.electronic.downloads');
    }

    public function showTCElectronicUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCElectronicnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Electronics')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.electronic.updates',compact('newses'));
    }

    public function showTCElectronicPhotosPage(){
        return view('frontend.EKC-TC.electronic.photos');
    }

    public function showTCElectronicVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCElectronicvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Electronics')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.electronic.videos',compact('videos'));
    }

    public function showTCElectronicContactPage(){
        return view('frontend.EKC-TC.electronic.contact');
    }


    //Computer ENGINEERING
    public function showTCComputerIndexPage(){
        return view('frontend.EKC-TC.computer.index');
    }

    public function showTCComputerAboutPage(){
        return view('frontend.EKC-TC.computer.about');
    }

    public function showTCComputerProfilePage(){
        return view('frontend.EKC-TC.computer.profile');
    }

    public function showTCComputerHOD_deskPage(){
        return view('frontend.EKC-TC.computer.HOD-desk');
    }

    public function showTCComputerVisionMissionPage(){
        return view('frontend.EKC-TC.computer.visionmission');
    }

    public function showTCComputerPeosposPage(){
        return view('frontend.EKC-TC.computer.peospos');
    }

    public function showTCComputerStaffPage(){
        return view('frontend.EKC-TC.computer.staff');
    }

    public function showTCComputerCareerPage(){
        return view('frontend.EKC-TC.computer.career');
    }

    public function showTCComputerAdmissionPage(){
        return view('frontend.EKC-TC.computer.admission');
    }

    public function showTCComputerFacilitiesPage(){
        return view('frontend.EKC-TC.computer.facilities');
    }

    public function showTCComputerDownloadsPage(){
        return view('frontend.EKC-TC.computer.downloads');
    }

    public function showTCComputerUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCComputernewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Computer')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.computer.updates',compact('newses'));
    }

    public function showTCComputerPhotosPage(){
        return view('frontend.EKC-TC.computer.photos');
    }

    public function showTCComputerVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCComputervideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Computer')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.computer.videos',compact('videos'));
    }

    public function showTCComputerContactPage(){
        return view('frontend.EKC-TC.computer.contact');
    }


    //Safety ENGINEERING
    public function showTCSafetyIndexPage(){
        return view('frontend.EKC-TC.safety.index');
    }

    public function showTCSafetyAboutPage(){
        return view('frontend.EKC-TC.safety.about');
    }

    public function showTCSafetyProfilePage(){
        return view('frontend.EKC-TC.safety.profile');
    }

    public function showTCSafetyHOD_deskPage(){
        return view('frontend.EKC-TC.safety.HOD-desk');
    }

    public function showTCSafetyVisionMissionPage(){
        return view('frontend.EKC-TC.safety.visionmission');
    }

    public function showTCSafetyPeosposPage(){
        return view('frontend.EKC-TC.safety.peospos');
    }

    public function showTCSafetyStaffPage(){
        return view('frontend.EKC-TC.safety.staff');
    }

    public function showTCSafetyCareerPage(){
        return view('frontend.EKC-TC.safety.career');
    }

    public function showTCSafetyAdmissionPage(){
        return view('frontend.EKC-TC.safety.admission');
    }

    public function showTCSafetyFacilitiesPage(){
        return view('frontend.EKC-TC.safety.facilities');
    }

    public function showTCSafetyDownloadsPage(){
        return view('frontend.EKC-TC.safety.downloads');
    }

    public function showTCSafetyUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCSafetynewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Safety')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-TC.safety.updates',compact('newses'));
    }

    public function showTCSafetyPhotosPage(){
        return view('frontend.EKC-TC.safety.photos');
    }

    public function showTCSafetyVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCSafetyvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Safety')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.safety.videos',compact('videos'));
    }

    public function showTCSafetyContactPage(){
        return view('frontend.EKC-TC.safety.contact');
    }

    //Science and Humanities
    public function showTCHumanitiesIndexPage(){
        return view('frontend.EKC-TC.humanities.index');
    }

    public function showTCHumanitiesAboutPage(){
        return view('frontend.EKC-TC.humanities.about');
    }

    public function showTCHumanitiesProfilePage(){
        return view('frontend.EKC-TC.humanities.profile');
    }

    public function showTCHumanitiesHOD_deskPage(){
        return view('frontend.EKC-TC.humanities.HOD-desk');
    }

    public function showTCHumanitiesVisionMissionPage(){
        return view('frontend.EKC-TC.humanities.visionmission');
    }

    public function showTCHumanitiesPeosposPage(){
        return view('frontend.EKC-TC.humanities.peospos');
    }

    public function showTCHumanitiesStaffPage(){
        return view('frontend.EKC-TC.humanities.staff');
    }

    public function showTCHumanitiesCareerPage(){
        return view('frontend.EKC-TC.humanities.career');
    }

    public function showTCHumanitiesAdmissionPage(){
        return view('frontend.EKC-TC.humanities.admission');
    }

    public function showTCHumanitiesFacilitiesPage(){
        return view('frontend.EKC-TC.humanities.facilities');
    }

    public function showTCHumanitiesDownloadsPage(){
        return view('frontend.EKC-TC.humanities.downloads');
    }

    public function showTCHumanitiesUpdatesPage(){

        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('TCHumanitiesnewses', 2*60, function() {
            return \App\News::where('department','Engineering')->where('from','Humanities')->orderBy('id', 'desc')->paginate(9);
        });

        return view('frontend.EKC-TC.humanities.updates',compact('newses'));
    }

    public function showTCHumanitiesPhotosPage(){
        return view('frontend.EKC-TC.humanities.photos');
    }

    public function showTCHumanitiesVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('TCHumanitiesvideos', 2*60, function() {
            return \App\Video::where('department','Engineering')->where('from','Humanities')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-TC.humanities.videos',compact('videos'));
    }

    public function showTCHumanitiesContactPage(){
        return view('frontend.EKC-TC.humanities.contact');
    }


    //COLLEGE OF ARCHITECTURE..................
    public function showCOAIndexPage(){
        return view('frontend.EKC-COA.index');
    }

    public function showCOAAboutPage(){
        return view('frontend.EKC-COA.about');
    }
    
    public function showCOAGoverningbodyPage(){
        return view('frontend.EKC-COA.governingbody');
    }

    public function showCOADirectorsPage(){
        return view('frontend.EKC-COA.directors');
    }
    
    public function showCOAPrincipalPage(){
        return view('frontend.EKC-COA.principal');
    }

    public function showCOAHODdeskPage(){
        return view('frontend.EKC-COA.HODdesk');
    }

    public function showCOAAdministrativeTeamPage(){
        return view('frontend.EKC-COA.administrative-team');
    }

    public function showCOAStaffPage(){
        return view('frontend.EKC-COA.staff');
    }

    public function showCOATeamPage(){
        return view('frontend.EKC-COA.team');
    }

    public function showCOACareerPage(){
        return view('frontend.EKC-COA.career');
    }

    public function showCOAAcademicsPage(){
        return view('frontend.EKC-COA.academics');
    }

    public function showCOACurricularActivityPage(){
        return view('frontend.EKC-COA.curricular-activity');
    }

    public function showCOAAdmissionPage(){
        return view('frontend.EKC-COA.admission');
    }

    public function showCOAFacilitiesPage(){
        return view('frontend.EKC-COA.facilities');
    }

    public function showCOADownloadsPage(){
        return view('frontend.EKC-COA.downloads');
    }

    public function showCOAUpdatesPage(){

        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('Arnewses', 2*60, function() {
            return \App\News::where('department','Architecture')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-COA.updates',compact('newses'));
    }

    public function showCOAPhotosPage(){
        return view('frontend.EKC-COA.photos');
    }

    public function showCOAVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('Arvideos', 2*60, function() {
            return \App\Video::where('department','Architecture')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-COA.videos',compact('videos'));
    }

    public function showCOAContactPage(){
        return view('frontend.EKC-COA.contact');
    }

    //College of Commerce & Science
    public function showCSIndexPage(){
        return view('frontend.EKC-CS.index');
    }

    public function showCSAboutPage(){
        return view('frontend.EKC-CS.about');
    }

    public function showCSGoverningbodyPage(){
        return view('frontend.EKC-CS.governingbody');
    }

    public function showCSDirectorsPage(){
        return view('frontend.EKC-CS.directors');
    }

    public function showCSPrincipalPage(){
        return view('frontend.EKC-CS.principal');
    }

    public function showCSAdministrativeTeamPage(){
        return view('frontend.EKC-CS.administrative-team');
    }

    public function showCSCommitteesPage(){
        return view('frontend.EKC-CS.committees');
    }

    public function showCSCareerPage(){
        return view('frontend.EKC-CS.career');
    }

    public function showCSAdmissionPage(){
        return view('frontend.EKC-CS.admission');
    }

    public function showCSFacilitiesPage(){
        return view('frontend.EKC-CS.facilities');
    }

    public function showCSDownloadsPage(){
        return view('frontend.EKC-CS.downloads');
    }

    public function showCSLifePage(){
        return view('frontend.EKC-CS.life');
    }

    public function showCSUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.updates',compact('newses'));
    }

    public function showCSPhotosPage(){
        return view('frontend.EKC-CS.photos');
    }

    public function showCSVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.videos',compact('videos'));
    }

    public function showCSContactPage(){
        return view('frontend.EKC-CS.contact');
    }

    public function showCSlaungagePage(){
        return view('frontend.EKC-CS.laungage');
    }



    //BBA
    public function showCSBBAIndexPage(){
        return view('frontend.EKC-CS.bba.index');
    }

    public function showCSBBAAboutPage(){
        return view('frontend.EKC-CS.bba.about');
    }

    public function showCSBBAProfilePage(){
        return view('frontend.EKC-CS.bba.profile');
    }

    public function showCSBBAHOD_deskPage(){
        return view('frontend.EKC-CS.bba.HOD-desk');
    }

    public function showCSBBAStaffPage(){
        return view('frontend.EKC-CS.bba.staff');
    }

    public function showCSBBACareerPage(){
        return view('frontend.EKC-CS.bba.career');
    }

    public function showCSBBAAdmissionPage(){
        return view('frontend.EKC-CS.bba.admission');
    }

    public function showCSBBAFacilitiesPage(){
        return view('frontend.EKC-CS.bba.facilities');
    }

    public function showCSBBADownloadsPage(){
        return view('frontend.EKC-CS.bba.downloads');
    }

    public function showCSBBAUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBBAnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BBA')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.bba.updates',compact('newses'));
    }

    public function showCSBBAPhotosPage(){
        return view('frontend.EKC-CS.bba.photos');
    }

    public function showCSBBAVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBBAvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BBA')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.bba.videos',compact('videos'));
    }

    public function showCSBBAContactPage(){
        return view('frontend.EKC-CS.bba.contact');
    }

    //BCOM Finance
    public function showCSBCOMFinanceIndexPage(){
        return view('frontend.EKC-CS.BCOMFinance.index');
    }

    public function showCSBCOMFinanceAboutPage(){
        return view('frontend.EKC-CS.BCOMFinance.about');
    }

    public function showCSBCOMFinanceProfilePage(){
        return view('frontend.EKC-CS.BCOMFinance.profile');
    }

    public function showCSBCOMFinanceHOD_deskPage(){
        return view('frontend.EKC-CS.BCOMFinance.HOD-desk');
    }

    public function showCSBCOMFinanceStaffPage(){
        return view('frontend.EKC-CS.BCOMFinance.staff');
    }

    public function showCSBCOMFinanceCareerPage(){
        return view('frontend.EKC-CS.BCOMFinance.career');
    }

    public function showCSBCOMFinanceAdmissionPage(){
        return view('frontend.EKC-CS.BCOMFinance.admission');
    }

    public function showCSBCOMFinanceFacilitiesPage(){
        return view('frontend.EKC-CS.BCOMFinance.facilities');
    }

    public function showCSBCOMFinanceDownloadsPage(){
        return view('frontend.EKC-CS.BCOMFinance.downloads');
    }

    public function showCSBCOMFinanceUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBCOMFinancenewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BCOMFinance')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.BCOMFinance.updates',compact('newses'));
    }

    public function showCSBCOMFinancePhotosPage(){
        return view('frontend.EKC-CS.BCOMFinance.photos');
    }

    public function showCSBCOMFinanceVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBCOMFinancevideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BCOMFinance')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.BCOMFinance.videos',compact('videos'));
    }

    public function showCSBCOMFinanceContactPage(){
        return view('frontend.EKC-CS.BCOMFinance.contact');
    }


    //BSC Computer Science
    public function showCSBSCCSIndexPage(){
        return view('frontend.EKC-CS.BSCCS.index');
    }

    public function showCSBSCCSAboutPage(){
        return view('frontend.EKC-CS.BSCCS.about');
    }

    public function showCSBSCCSProfilePage(){
        return view('frontend.EKC-CS.BSCCS.profile');
    }

    public function showCSBSCCSHOD_deskPage(){
        return view('frontend.EKC-CS.BSCCS.HOD-desk');
    }

    public function showCSBSCCSStaffPage(){
        return view('frontend.EKC-CS.BSCCS.staff');
    }

    public function showCSBSCCSCareerPage(){
        return view('frontend.EKC-CS.BSCCS.career');
    }

    public function showCSBSCCSAdmissionPage(){
        return view('frontend.EKC-CS.BSCCS.admission');
    }

    public function showCSBSCCSFacilitiesPage(){
        return view('frontend.EKC-CS.BSCCS.facilities');
    }

    public function showCSBSCCSDownloadsPage(){
        return view('frontend.EKC-CS.BSCCS.downloads');
    }

    public function showCSBSCCSUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBSCCSnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BSCCS')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.BSCCS.updates',compact('newses'));
    }

    public function showCSBSCCSPhotosPage(){
        return view('frontend.EKC-CS.BSCCS.photos');
    }

    public function showCSBSCCSVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBSCCSvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BSCCS')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.BSCCS.videos',compact('videos'));
    }

    public function showCSBSCCSContactPage(){
        return view('frontend.EKC-CS.BSCCS.contact');
    }



    //BA.Mass Communication and Journalism
    public function showCSBAMCAJIndexPage(){
        return view('frontend.EKC-CS.BAMCAJ.index');
    }

    public function showCSBAMCAJAboutPage(){
        return view('frontend.EKC-CS.BAMCAJ.about');
    }

    public function showCSBAMCAJProfilePage(){
        return view('frontend.EKC-CS.BAMCAJ.profile');
    }

    public function showCSBAMCAJHOD_deskPage(){
        return view('frontend.EKC-CS.BAMCAJ.HOD-desk');
    }

    public function showCSBAMCAJStaffPage(){
        return view('frontend.EKC-CS.BAMCAJ.staff');
    }

    public function showCSBAMCAJCareerPage(){
        return view('frontend.EKC-CS.BAMCAJ.career');
    }

    public function showCSBAMCAJAdmissionPage(){
        return view('frontend.EKC-CS.BAMCAJ.admission');
    }

    public function showCSBAMCAJFacilitiesPage(){
        return view('frontend.EKC-CS.BAMCAJ.facilities');
    }

    public function showCSBAMCAJDownloadsPage(){
        return view('frontend.EKC-CS.BAMCAJ.downloads');
    }

    public function showCSBAMCAJUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBAMCAJnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BAMCAJ')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.BAMCAJ.updates',compact('newses'));
    }

    public function showCSBAMCAJPhotosPage(){
        return view('frontend.EKC-CS.BAMCAJ.photos');
    }

    public function showCSBAMCAJVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBAMCAJvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BAMCAJ')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.BAMCAJ.videos',compact('videos'));
    }

    public function showCSBAMCAJContactPage(){
        return view('frontend.EKC-CS.BAMCAJ.contact');
    }


    //B.COM Corporation
    public function showCSBCOMCORPIndexPage(){
        return view('frontend.EKC-CS.BCOMCORP.index');
    }

    public function showCSBCOMCORPAboutPage(){
        return view('frontend.EKC-CS.BCOMCORP.about');
    }

    public function showCSBCOMCORPProfilePage(){
        return view('frontend.EKC-CS.BCOMCORP.profile');
    }

    public function showCSBCOMCORPHOD_deskPage(){
        return view('frontend.EKC-CS.BCOMCORP.HOD-desk');
    }

    public function showCSBCOMCORPStaffPage(){
        return view('frontend.EKC-CS.BCOMCORP.staff');
    }

    public function showCSBCOMCORPCareerPage(){
        return view('frontend.EKC-CS.BCOMCORP.career');
    }

    public function showCSBCOMCORPAdmissionPage(){
        return view('frontend.EKC-CS.BCOMCORP.admission');
    }

    public function showCSBCOMCORPFacilitiesPage(){
        return view('frontend.EKC-CS.BCOMCORP.facilities');
    }

    public function showCSBCOMCORPDownloadsPage(){
        return view('frontend.EKC-CS.BCOMCORP.downloads');
    }

    public function showCSBCOMCORPUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBCOMCORPnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BCOMCORP')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.BCOMCORP.updates',compact('newses'));
    }

    public function showCSBCOMCORPPhotosPage(){
        return view('frontend.EKC-CS.BCOMCORP.photos');
    }

    public function showCSBCOMCORPVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBCOMCORPvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BCOMCORP')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.BCOMCORP.videos',compact('videos'));
    }

    public function showCSBCOMCORPContactPage(){
        return view('frontend.EKC-CS.BCOMCORP.contact');
    }

    //B.COM Computer Application
    public function showCSBCOMCAIndexPage(){
        return view('frontend.EKC-CS.BCOMCA.index');
    }

    public function showCSBCOMCAAboutPage(){
        return view('frontend.EKC-CS.BCOMCA.about');
    }

    public function showCSBCOMCAProfilePage(){
        return view('frontend.EKC-CS.BCOMCA.profile');
    }

    public function showCSBCOMCAHOD_deskPage(){
        return view('frontend.EKC-CS.BCOMCA.HOD-desk');
    }

    public function showCSBCOMCAStaffPage(){
        return view('frontend.EKC-CS.BCOMCA.staff');
    }

    public function showCSBCOMCACareerPage(){
        return view('frontend.EKC-CS.BCOMCA.career');
    }

    public function showCSBCOMCAAdmissionPage(){
        return view('frontend.EKC-CS.BCOMCA.admission');
    }

    public function showCSBCOMCAFacilitiesPage(){
        return view('frontend.EKC-CS.BCOMCA.facilities');
    }

    public function showCSBCOMCADownloadsPage(){
        return view('frontend.EKC-CS.BCOMCA.downloads');
    }

    public function showCSBCOMCAUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSBCOMCAnewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','BCOMCA')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.BCOMCA.updates',compact('newses'));
    }

    public function showCSBCOMCAPhotosPage(){
        return view('frontend.EKC-CS.BCOMCA.photos');
    }

    public function showCSBCOMCAVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSBCOMCAvideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','BCOMCA')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.BCOMCA.videos',compact('videos'));
    }

    public function showCSBCOMCAContactPage(){
        return view('frontend.EKC-CS.BCOMCA.contact');
    }


    //M.COM Finance
    public function showCSMCOMFinanceIndexPage(){
        return view('frontend.EKC-CS.MCOMFinance.index');
    }

    public function showCSMCOMFinanceAboutPage(){
        return view('frontend.EKC-CS.MCOMFinance.about');
    }

    public function showCSMCOMFinanceProfilePage(){
        return view('frontend.EKC-CS.MCOMFinance.profile');
    }

    public function showCSMCOMFinanceHOD_deskPage(){
        return view('frontend.EKC-CS.MCOMFinance.HOD-desk');
    }

    public function showCSMCOMFinanceStaffPage(){
        return view('frontend.EKC-CS.MCOMFinance.staff');
    }

    public function showCSMCOMFinanceCareerPage(){
        return view('frontend.EKC-CS.MCOMFinance.career');
    }

    public function showCSMCOMFinanceAdmissionPage(){
        return view('frontend.EKC-CS.MCOMFinance.admission');
    }

    public function showCSMCOMFinanceFacilitiesPage(){
        return view('frontend.EKC-CS.MCOMFinance.facilities');
    }

    public function showCSMCOMFinanceDownloadsPage(){
        return view('frontend.EKC-CS.MCOMFinance.downloads');
    }

    public function showCSMCOMFinanceUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('CSMCOMFinancenewses', 2*60, function() {
            return \App\News::where('department','Commerce')->where('from','MCOMFinance')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-CS.MCOMFinance.updates',compact('newses'));
    }

    public function showCSMCOMFinancePhotosPage(){
        return view('frontend.EKC-CS.MCOMFinance.photos');
    }

    public function showCSMCOMFinanceVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('CSMCOMFinancevideos', 2*60, function() {
            return \App\Video::where('department','Commerce')->where('from','MCOMFinance')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-CS.MCOMFinance.videos',compact('videos'));
    }

    public function showCSMCOMFinanceContactPage(){
        return view('frontend.EKC-CS.MCOMFinance.contact');
    }


    //Public School
    public function showSCHIndexPage(){
        return view('frontend.EKC-SCHOOL.index');
    }

    public function showSCHAboutPage(){
        return view('frontend.EKC-SCHOOL.about');
    }

    public function showSCHGoverningbodyPage(){
        return view('frontend.EKC-SCHOOL.governingbody');
    }

    public function showSCHDirectorsPage(){
        return view('frontend.EKC-SCHOOL.directors');
    }

    public function showSCHPrincipalPage(){
        return view('frontend.EKC-SCHOOL.principal');
    }

    public function showSCHAdministrativeTeamPage(){
        return view('frontend.EKC-SCHOOL.administrative-team');
    }

    public function showSCHStaffPage(){
        return view('frontend.EKC-SCHOOL.staff');
    }

    public function showSCHCareerPage(){
        return view('frontend.EKC-SCHOOL.career');
    }

    public function showSCHAdmissionPage(){
        return view('frontend.EKC-SCHOOL.admission');
    }

    public function showSCHFacilitiesPage(){
        return view('frontend.EKC-SCHOOL.facilities');
    }

    public function showSCHLifePage(){
        return view('frontend.EKC-SCHOOL.life');
    }

    public function showSCHUpdatesPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $newses = \Illuminate\Support\Facades\Cache::remember('SCHnewses', 2*60, function() {
            return \App\News::where('department','Public')->orderBy('id', 'desc')->paginate(9);
        });
        return view('frontend.EKC-SCHOOL.updates',compact('newses'));
    }

    public function showSCHPhotosPage(){
        return view('frontend.EKC-SCHOOL.photos');
    }

    public function showSCHVideosPage(){
        \Illuminate\Support\Facades\Cache::flush();
        $videos = \Illuminate\Support\Facades\Cache::remember('SCHvideos', 2*60, function() {
            return \App\Video::where('department','Public')->orderBy('id', 'desc')->paginate(6);
        });
        return view('frontend.EKC-SCHOOL.videos',compact('videos'));
    }

    public function showSCHContactPage(){
        return view('frontend.EKC-SCHOOL.contact');
    }

    public function onlineApplication(Request $request)
    {
        $mail_data['name'] = $request->name;
        $mail_data['gender'] = $request->gender;
        $mail_data['dob'] = $request->dob;
        $mail_data['phone'] = $request->phone;
        $mail_data['place'] = $request->place;
        $mail_data['f_name'] = $request->f_name;
        $mail_data['address'] = $request->address;
        $mail_data['cast'] = $request->cast;
        $mail_data['plustworeg'] = $request->plustworeg;
        $mail_data['yearofpass'] = $request->yearofpass;
        $mail_data['cource'] = $request->cource;

        Mail::send('email.contactform', $mail_data, function ($message) use ($mail_data) {
            $message->to('office@eranadknowledgecity.com', 'EKC Web')->subject('Contact from web');
        });
        return Redirect::back();
    }

}
