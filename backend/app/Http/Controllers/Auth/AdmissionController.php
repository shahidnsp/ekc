<?php

namespace App\Http\Controllers\Auth;

use App\Admission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Response;
use Image;
use Storage;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required', 'department'=>'required'
        ]);
    }
    public function index(Request $request)
    {
        $department=$request->department;
        return Admission::where('department',$department)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }


        $admission = new Admission($request->all());
        $admission->appno=$this->genApplicationNo();
        $percentage=($admission->plustwomark*100)/1200;
        $admission->plustwomarkper=$percentage;

        $natascore= $request->natascore;
        if($natascore!=null){
            $ekcscore=($percentage*2)+$natascore;
            $admission->ekcscore=$ekcscore;
        }

        if ($admission->save()) {
            return $admission;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }


    private function genApplicationNo(){
        $count=Admission::count('id');
        return $count+1;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Admission::destroy($id)) {
            return Response::json(['msg' => 'Admission Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $admission = Admission::findOrfail($id);
        $admission->fill($request->all());


        if ($admission->update()) {
            return $admission;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Admission::destroy($id)) {
            return Response::json(['msg' => 'Admission Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
