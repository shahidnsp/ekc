<?php

namespace App\Http\Controllers\Auth;

use App\Gallary;
use App\GallaryItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Validator;
use Auth;
use Response;
use Image;
use Storage;

class GallaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required', 'department'=>'required'
        ]);
    }
    public function index(Request $request)
    {
        $department=$request->department;
        return Gallary::where('department',$department)->with('user','gallary_items')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $gallary = new Gallary($request->all());
        $gallary->user_id=Auth::id();
        $photos=$request->photos;

        if ($gallary->save()) {

            if($photos!=null){
                foreach ($photos as $photo){
                    $item=new GallaryItem();
                    $item->filename = $this->savePhoto($photo['data']);
                    $item->type ='Photo';
                    $item->gallaries_id =$gallary->id;
                    $item->save();
                }
            }
            Cache::flush();
            return $gallary;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'news'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                // $img=Image::make($data)->resize(256, 256)->stream();
                $img=Image::make($data)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Gallary::destroy($id)) {
            GallaryItem::where('gallaries_id',$id)->delete();
            Cache::flush();
            return Response::json(['msg' => 'Gallery Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }
        $gallary = Gallary::findOrfail($id);
        $gallary->fill($request->all());
        $gallary->user_id=Auth::id();

        $photos=$request->photos;

        if ($gallary->update()) {
            if($photos!=null){

                foreach ($photos as $photo){
                    $item=new GallaryItem();
                    $item->filename = $this->savePhoto($photo['data']);
                    $item->type ='Photo';
                    $item->gallaries_id =$gallary->id;
                    $item->save();
                }
            }
            Cache::flush();
            return Gallary::where('id', $gallary->id)->with('user', 'gallary_items')->get()->first();
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Gallary::destroy($id)) {
            GallaryItem::where('gallaries_id',$id)->delete();
            Cache::flush();
            return Response::json(['msg' => 'Gallery Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }


    public function deleteImage($id)
    {
        if (GallaryItem::destroy($id)) {
            Cache::flush();
            return Response::json(['msg' => 'Image Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
