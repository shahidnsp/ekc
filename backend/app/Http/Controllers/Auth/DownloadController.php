<?php

namespace App\Http\Controllers\Auth;

use App\Download;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Validator;
use Auth;
use Response;
use Image;
use Storage;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',  'type' => 'required',  'department'=>'required'
        ]);
    }
    public function index(Request $request)
    {
        $department=$request->department;
        return Download::where('department',$department)->with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $download = new Download($request->all());
        $download->user_id=Auth::id();



        $type=$request->type;
        if($type=='Photo'){
            $files = $request->photos;
            if ($files != null) {
                foreach ($files as $file) {
                    $download->file = $this->savePhoto($file['data']);
                }
            }
        }

        if($type=='PDF'){
            $files = $request->photos;
            if ($files != null) {
                foreach ($files as $file) {
                    $download->file = $this->savePDF($file['data']);
                }
            }
        }

        if($type=='Doc'){
            $files = $request->photos;
            if ($files != null) {
                foreach ($files as $file) {
                    $download->file = $this->saveWord($file['data']);
                }
            }
        }

        if($type=='Excel'){
            $files = $request->photos;
            if ($files != null) {
                foreach ($files as $file) {
                    $download->file = $this->saveExcel($file['data']);
                }
            }
        }


        if ($download->save()) {

            Cache::flush();
            return $download;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'download'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                // $img=Image::make($data)->resize(256, 256)->stream();
                $img=Image::make($data)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    private function savePDF($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                //$mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'PDF'.rand(11111,99999).'.pdf';
                //file_put_contents('uploads/images/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    private function saveWord($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                //$mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'PDF'.rand(11111,99999).'.docx';
                //file_put_contents('uploads/images/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    private function saveExcel($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                //$mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'PDF'.rand(11111,99999).'.xlx';
                //file_put_contents('uploads/images/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Download::destroy($id)) {
            return Response::json(['msg' => 'Download Record Deleted']);
        }
        else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $download = Download::findOrfail($id);
        $download->fill($request->all());
        $download->user_id=Auth::id();
        $type=$request->type;
        if($type=='Photo'){
            $photos = $request->photos;
            if ($photos != null) {
                foreach ($photos as $photo) {
                    $download->file = $this->savePhoto($photo['data']);
                }
            }
        }

        if($type=='PDF'){
            $photos = $request->photos;
            if ($photos != null) {
                foreach ($photos as $photo) {
                    $download->file = $this->savePDF($photo['data']);
                }
            }
        }

        if ($download->update()) {

            Cache::flush();
            return $download;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Download::destroy($id)) {
            return Response::json(['msg' => 'Download Record Deleted']);
        }
        else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
