<?php

namespace App\Http\Controllers\Auth;

use App\Popup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Validator;
use Auth;
use Response;
use Image;
use Storage;

class PopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required','department'=>'required'
        ]);
    }
    public function index(Request $request)
    {
        $department=$request->department;
        return Popup::where('department',$department)->with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $popup = new Popup($request->all());
        $popup->user_id=Auth::id();
        if($request->link==null)
            $popup->link='#';
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $popup->photo = $this->savePhoto($photo['data']);
            }
        }

        if ($popup->save()) {
            Cache::flush();
            return $popup;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'notification'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                // $img=Image::make($data)->resize(256, 256)->stream();
                $img=Image::make($data)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
            return false;
        }
        return $fileName;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Popup::destroy($id)) {
            Cache::flush();
            return Response::json(['msg' => 'Popup Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $popup = Popup::findOrfail($id);
        $popup->fill($request->all());
        $popup->user_id=Auth::id();
        if($request->link==null)
            $popup->link='#';
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $popup->photo = $this->savePhoto($photo['data']);
            }
        }

        if ($popup->update()) {
            Cache::flush();
            return $popup;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Popup::destroy($id)) {

            Cache::flush();
            return Response::json(['msg' => 'Popup Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
