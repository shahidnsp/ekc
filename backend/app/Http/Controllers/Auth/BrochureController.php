<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Brochure;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Response;
use Image;
use Storage;

class BrochureController extends Controller
{

    /**
     * Validates given data
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $department=$request->department;
        return Brochure::where('department',$department)->with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $brouchure = new Brochure($request->all());
        $brouchure->user_id=Auth::id();

        $files=$request->photos;

        if($files!=null){
            foreach ($files as $file){
                $brouchure->filename = $this->savePDF($file['data']);
            }
        }


        if ($brouchure->save()) {
            return $brouchure;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    private function savePDF($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'pdf'.rand(11111,99999).'.pdf';

                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Brochure::destroy($id)) {
            return Response::json(['msg' => 'Brochure Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $brouchure = Brochure::findOrfail($id);
        $brouchure->fill($request->all());
        $brouchure->user_id=1;//Auth::id();


        if ($brouchure->update()) {
            return $brouchure;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Brochure::destroy($id)) {
            return Response::json(['msg' => 'Brochure Record Deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
