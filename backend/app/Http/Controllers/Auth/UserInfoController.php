<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\Redirect;
use Validator;
use Response;
use Auth;

use App\Helper\UserHelper;
use App\User;

class UserInfoController extends Controller
{
    const ALL_PERMISSION = true;
    const ONLY_PAGE = true;

    public function UserInfoController()
    {
        $userInfo['id']        = Auth::id();
        $userInfo['name'] = Auth::user()->name;
        $userInfo['isAdmin'] = Auth::user()->isAdmin;
        $userInfo['photo'] = Auth::user()->photo;
        $userInfo['email'] = Auth::user()->email;
        $userInfo['menu']      = UserHelper::pages(Auth::user()->permission);
        $userInfo['permissions'] = UserHelper::pages(Auth::user()->permission,false,true,true);

        return $userInfo;
    }

    public static  function getPermission(){
        $userInfo['id']        = Auth::id();
        $userInfo['name'] = Auth::user()->name;
        $userInfo['menu']      = UserHelper::pages(Auth::user()->permission);
        $userInfo['permissions'] = UserHelper::pages(Auth::user()->permission,false,true,true);

        return $userInfo;
    }


    public function resetUserPassword(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);
                //TODO add settings for reset password
                $user->password = 'admin';
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }

    }

    public function changeUserPassword(Request $request)
    {

        $data = $request->only('curpassword','newpassword','repassword');
        $validator = Validator::make($data,[
            'curpassword'=>'required',
            'newpassword'=>'required',
            'repassword'=>'required',
        ]);

        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        try{

            $user = User::findOrFail(Auth::id());
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'Requested user not found'],404);
        }

        if($user->changePassword($data['curpassword'],$data['newpassword'])){
            $user->pass=$data['newpassword'];
            if($user->save()){
                return $user;
            }
        }
        else
            return Response::json(['error'=>'Old password does not match'],400);
    }

    public function getUserPermission(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);

                $permission =
                    UserHelper::pages($user->permission,!self::ONLY_PAGE,self::ALL_PERMISSION);

                return Response::json($permission);
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }
    }

    public function setUserPermission(Request $request)
    {
        if($request->only('id','permission'))
        {
            $permission= UserHelper::makePermission($request->input('permission'));

            $user = User::findOrFail($request->input('id'));
            $user->permission = $permission;
            if($user->save())
                return $user;
            return Response::json(['error'=>'Server is down'],500);
        }

        return Response::json(['error'=>'bad request'],400);

        //return $request->input('permission');
    }

    public function getAllPages($id)
    {
        return $userPages = UserHelper::notPages(User::find($id)->permission);
    }

    public function updateProfile(Request $request){
        $user=Auth::user();
        $user->name=$request->name;
        if($request->address!=null){
            $user->address=$request->address;
        }

        if($request->phone!=null){
            $user->phone=$request->phone;
        }

        if($user->save()){
            return $user;
        }
    }
}
