<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;
use Tracker;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.index');
    }

    public function getDashboardInfo(){

        $lists=[];
        $onlineusers = Tracker::onlineUsers()->count(); // defaults to 3 minutes
        $lists['onlineusers']=$onlineusers;

        $pageViews = Tracker::pageViews(60 * 24 * 30);
        $pageCount=0;
        foreach($pageViews as $pageView){
            $pageCount=$pageCount+$pageView['total'];
        }

        $lists['pageViews']=$pageCount;

        $testimonials=Testimonial::where('department','Main')->get();
        $lists['testimonials']=$testimonials;

        return $lists;
    }
}
