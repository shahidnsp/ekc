<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $fillable = [
        'title','photo','description','department','user_id','from'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
