<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GallaryItem extends Model
{
    //
    protected $fillable = [
        'filename','type','gallaries_id'
    ];
}
