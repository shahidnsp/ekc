<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    //

    protected $fillable = [
        'name','designation','photo','description','department','user_id','from'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
