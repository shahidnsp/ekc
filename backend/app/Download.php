<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    //
    protected $fillable = [
        'name','type','link','file','department','user_id','from'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
