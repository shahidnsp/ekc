<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    //
    protected $fillable = [
        'title','description','department','photo','user_id','from','link'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
