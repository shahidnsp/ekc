<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    protected $fillable = [
        'appno','name','date','course','address','fathername','studentphone','fatherphone',
        'caste','plustworegno','yearofpass','gender','dateofbirth','plustwomark','plustwomarkper','keamregno',
        'jeeregno','jeescore','natascore','ekcscore','department','from'
    ];

}
