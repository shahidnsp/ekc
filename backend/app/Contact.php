<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name','email','description','department','user_id','from','address','phone'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
