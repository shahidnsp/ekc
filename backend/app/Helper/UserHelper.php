<?php

namespace App\Helper;
use Illuminate\Support\Facades\Config;

/**
 * All user defined helper function for app.
 * User: Shahid Neermunda
 * Date: 11/05/2017
 * Time: 5:16 AM
 */

class UserHelper
{

    /**
     * Get all not permitted pages
     * @param String $userPermission permission User to find
     * @param bool $isPlain get only page
     * @param bool $all get all permission including write and edit
     * @return array
     */

    public static function notPages($userPermission)
    {
        $pages = [];
        $allPages = config('app.pages');

        //Check if no permissions are set
        if(strlen($userPermission) == 0)
            return $allPages;

        $permissions = explode(',',$userPermission);

        foreach($permissions as $key =>$permission){

            $pagePermission = str_split($permission);

            if($pagePermission[0] == '0'){

                if(isset($allPages[$key])){
                    $page = $allPages[$key];
                    array_push($pages,$page);
                }
            }
        }

        return $pages;
    }

    /**
     *Get all permitted pages
     * @param String $userPermission permission User to find
     * @param bool $isPlain get only page
     * @param bool $all get all permission including write and edit
     * @return array
     */

    public static function pages($userPermission,$isPlain=false,$all=false,$isTitle=false)
    {
        $pages = [];
        $allPages =config('app.pages');

        $permissions = explode(',',$userPermission);

        foreach($permissions as $key =>$permission){

            $pagePermission = str_split($permission);

            if($pagePermission[0] == '1'){
                if(isset($allPages[$key])){
                    $page = $allPages[$key];
                    if($all){
                        $page['write'] = ($pagePermission[1] == '1')?'true':'false';
                        $page['edit'] = ($pagePermission[2] == '1')?'true':'false';
                        $page['delete'] = ($pagePermission[3] == '1')?'true':'false';
                    }
                    if($isTitle)
                        $pages[$page['alias']] = $page;
                    else
                        array_push($pages,$page);
                }
            }
        }

        if($isPlain)
            array_walk($pages,function(&$page){
                $page = $page['alias'];
            });
        return $pages;
    }

    /***
     * @param $jsonPermission
     * @return string with permission
     */

    public static function makePermission($jsonPermission)
    {
        $permisisons =[];

//        $pages = json_decode($jsonPermission);
        $pages = $jsonPermission;
        $allPages = config('app.pages');

        foreach($allPages as $page){

            if(($key = self::findPage($pages,$page['alias'])) !== false){
                $permission = '1';
//                $permission .= $pages[$key]->write === 'true'?'1':'0';
//                $permission .= $pages[$key]->edit === 'true'?'1':'0';

                $permission .= $pages[$key]['write'] === 'true'?'1':'0';
                $permission .= $pages[$key]['edit']=== 'true'?'1':'0';
                $permission .= $pages[$key]['delete'] === 'true'?'1':'0';

                array_push($permisisons,$permission);
            }
            else
                array_push($permisisons,'0000');
        }

        return implode(',',$permisisons);
    }

    public static function allPages()
    {

        return config('app.pages');
    }

    private static function findPage(array $pages, $needle)
    {
        foreach($pages as $key =>$page){
            if($page['alias'] == $needle)
                return $key;
        }
        return false;
    }
}
