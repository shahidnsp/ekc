<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallary extends Model
{
    //
    protected $fillable = [
        'title','description','department','user_id','from'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function gallary_items(){
        return $this->hasMany('App\GallaryItem','gallaries_id');
    }
}
