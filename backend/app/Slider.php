<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
     protected $fillable = [
            'department','photo','user_id','from'
        ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
