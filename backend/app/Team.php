<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $fillable = [
        'name','designation','photo','department','user_id','from','description','facebook','linkedin','gplus','twitter','showin','as','order','phone'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
